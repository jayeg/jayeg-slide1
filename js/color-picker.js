    (function($) {
        "use strict";
        // With the element initially hidden, we can show it slowly:
        $("#OpenColorPicker").click(function() {
            $("#ColorPicker").fadeIn();
        });

        $("#CloseColorPicker").click(function() {
            $(this).closest("#ColorPicker").fadeOut();
        });

        $(".orange").click(function() {
            $("#ColorStyle").attr("href", "css/style.css");
            $("#ColorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style.css");
            return false;
        });

        $(".green").click(function() {
            $("#ColorStyle").attr("href", "css/colors/green.css");
            $("#ColorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style-green.css");
            return false;
        });

        $(".blue").click(function() {
            $("#ColorStyle").attr("href", "css/colors/blue.css");
            $("#ColorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style-blue.css");
            return false;
        });

        $(".purple").click(function() {
            $("#ColorStyle").attr("href", "css/colors/purple.css");
            $("#ColorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style-purple.css");
            return false;
        });


    }(jQuery));
