<?php
    $meta_title       = 'jayegroup';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Contact us';

		
	$og_meta_image = 'http://www.jayegroup.com/images/social-media/contact123.jpg';
	$og_url = 'http://www.jayegroup.com/contact.php';
	$og_meta_title = 'Jayegroup Contact';
	$og_meta_description = 'Got a question or need a quote? feel free to contact us any time.';
	$site_name = 'Jayegroup';		
	
    $add_styles = '
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css" id="ColorStyle">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    ';
    
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/jquery.gmap.min.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';

  ?>


<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="GoogleMapWrapper clearfix">
        <div id="gmap-canvas" class="MapShadow" style="width:100%;height:300px;"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
               
					<?php
session_start();
					$contact_form_fields = array(
					  array('name'    => 'Name:',
							'type'    => 'name',
							'require' => 1),
					  array('name'    => 'E-mail:',
							'type'    => 'email',
							'require' => 1),
					  array('name'    => 'Tel/fax:',
							'type'    => 'input',
							'require' => 1),

					  array('name'    => 'Subject:',
							'type'    => 'subject',
							'require' => 1),
					  array('name'    => 'Message:',
							'type'    => 'textarea',
							'require' => 1),
					  array('name'    => 'Turing number:',
							'type'    => 'turing',
							'require' => 1,
							'url'     => 'contact-form/image.php',
							'prompt'  => 'Enter the number displayed above'));

					$contact_form_graph           = false;
					$contact_form_xhtml           = false;

					$contact_form_email           = "info@jayegroup.com";
					$contact_form_encoding        = "utf-8";
					$contact_form_default_subject = "Default subject";
					$contact_form_message_prefix  = "Sent from contact form\r\n==============================\r\n\r\n";

					include_once "contact-form/contact-form.php";

					?>				
				
				
              <!--  <form method="post" action="contact.php" name="contactform" id="contactform">
                    <div id="ContactWrapper">
                        <div class="LeaveMassage">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="name" type="text" id="name" class="form-control MassageInput" placeholder="Name">
                                </div>
                                <div class="col-sm-6">
                                    <input name="email" type="text" id="email" class="form-control MassageInput" placeholder="Email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="website" type="text" id="website"  class="form-control MassageInput" placeholder="Website">
                                </div>
                                <div class="col-sm-6">
                                    <input name="phone" type="text" id="phone" class="form-control MassageInput" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <textarea name="comments"  placeholder="Your Message" rows="10" id="comments"></textarea>
                                    <div class="SubCleButtons pull-right clearfix">
                                        <button type="submit" id="submit" class="Button ActivateButton">SUBMIT</button>
                                        <button class="Button">CLEAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form> -->
            </div>

            <div class="col-sm-4">
                <div class="Widget">
                    <div class="WidgetTitle">
                        <h4>CONTACT DETAILS</h4>
                    </div>
                    <div class="WidgetBody ContactDetails WhiteSkin clearfix">
                        <ul class="media-list">
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <i class="fa-location"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Head Office Address</h4>
                                    67-68 Hatton Garden, City of London , EC1N 8JY
                                </div>
                            </li>
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <i class="fa-phone"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Phone Number</h4>
                                   020 8778 2389 / 
                                    
                                </div>
                            </li>
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <i class="fa-paper-plane"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Email Address</h4>
									<?php echo $email ;?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br /><br />
	<?php include 'includes/core/testimonials.php';?>

	<?php include 'includes/core/footer_in_full.php';?>
    <script type="text/javascript">
    jQuery('#gmap-canvas').gMap({
        maptype: 'ROADMAP',
        scrollwheel: false,
        zoom: 15,
        markers: [{
            address: 'EC1N 8JY', // Your Adress Here
            html: '',
            popup: false,
        }],
    });
    </script>
</body>
</html>
