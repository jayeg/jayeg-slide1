<?php
{/* meta tags */

    $meta_title       = 'Terms and Conditions';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Terms and Conditions';

		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/TermsCondo/terms-and-conditions-cropped.jpg';
	$og_url = 'http://www.jayegroup.com/motorbike.php';
	$og_meta_title = 'Win free motorbike recovery service!';
	$og_meta_description = 'simply like this post to enter the competition!';
	$site_name = 'Jayegroup';	
	
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>
            </div>
        </div>
    </div>
    <!-- The class termscondo is located in theme-responsive.css file -->
    <header class="elements termscondo"> 
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		
    <div class="container">

                    <div class="terms-condition">
                        <h1>Jayegroup Terms & Conditions of works & services</h1>
                        <h2>Terms and conditions of Jayegroup concerning all works, quotes and man & van services</h2>  
                        <h3>Summary</h3> 


<!-- if you wanna show the pdf in bootstrap modal -->
 <!--                        <div class="container">
  <div class="row">
    <h2>PDF in modal preview using Easy Modal Plugin</h2>
        <a class="btn btn-primary view-pdf" href="images/TermsCondo/termscondo.pdf">View PDF</a>        
  </div>
</div> -->

<!-- <iframe src="/images/TermsCondo/termscondo.pdf" ></iframe> -->
<!-- <embed src="/images/TermsCondo/termscondo.pdf" type="application/pdf" width="100%" height="100%"> -->

<object data="images/TermsCondo/termscondo.pdf" type="application/pdf" width="100%" height="100%">
   <p><b>Example fallback content</b>: This browser does not support PDFs. Please download the PDF to view it: <a href="/images/TermsCondo/termscondo.pdf">Download PDF</a>.</p>
</object>
</div>

</div>

  <?php include 'includes/core/footer_in_full.php';?>
</body>
</html>

                        <!-- test test -->
 <!--                        <?php
  // $file = 'images/TermsCondo/termscondo.pdf';
  // $filename = 'termscondo.pdf';
  // header('Content-type: application/pdf');
  // header('Content-Disposition: inline; filename="' . $filename . '"');
  // header('Content-Transfer-Encoding: binary');
  // header('Accept-Ranges: bytes');
  // @readfile($file);
?>
         -->                <!-- beginnning of the collapsible content box -->
                        <!-- <div id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Section 1A
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <p>1a. It is assumed a full and comprehensive inventory of goods to be transported has been provided and agreed as part of the contract and price quoted</p>
                  <ol>
                      <li>That it is possible for collection and delivery vehicle to access entrance within 5 meters of parking area.</li>
                      <li>That we have been information as to the number of flights of stairs and distance from vehicle has been correctly provided.</li>
                      <li>That no flammable or hazardous material is to be carried.</li>
                      <li>No paints, Oils gasses or similar items are included</li>
                      <li>All furniture that need disassembling is ready on the day</li>
                      <li>All appliances have been disconnected</li>
                      <li>Any items not on the original inventory will be charged separately (pro rata)</li>
                      <li>No reassemble (other than by agreement)</li>
                      <li>No packing or unpacking on the day that might cause a hold up on loading or unloading</li>
                      <li>Health and safety issues are discussed prior to contract</li>
                      <li>No boxes or bags weigh over 30kilo each</li>
                      <li>All items to be moved will be packed and wrapped prior to loading particularly glazed pictures mirrors etc.</li>
                      <li>Large items such as freezers wall/display cabinets pianos and any large bulky/heavy items have been itemized.</li>
                      <li>We are made aware of any particularly fragile or damaged items prior to loading</li>
                      <li>Due to health and safety laws it is not possible for operatives to remove footwear and any floor coverings must be secure and not form trips or hazard.</li>
                      <li>Access to delivery address is available within two hours of collection time</li>
                      <li>Waiting time is charged at £30 per hour</li>
                      <li>Payment is in cash on completion</li>
                      <li>Receipt for payment on the day (hand written) full invoice if requested by HM mail or E Mail</li>
                      
                  </ol>
                  <hr>

                  <p>These terms and conditions are based on a standard legal document used by removals companies throughout the UK. As such we believe they are fair and reasonable for our <b>Jayegroup</b> however they are also legally binding and should not be entered into lightly.</p>

                  <ol>
                      
                    <li>This Agreement shall be interpreted and construed exclusively under the laws of England, Wales, Scotland and Ireland and the laws of England shall prevail in the event of any conflict of law. You hereby consent to such jurisdiction.</li>
                    <li>These terms and conditions of business set out the basis upon which Jayegroup will arrange removals and/or storage of your goods with ourselves or with a nominated Removals Contractor on your behalf. They contain provisions which exclude and/or limit Jayegroup liability for loss and/or damage to your goods and also set out provisions governing insurance of those goods.</li>
                    <li>Where these conditions use the word ‘you’ or ‘your’ it means you as the removal customer, Jayegroup means Jayegroup as defined at the end of this page or a Removals Contractor referred by us whose services you accept. You are particularly referred to clause 3 (regarding insurance) clause 8 (exclusions and limitations of liability) and clause 12 (for damages to premises or property other than the removed goods). The removal conditions also contain time limits for claiming against Jayegroup (see clause 10).</li>
                  </ol>

                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      The Quotation
                    </a>
                  </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <p>The removal quotation issued by Jayegroup (“the quotation”) is a fixed price. Unless otherwise stated, it does not include customs duties and inspections or any other fees payable to government bodies. Jayegroup may change the quoted price for the removal or make additional charges if any of the following have not been taken into account when preparing the quotation or, if separately confirmed by Jayegroup in writing:-</p>

                  <ol>
                      <li>You do not accept the quotation in writing within 28 days, providing at the time of acceptance a firm removal date which Jayegroup agrees in writing.</li>
                      <li> By reason of your delay, the removal is not carried out or completed within three months of the date of acceptance of the quotation.</li>
                      <li>Jayegroup’s costs increase (or decrease) because of currency fluctuations (where applicable) or changes in taxation or freight charges for reasons beyond their control.</li>
                      <li>The work is carried out on a Weekend or Bank Holiday at your request.</li>
                      <li>Jayegroup has to collect or deliver goods from/to above the ground and first upper floor of a property.</li>
                      <li>Jayegroup supply any additional services not included or requested to be included in the quotation, including moving or storing extra goods (these conditions will apply to such work in any event).</li>
                      <li>The stairs, lifts or doorways at the property are inadequate for free movement of the goods without the need for mechanical equipment or structural alteration, or the approach, road or drive to the property is unsuitable for Jayegroup’s vehicles and/or containers to get to and load and/or unload within 20 metres of the doorway, and as a result Jayegroup have to carry out extra work not included within the quotation.</li>
                      <li> Any parking or other fees or charges that have to be paid by Jayegroup in order to carry out the removal services on your behalf.</li>
                      <li> There are delays or events outside Jayegroup reasonable control.</li>
                      <li>Jayegroup are asked to agree in writing to increase their limit of liability (as set out in clause 8 of these conditions).</li>
                      <p>In all these circumstances a revised quotation will be put forward and, if agreed, you will pay the adjusted charges.</p>

                  </ol>

                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Work not included in the quotation
                    </a>
                  </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                  Unless agreed by Jayegroup, in writing, they will not, as part of the quoted removal services:
                  <ol>
                      
                      <li>Dismantle or assemble unit or system furniture, fitments or fittings.</li>
                      <li>Disconnect or reconnect appliances, fixtures, fittings or equipment.</li>
                      <li>Take up or lay fitted floor coverings.</li>
                      <li>Move storage heaters, unless they are dismantled.</li>
                      <li>Move items from a loft or cellar, unless properly lit, and floored and safe access is provided.</li>
                      <li>Move or store any items excluded under clause 4 of these conditions.</li>


                  </ol>
                  <p>Jayegroup staff will not be authorised or qualified to carry out such work. It is recommended that a properly qualified person is separately employed by you to carry out these services, if required.</p>

                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      Your responsibility
                    </a>
                  </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                  It will be your sole responsibility to:

                  <ol>
                      
                        <li>Declare to Jayegroup the value of the goods being removed and/or stored.</li>
                        <li>If any insurance cover offered by Jayegroup in the quotation and is not accepted (and paid for in advance of the start of the removal) arrange adequate insurance cover for the goods submitted for removal, and/or storage, against all insurable risks,</li>
                        <li>Obtain at your own expense, all documents, permits, licences, and/or customs documents necessary for the removal to be completed.</li>
                        <li>Be present or represented throughout the removal.</li>
                        <li>Take all reasonable steps to ensure that nothing that should be removed is left behind and nothing is taken away in error.</li>
                        <li>Arrange proper protection for goods left in unoccupied or unattended premises, or where other people such as (but not limited to) tenants or workmen are, or will be present.</li>
                        <li>Prepare and properly stabilise all appliances or electronic equipment prior to their removal.</li>
                        <li>Empty, properly defrost and clean refrigerators and deep freezers. Jayegroup is not responsible for the contents of this equipment.</li>
                        <li>Provide House to Home and Jayegroup with a contact address for correspondence during removal, and/or storage of goods.</li>
                        
                  </ol>
                  <p>Other than by reason of Jayegroup negligence, they will not be liable for any loss or damage, costs or additional charges that may arise from any of these matters.</p>

                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      Goods not to be submitted for removal or storage
                    </a>
                  </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                  The following items are specifically excluded from this contract unless otherwise agreed as below.
                  <ol>
                      
                    <li>Jewellery, watches, trinkets, precious stones or metals, money, deeds, securities, stamps, coins, or goods or collections of any similar kind.</li>
                    <li>Prohibited or stolen goods, drugs, potentially dangerous, damaging or explosive items, including gas bottles, aerosols, paints, firearms and ammunition</li>
                    <li>Plants or goods likely to encourage vermin or other pests and/or plants or goods likely to cause infestation.</li>
                    <li>Refrigerated or frozen food, drink, products or goods.</li>
                    <li>Any animals and their cages or tanks including pets, birds or fish.</li>
                    <li>Goods which require a special licence or government permission for export or import.</li>
                  </ol>

                  Such goods will not be removed by Jayegroup except with their prior written agreement. In the event that they do remove such goods, Jayegroup will not accept liability for loss or damage wholly or mainly attributable to the special nature of the goods concerned. If you submit such goods without Jayegroup’s knowledge and prior written agreement they will not be liable for any loss or damage and you will indemnify Jayegroup against any charges, expenses, damages or penalties claimed against them. In addition, Jayegroup shall be entitled to dispose of (without notice) any such goods which are listed under paragraphs 4(b), 4(c) or 4 (d).
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSix">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                      Ownership of the goods
                    </a>
                  </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                  By entering into this contract, you declare that:
                  <ol>
                      <li>The goods to be removed and/or stored are your own property, or</li>
                      <li>The person(s) who own or have an interest in them, have given you authority to make this contract, and have been made aware of these conditions.
            You will meet any claim for damages and/or costs against Jayegroup if these declarations are not true.</li>
                  </ol>

                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSeven">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                      Charges if you postpone or cancel the removal
                    </a>
                  </h4>
                </div>
                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                  If you postpone or cancel this contract, Jayegroup may charge according to how much notice is given. Notification must be in writing by recorded delivery or electronic medium that can provide proof of time and date stamp.
                  <ol>
                      <li>More than 14 working days before the removal was due to start: Nothing payable.</li>
                      <li>Less than 14 working days, but more than 7 working days before the removal was due to start: 30% of the full removal charge.</li>
                      <li>Less than 7 working days, but more than 2 working days before the removal was due to start: 60% of the full removal charge.</li>
                      <li>ithin 48 hours of the start of the removal, 100% of the full removal charge (the start of the removal is viewed as the first day that the removal crew are due to be present at the property)</li>
                    
                  </ol>

                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingEight">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                      Paying for the Removal
                    </a>
                  </h4>
                </div>
                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                  Unless otherwise agreed by Jayegroup in writing:
                  <ol>
                      
                      <li>Payment is required, by cleared funds in advance of the removal or storage period.</li>
                      <li>You may not withhold any part of the agreed price.</li>
                      <li>We accept most major Debit and Credit cards via PayPal.</li>
                  </ol>

                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingNine">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                      Our liability for loss or damage
                    </a>
                  </h4>
                </div>
                <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                 For the purposes of this clause, reference to an item is reference to any one article, suite, pair, set, complete case, carton, package or other container.
                 <ol>
                     <li>Jayegroup liability for loss or damage is limited. This is set out in clause 8(a)(i) below. You may request Jayegroup increase their liability, as set out in clause 8(b) below. This is subject to their express written agreement in advance of carrying out the removal and/or storage and payment of any additional charges.
                            <ol>
                                <li>In the event of Jayegroup losing or damaging your goods, if they are liable, we will pay you up to a maximum sum in line with their insurance (currently £20,000) subject to the normal Insurance Company terms and conditions. If you require a higher cover you must arrange it (or we can help with this). Cover in Europe is not included and must be arranged separately.
            OR</li>
                                <li>ayegroup, in their sole discretion, may choose to repair or replace the damaged item. If an item is repaired Jayegroup will not be liable for depreciation in value.</li>
                            </ol>
                     </li>
                     <li>Subject to receiving your itemised (and valued) inventory with a request in writing for Jayegroup to increase their liability (above that set out in clause 8(a) above), prior to the removal and/or storage commencing, Jayegroup may offer to extend their maximum liability to the value declared by you to them. An additional charge for the removal/storage is likely. This is not an insurance on the goods and you are strongly advised to accept the insurance offered in Jayegroup quote (if any) or if arranging insurance cover yourself, you are advised to show this contract to your insurance company.</li>
                     <li>Other than by reason of Jayegroup negligence, they will not be liable for any loss, damage or failure to produce or deliver the goods if this is caused by one or other of the circumstances set out in the following:
                                <ol>
                                    
                                    <li>By fire, howsoever caused.</li>
                                    <li>By war, invasion, acts of foreign enemies, hostilities (whether war is declared or not), civil war, terrorism, rebellion and/or military coup, Act of God, industrial action or other such events outside Jayegroup’s reasonable control.</li>
                                    <li>By normal wear and tear, natural or gradual deterioration, leakage or evaporation or from perishable or unstable goods. This includes goods left within furniture or appliances.</li>
                                    <li>By moth or vermin or similar infestation.</li>
                                    <li>By cleaning, repairing or restoring unless Jayegroup did the work.</li>
                                    <li>To any goods in wardrobes, drawers or appliances, or in a package, bundle, case or other container not both packed and unpacked by Jayegroup.</li>
                                    <li>For electrical or mechanical derangement to any appliance, instrument or equipment unless there is evidence of external impact.</li>
                                    <li>To jewellery, watches, trinkets, precious stones or metals, money, deeds, securities, stamps, coins, or goods or collections of a similar kind, howsoever caused, unless you have previously given Jayegroup full particulars with value, and they have confirmed in writing that they accept responsibility as in conditions 8(a) or 8(b) above.</li>
                                    <li>To any goods which have a relevant proven defect or are inherently defective.</li>
                                    <li>To animals and their cages or tanks including pets, birds or fish.</li>
                                    <li>To plants.</li>
                                    <li>To refrigerated or frozen food, drink, products or goods.</li>
                                </ol>
                         </li>
                     <li>Other than by reason of Jayegroup negligence, we will not be liable for damages or costs resulting indirectly from, or as a consequence of loss, damage, or failure to produce the goods. </li>
                     <li>No employee of Jayegroup shall be separately liable to you for any loss, damage, mis-delivery, errors or omissions under the terms of this contract.</li>
                     <li></li>
                     <li></li>
                     <li></li>
                     <li></li>

                 </ol>

                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTen">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                      Extra conditions
                    </a>
                  </h4>
                </div>
                <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                  Extra conditions that apply to removals to/from a foreign country outside the United Kingdom: For goods destined to, or received from a place outside the United Kingdom.
                  <ol>
                      
                      <li>Jayegroup will accept liability only for loss or damage to goods when they are in our actual possession, and if it can be proven that they were negligent. In such circumstances, Jayegroup will accept liability as in condition 8(a)(i) or 8(b) above. Jayegroup is not liable for any loss, damage or failure to produce the goods if it is caused by those circumstances set out in condition 8(c) above.</li>
                      <li>Where Jayegroup engages an international transport operator, shipping company or airline, to convey your goods to the place, port or airport of destination, they do so on your behalf and subject to the terms and conditions set out by that carrier. By agreeing to these terms you confirm their availability to appoint such party on your behalf.</li>
                      <li>If the carrying vessel/conveyance, should for reasons beyond the carrier’s control, fail to deliver the goods, or route them to a place other than the original destination, you may have limited recourse against the carrier, and may be liable for General Average contribution (e.g. the costs incurred to preserve the vessel/conveyance and cargo) and salvage charges (eg. charges payable to a person who saves those goods), or the additional cost of onward transmission to the place, port or airport of destination. These are insurable risks and if appropriate it is your responsibility to arrange adequate marine/transit insurance cover. These risks will not be insured by Jayegroup.</li>
                      <li>Jayegroup does not accept liability for goods confiscated, seized or removed by Customs Authorities or other Government Agencies.</li>
                      <li>All loads that are based on a shared container or shared vehicle are subject to additional terms, delivery times cannot be guaranteed and any dates or times given should be used for the purpose of guidance only. If time scales are quoted in days then this is calculated on working days and excludes Saturday, Sunday or any bank holiday in both the UK and country of delivery.</li>
                      <li></li>
                  </ol>

                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingEleven">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                      Time limit for claims
                    </a>
                  </h4>
                </div>
                <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                <ol>
                    <li>For goods which Jayegroup delivers, you must note any visible loss, damage or failure to produce any goods at the time of delivery.</li>
                    <li>If you or your agent collects goods from Jayegroup’s warehouse, you must note any loss or damage at the time the goods are handed to you, requesting that Jayegroup acknowledges and confirms your note</li>
                    <li>Notwithstanding clause 8, Jayegroup will not be liable for any loss of or damage to the goods unless a claim is notified to us (or their foreign correspondent if condition 9 applies) in writing as soon as such loss or damage is discovered (or with reasonable diligence ought to have been discovered) and in any event within seven (7) days of delivery of the goods by Jayegroup, as the case may be.</li>
                    <li>The time limits referred to in clauses 10(a), 10(b) and 10 (c) above shall be essential to the contract.</li>
                    <li>Upon your written request, Jayegroup may at their discretion agree to extend your time for compliance with clause 10 (c), PROVIDED your request is received within the time limits provided for above. Subject to this provison Jayegroup will not unreasonably refuse such a request.</li>
                    <li></li>
                </ol>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwelve">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                      Delays in transit
                    </a>
                  </h4>
                </div>
                <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
                <ol>
                    <ul>Other than by reason of Jayegroup negligence, they will not be liable for delays in transit.</ul>
                    <ul>If through no fault of Jayegroup, they are unable to deliver your goods, e.g. late arrival of keys to the property/ies preventing completion of the removal service on the allocated day, will take them into store. The contract will then be fulfilled and any additional service(s), including storage and delivery, will be at your expense.</ul>
                    <ul>If through no fault of Jayegroup, they are unable to complete the removal services on the stated delivery date due to delay on your part, Jayegroup may be entitled to ask for additional charges, such as for extra waiting time.</ul>
                </ol>
                 
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThirteen">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                      Damage to premises or property other than goods
                    </a>
                  </h4>
                </div>
                <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                    <ol>
                        <li>Jayegroup will not be liable for any damage to premises or property other than goods submitted for removal and/or storage unless they have been negligent.</li>
                        <li>If Jayegroup causes damage as a result of moving goods under your express instruction, against our advice, and to move the goods in the manner instructed will inevitably cause damage, they shall not accept that they were negligent.</li>
                        <li>If Jayegroup is responsible for causing damage to your premises or to property other than goods submitted for removal and/or storage, you must note this on the worksheet or delivery receipt. This is essential to the contract.</li>
                    </ol>
              
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFourteen">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                      Right to Hold the Goods (lien)
                    </a>
                  </h4>
                </div>
                <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
                  Jayegroup shall have a right to withhold and/or ultimately dispose of some or all of the goods until you have paid all Jayegroup’s charges and any other payments due under this or any other contract. These include any charges that they have paid out on your behalf. Whilst Jayegroup hold the goods without payment you will be liable to pay all storage charges and other costs incurred as a result of them withholding your goods and these removal/storage terms and conditions shall continue to apply.
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFifteen">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                      Disputes
                    </a>
                  </h4>
                </div>
                <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
                  If there is a dispute arising from this agreement, which cannot be resolved by agreement or any applicable Alternative Dispute Resolution Scheme, either you or Jayegroup, may refer it to Arbitration with disputes being determined by an arbitrator appointed by the Chartered Institute of Arbitrators. The identity of the Arbitrator to be agreed between you and Jayegroup. This contract to be subject to the law of the Country in which Jayegroup’s principal place of business is situated.
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSixteen">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                      Sub-contracting the work
                    </a>
                  </h4>
                </div>
                <div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixteen">
                <ol>
                    
                    <li>Jayegroup reserves the right to sub-contract some or all of the work.</li>
                    <li>If Jayegroup sub-contracts, then these conditions will still apply.</li>
                </ol>
                  
                </div>
              </div>

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSeventeen">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
                      Route and method
                    </a>
                  </h4>
                </div>
                <div id="collapseSeventeen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventeen">
                <ol>
                    <li>Jayegroup has the full right to choose the route for delivery.</li>
                    <li>Unless it has been specifically agreed in writing on the Quotation, other space/volume/capacity on Jayegroup’s vehicles and/or the container may be utilised for consignments of other customers.</li>
                </ol>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingEighteen">
                  <h4 class="panel-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEighteen" aria-expanded="false" aria-controls="collapseEighteen">
                      Advice and information
                    </a>
                  </h4>
                </div>
                <div id="collapseEighteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighteen">
                  Advice and information in whatever form it may be given is provided by Jayegroup for their customer only. Any oral advice given without special arrangement is provided gratuitously and without contractual liability.
                </div>
              </div>
              
            </div>-->
                        <!-- end of the collapsible box content -->


    <!-- </div> -->
       <!--  <div class='row '>
            <div class='col-md-6 service-text'>
				<p>   A company you can rely on. We have been working in the motorbike industry for over 6 years with hands-on experience with a vast amount of bikes ranging from mopeds to ninjas to superbikes. All collected and delivered safely.<br>
                We cater for private customers and businesses bringing a great ethic support of confidence due to a high demand of customers.<br>
                We can tailor our services to meet your requirements. Give our friendly staff a call and we will be more than happy to assist you.<br>
					
				<a href="booking-form.php?action=motorbike_quote">Get a quote </a>	
				</p>
				<h4>We Cover</h4>
				
				<ul>
					<li><i class="fa fa-check "></i>Motorbike Recovery</li>
					<li><i class="fa fa-check "></i>Any location within and outside London</li>
					
				</ul>
			
			</div>
            <div class='col-sm-5 col-md-offset-1'>
                <div class="controls clearfix">
                    <div class="controls pull-right clearfix">
                        <a href="">
                            <span id="arrowNext">
                            </span>
                        </a>
                        <a href="">
                            <span id="arrowPrev">
                            </span>
                        </a>
                    </div>
                </div>
                <article class="blog-post  clearfix">
                    <figure id="folioGallery">
                        <img class="img-responsive" src="images/motorbike/bike-2.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/motorbike/bike-1.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/motorbike/bike-3.jpg" title="Image title" alt="alternative information">
                        
                    </figure>
                </article>
			</div>		
		</div> -->
<!-- </div>

	<!-- <?php 
  // include 'includes/core/footer_in_full.php';?> -->
<!-- </body>
</html> -->
 