<?php 

include 'config.php'

?>

<!Doctype html>
<!--[if IE 8]> <html class="ie ie8"> <![endif]-->
<!--[if IE 9]> <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $meta_title ;?></title>
	<meta name="description" content="<?php echo $meta_description;?>"/>
	<meta name="keywords" content="<?php echo $meta_keywords;?>"/> 	
	<meta property="og:image" content="<?php echo $og_meta_image;?>" />
	<meta property="og:title" content="<?php echo $og_meta_title;?>" />
	<meta property="og:description" content="<?php echo $og_meta_description;?>" />
	<meta property="og:url" content="<?php echo $og_url;?>" />
	<meta property="og:site_name" content="<?php echo $site_name;?>" />	
  <meta name="viewport" content=" width=device-width, initial-scale=1.0 ">
  <meta charset="utf-8">

	<?php echo $add_styles ;?>

	<meta name="Geography" content="67-68 Hatton Garden, City of London , EC1N 8JY "/>
	<meta name="Language" content="English"/>
	<meta name="Copyright" content="jayegroup"/>
	<meta name="Designer" content="www.creative-webuk.com"/>
	<meta name="distribution" content="local"/>
	<meta name="Robots" content="INDEX,FOLLOW"/>
	<meta name="city" content="London"/>
	<meta name="country" content="UNITED KINGDOM"/>	
	
    <link rel="shortcut icon" href="images/fav.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<?php echo $root_url;?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $root_url;?>css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo $root_url;?>css/style.css" id="colorStyle">
    <link rel="stylesheet" href="<?php echo $root_url;?>masterslider/style/masterslider.css" />
    <link href="<?php echo $root_url;?>masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>
    <link href='<?php echo $root_url;?>masterslider/staffcarousel/style/ms-staff-style.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo $root_url;?>css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $root_url;?>css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/abobjax/libs/owl-carousel/1.3.2/owl.carousel.css">
    <link rel="stylesheet" href="owl-carousel/owl.theme.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!--[if gte IE 8]>
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300" /> 
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400" /> 
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:600" /> 
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:700" /> 
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:800" />
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="./js/html5shiv.js"></script>
      <script src="./js/respond.js"></script>
    <![endif]-->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="masterslider/masterslider.min.js"></script>
    <script src="masterslider/staffcarousel/js/masterslider.staff.carousel.js"></script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51413549-7', 'jayegroup.com');
  ga('send', 'pageview');

</script>
	
</head>