<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="AboutUs">
                        <div class="FooterSiteBrand">
                            <!--<img src="./img/footer-logos/f-logo.png" title="Image title" alt="alternative information">-->
                        </div>
 				<p>Jayegroup Ltd was established in 2011 and since then we have grown as a company and have gained extensive skills with dealing with our customers. </p>
				<br/>
<p>We have developed a great relation with our repeated customers as our principal is to always deliver the best service on time.</p>
<p>We are proud to offer a wide range of services including house and office removals, same day delivery, logistics, short term storage solution and many more.</p>
		<br/>
<p>Our staff are committed to always put customers first by tailoring our services to their individual needs.</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="CommunicationPath">
                        <ul class="media-list">
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <i class="fa-location"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Head Office Address</h4>
                                  67-68 Hatton Garden, City of London , EC1N 8JY 
                                </div>
                            </li>
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <i class="fa-phone"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Phone Number</h4>
											020 8778 2389
                                </div>
                            </li>
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <i class="fa-paper-plane"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Email Address</h4>
                                   info@jayegroup.com
                                    
                                </div>
                            </li>
                            <li class="media">
                                <a class="pull-left" href="#">
                                    <i class="fa-clock"></i>
                                </a>
                                <div class="media-body">
									<ul>
										<li>Monday-Friday        9 am- 5 pm</li>
										<li>Saturday                  9 am-1 pm</li>
										<li>Sunday                     closed</li>
										<li>24 hr call out service available</li>
									</ul>
   
                                </div>
                            </li>							
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</footer>
    <div class="PrivacySection">
        <div class="container">
            <div class="row">
                <ul>
                    <li><a class="btn  " href="/terms-conditions.php">Terms and Conditions</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="CopyRightWrapper">
        <div class="container">
            <div class="row">
                <div class="CopyRight">
                    <span><!--Maintained By <a href=#' rel="nofollow" title="Jayegroup Ltd" style="color:#fff;" target='_blank'>Jayegroup Ltd</a>--></span>
                </div>
            </div>
        </div>
    </div>