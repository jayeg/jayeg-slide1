<div class="container"> 
    <!-- //beginning of added div -->
  <div class="thecontent col-xs-12 col-sm-12">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
    <!-- Indicators --> 
       <!--  <ol class="carousel-indicators"> 
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active">
                
            </li> 
            <li data-target="#carousel-example-generic" data-slide-to="1">
                
            </li> 
            <li data-target="#carousel-example-generic" data-slide-to="2">
                
            </li> 
            <li data-target="#carousel-example-generic" data-slide-to="3">
                
            </li>
        </ol>  -->
        <div class="testimonial-head col-xs-12 headerwrap"><h1>Hear From Our Customers</h1><br></div>
        <!-- Wrapper for slides --> 
        <div class="carousel-inner "> 
            <div class="item active"> 
                <div class="row"> 
                    <div class="col-xs-12"> 
                        <div class="thumbnail adjust1 "> 
                            
                            <div class="col-md-10 col-sm-10 col-xs-12 "> 
                                <div class="caption"> <h2 class="text-info lead adjust2 testimonialhead">Fantastic service provided by this company</h2> <p class="testimonial-text"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> We had very stressful time moving houses and thanks to friendly and professional approach of the staff- we managed to do it within one day.
                                Job really well done.<br><span style="color:#C50A0A; float:right"> - Tony Francis</span>
                                </p> 
                                 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
            <div class="item"> 
                <div class="row"> 
                    <div class="col-xs-12"> 
                        <div class="thumbnail adjust1"> 
                             
                            <div class="col-md-10 col-sm-10 col-xs-12"> 
                                <div class="caption"> <h2 class="text-info lead adjust2 testimonialhead">Definitely worth recommending</h2> 
                                    <p class="testimonial-text"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> I would like to say big thank you  to Jayegroup for the delivery of  our flight cases to London Excel Exhibition. Everything has been done before time and the staff were on board all the time.<br><span style="color:#C50A0A; float:right"> - Mark Walker</span></p>   
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
            <div class="item"> 
                <div class="row"> 
                    <div class="col-xs-12"> 
                        <div class="thumbnail adjust1"> 
                            <!-- Germaine has always shown a conscientious attitude and his attention to detail is clear from the smallest job  up to the largest event. With competitive pricing and exemplary service working with JAYEGROUP makes perfect sense. -->
                            <div class="col-md-10 col-sm-10 col-xs-12"> 
                                <div class="caption"> 
                                    <h2 class="text-info lead adjust2 testimonialhead">Competitive pricing and Exemplary service</h2> 
                                    <p><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 
                                    Having dealt with JAYEGROUP on a number of occasions I have found them to be reliable, friendly and above all professional.<br> <span style="color:#C50A0A; float:right"> - Andy</span></p> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
            <div class="item"> 
                <div class="row"> 
                    <div class="col-xs-12"> 
                        <div class="thumbnail adjust1 "> 
                            <!-- because of the attention to detail to ensure that our equipment gets delivered safely and going the extra mile to make sure everything is done to a high standard. -->
                            <div class="col-md-10 col-sm-10 col-xs-12"> 
                                <div class="caption"> 
                                    <h2 class="text-info lead adjust2 testimonialhead">Very friendly and no job is too big or small for Jayegroup</h2> 
                                    <p><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 
                                    HHB/Source Distribution: Germaine is very reliable and hardworking individual who thrives on giving good service with a smile. I highly recommend using Jayegroup <br><span style="color:#C50A0A; float:right"> - Martin Diep</span></p>
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div>
        </div> 
        <!-- Controls --> 
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> </a> 
    </div> 
  </div>
</div>