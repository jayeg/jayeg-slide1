   <div class="svc-carousel-wrapp">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="owl-carousel" id="svcCarousel">
                        <a href="deliveries.php" data-toggle="tab">
                            <div class="service-item <?php if($page == 'Deliveries') echo 'active';?>">
                               <a href="deliveries.php" >   <i class="fa fa-check " style="font-size:60px;"></i></a>
                               <h4>Deliveries</h4> 
                            </div>
                        </a>
                        <a href="motorbike.php" data-toggle="tab">
                            <div class="service-item <?php if($page == 'motorbike') echo 'active';?>">
                               <a href="motorbike.php"> <i class="fa fa-check " style="font-size:60px;"></i> </a>
                             <h4>Motorbike Recovery</h4>
                            </div>
                        </a>
                        <a href="removals.php" data-toggle="tab">
                            <div class="service-item <?php if($page == 'Removals') echo 'active';?>">
                               <a href="removals.php"> <i class="fa fa-check " style="font-size:60px;"></i> </a>
                             <h4>Removals</h4>
                            </div>
                        </a>
                        <a href="disposals.php" data-toggle="tab">
                            <div class="service-item <?php if($page == 'Disposals') echo 'active';?>">
                                <a href="disposals.php"  > <i class="fa fa-check "style="font-size:60px;"></i></a>
                               <h4>Disposals</h4> 
                            </div>
                        </a>
                        <a href="storage.php" data-toggle="tab">
                            <div class="service-item <?php if($page == 'Storage') echo 'active';?>">
                              <a href="storage.php" >    <i class="fa fa-check " style="font-size:60px;"></i> </a>
                               <h4>Storage</h4>
                            </div>
                        </a>
                        <a href="logistics.php" data-toggle="tab">
                            <div class="service-item <?php if($page == 'Logistics') echo 'active';?>">
                            <a href="logistics.php" >        <i class="fa fa-check " style="font-size:60px;"></i></a>
                             <h4>Logistics</h4> 
                            </div>
                        </a>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>