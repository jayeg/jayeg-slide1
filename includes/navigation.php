<nav class="navbar navbar-default" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#FullWidthNavigationCollapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
       
    </div>
    <div class="navigation collapse navbar-collapse" id="FullWidthNavigationCollapse">
        <ul class="nav navbar-nav">
            <!-- <li><a href="<?php echo $root_url; ?>index<?php echo $extension ;?>" rel="follow" title="home" >HOME </a>
            </li> -->
            <li><a href="<?php echo $root_url; ?>index<?php echo $extension ;?>" rel="follow" title="home" >HOME </a>
            </li>
            <li><a href="about<?php echo $extension ;?>" rel="follow" title="about us">ABOUT US</a>
            </li>
           
             <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SERVICES<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="<?php echo $root_url; ?>deliveries<?php echo $extension ;?>">Deliveries</a></li>
		            <li><a href="<?php echo $root_url; ?>motorbike<?php echo $extension ;?>">Motorbike Recovery</a></li>
		            <li><a href="<?php echo $root_url; ?>removals<?php echo $extension ;?>">Removals</a></li>
		            <!-- <li role="separator" class="divider"></li> -->
		            <li><a href="<?php echo $root_url; ?>disposals<?php echo $extension ;?>">Disposals</a></li>
		            <!-- <li role="separator" class="divider"></li> -->
		            <li><a href="<?php echo $root_url; ?>storage<?php echo $extension ;?>">Storage</a></li>
		            <li><a href="<?php echo $root_url; ?>logistics<?php echo $extension ;?>">Logistics</a></li>
		          </ul>
       		 </li>
            <li class="dropdown"><a class="dropdown-toggle" href="<?php echo $root_url; ?>gallery<?php echo $extension ;?>" rel="nofollow" title="GALLERY">GALLERY </a> </li>
            <li class="dropdown"><a class="dropdown-toggle" href="<?php echo $root_url; ?>contact<?php echo $extension ;?>" rel="follow" title="CONTACT US"> CONTACT US </a> </li>
        </ul>
    </div>
</nav>