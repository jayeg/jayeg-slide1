<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'index';

		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		<link href="_layout/js/flexslider/flexslider.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/flexslider/jquery.flexslider.min.js" type="text/javascript"></script><!-- /// Tabs and Accordion //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		
		';
		include 'includes/site/core/settings/header.php';

	}	
?>
<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
        </div><!-- end #header -->
        <div id="content">
            <div class="flexslider index" id="flexslider-slider">
				<?php include 'includes/site/slider.php';?>
            </div><!-- end .flexslider -->
            <div class="row">
                <div class="span12">
                    <h1 class="text-center">We are <span>building</span>
                    the future.</h1>

                    <h5>Barrett Bros is a family run, building services organisation, incorporated in 1971. The founder members of the organisation are still involved in the management of the Company, and the original Managing Director, who formed the Company in 1971, is still in position. Long term continuity of management has ensured that our companies founding principles of excellent quality delivery at the workface, coupled with excellent customer service are adhered to.</h5>
                </div><!-- end .span1s2 -->
            </div><!-- end .row -->
            <div class="row">
                <div class="span12">
                    <h3 class="headline"><span>what we
                    <strong>do</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <div class="row">
                <div class="span3">
                    <div class="services">
                        <img alt="" src="_content/index/services/220x253-1.png"/>
                        <div class="hexagon">
                            <div class="hexagon-top"></div>
								<div class="hexagon-middle">
									<h3><a href="services.php">design/planning</a></h3>
								</div>
                            <div class="hexagon-bottom"></div>
                        </div><!-- end .hexagon -->
                    </div><!-- end .services -->
                </div><!-- end .span3 -->

                <div class="span3">
                    <div class="services">
                        <img alt="" src=
                        "_content/index/services/220x253-2.png" />

                        <div class="hexagon">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h3><a href="services.php">Building</a></h3>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .hexagon -->
                    </div><!-- end .services -->
                </div><!-- end .span3 -->

                <div class="span3">
                    <div class="services">
                        <img alt="" src=
                        "_content/index/services/220x253-3.png" />

                        <div class="hexagon">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h3><a href="services.php">Electrical</a></h3>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .hexagon -->
                    </div><!-- end .services -->
                </div><!-- end .span3 -->

                <div class="span3">
                    <div class="services">
                        <img alt="" src=
                        "_content/index/services/220x253-4.png" />

                        <div class="hexagon">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h3><a href="services.php">Mechanical</a></h3>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .hexagon -->
                    </div><!-- end .services -->
                </div><!-- end .span3 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline"><span>in
                    <strong>progress</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span7">
                    <h3 style="margin-bottom:40px;">Coming Soon</h3>
                    <p>Coming Soon</p>

                    <ul class="services-list">
                        <li>Coming Soon</li>


                    </ul><!--<a class="button-red" href="#">view project</a>-->
                </div><!-- end .span7 -->

                <div class="span5"><img alt="" class="services-img" src="_content/index/380x424.png" /></div><!-- end .span5 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline">
                    <span><strong>testimonials</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <div class="flexslider testimonial" id= "flexslider-testimonials">
                        <ul class="slides">
                            <li>
                                <div class="testimonial">
                                    <blockquote>
                                        <p>"Barrett Bros  have carried out work to three of my flats in London. During this time their team have given me first class service. There is always someone who answers your call and then deals with any issues or problems immediately. I have always been kept informed of issues if any additional work needed to be carried out. Barrett Bros also kept me updated with the current legislation required. Overall I am delighted with the service I receive as a Landlord." </p>
                                    </blockquote>
                                </div><!-- end .testimonial -->
                            </li>

                        </ul>
                    </div><!-- end .flexslider -->

                    <!--<div id="carousel">
                        <div class="carousel-item"><img alt="" id="item-1" src=
                        "_content/index/team/140x120-1.png" /></div>
                        <!-- end .carousel-item -->

                      <!--  <div class="carousel-item"><img alt="" id="item-2" src=
                        "_content/index/team/140x120-2.png" /></div>
                        <!-- end .carousel-item -->

                      <!--  <div class="carousel-item"><img alt="" id="item-3" src=
                        "_content/index/team/140x120-3.png" /></div>
                        <!-- end .carousel-item -->

                      <!--  <div class="carousel-item"><img alt="" id="item-4" src=
                        "_content/index/team/140x120-4.png" /></div>
                        <!-- end .carousel-item -->

                      <!--  <div class="carousel-item"><img alt="" id="item-5" src=
                        "_content/index/team/140x120-5.png" /></div>
                        <!-- end .carousel-item -->
                  <!--  </div><!-- end #carousel -->

                 <!--   <div class="carousel-nav">
                        <a href="#" id="prev" name="prev"><img alt="" src=
                        "_layout/js/waterwheel-carousel/images/left.png" /></a>
                        <a href="#" id="next" name="next"><img alt="" src=
                        "_layout/js/waterwheel-carousel/images/right.png" /></a>
                    </div><!-- end .carousel-nav -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline"><span>quick
                    <strong>view</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="portfolio-row fixed">

                <div class="portfolio">
                    <a href="external-remodelling-gallery.php">
					<img alt="" src="_content/index/portfolio/400x400-1.1.jpg" /></a>

                    <div class="portfolio-hover">
                        <h5>External Remodelling</h5>

                        <p>Description coming soon</p>
                    </div><!-- end .portfolio-hover -->

                    <div class="portfolio-hover-controls">
<a href="_content/index/portfolio/800x550-1.jpg" rel="imagebox"><img alt="" src="_layout/images/icon-zoom.png" /></a> 
<a href="external-remodelling-gallery.php"><img alt="" src="_layout/images/icon-link.png" /></a>
                    </div><!-- end .portfolio-hover-controls -->
                </div><!-- end .portfolio -->

                <div class="portfolio">
                    <a href="internal-refurbishment-gallery.php">
					<img alt="" src="_content/index/portfolio/400x400-2.1.jpg" /></a>

                    <div class="portfolio-hover">
                        <h5>Internal Refurbishment</h5>

                         <p>Description coming soon</p>
                    </div><!-- end .portfolio-hover -->

                    <div class="portfolio-hover-controls">
                        <a href="_content/index/portfolio/800x550-2.jpg" rel=
                        "imagebox"><img alt="" src=
                        "_layout/images/icon-zoom.png" /></a> <a href=
                        "internal-refurbishment-gallery.php"><img alt="" src=
                        "_layout/images/icon-link.png" /></a>
                    </div><!-- end .portfolio-hover-controls -->
                </div><!-- end .portfolio -->			
			
                <div class="portfolio">
                    <a href="steel-installation-gallery.php">
					<img alt="" src="_content/index/portfolio/400x400-3.jpg" /></a>

                    <div class="portfolio-hover">
                        <h5>Vaulted ceilings  and feature windows</h5>

                         <p>Description coming soon</p>
                    </div><!-- end .portfolio-hover -->

                    <div class="portfolio-hover-controls">
                        <a href="_content/index/portfolio/800x550-3.1.jpg" rel=
                        "imagebox"><img alt="" src=
                        "_layout/images/icon-zoom.png" /></a> <a href=
                        "steel-installation-gallery.php"><img alt="" src=
                        "_layout/images/icon-link.png" /></a>
						
                    </div><!-- end .portfolio-hover-controls -->
                </div><!-- end .portfolio -->

                <div class="portfolio">
                    <a href="portfolio.php">
					<img alt="" src="_content/index/portfolio/400x400-4.jpg" /></a>

                    <div class="portfolio-hover">
                        <h5>Electrical</h5>

                          <p>Description coming soon</p>
                    </div><!-- end .portfolio-hover -->

                    <div class="portfolio-hover-controls">
                        <a href="_content/index/portfolio/400x400-4.1.jpg" rel=
                        "imagebox"><img alt="" src=
                        "_layout/images/icon-zoom.png" /></a> <a href=
                        "portfolio.php"><img alt="" src=
                        "_layout/images/icon-link.png" /></a>
                    </div><!-- end .portfolio-hover-controls -->
                </div><!-- end .portfolio -->

                <div class="portfolio">
                    <a href="mechanical-gallery.php">
					<img alt="" src="_content/index/portfolio/400x400-5.jpg" /></a>

                    <div class="portfolio-hover">
                        <h5>Mechanical</h5>

                          <p>Description coming soon</p>
                    </div><!-- end .portfolio-hover -->

                    <div class="portfolio-hover-controls">
                        <a href="_content/index/portfolio/800x550-5.1.jpg" rel=
                        "imagebox"><img alt="" src=
                        "_layout/images/icon-zoom.png" /></a> <a href=
                        "mechanical-gallery.php"><img alt="" src=
                        "_layout/images/icon-link.png" /></a>
                    </div><!-- end .portfolio-hover-controls -->
                </div><!-- end .portfolio -->

            </div><!-- end .portfolio-row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->

        <div id="footer">
            <!-- /// FOOTER ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/blog_posts.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>