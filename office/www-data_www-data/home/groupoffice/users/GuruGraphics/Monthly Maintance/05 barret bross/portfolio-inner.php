<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = '';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		<script src="_layout/js/gmap/jquery.gmap.min.js" type="text/javascript"></script><!-- /// Waterwheel Carousel //////// -->
		<script src="_layout/js/flexslider/jquery.flexslider.min.js" type="text/javascript"></script><!-- /// Tabs and Accordion //////// -->
		<script src="_layout/js/accordion/jquery.accordion.min.js" type="text/javascript"></script>		
		<script src="_layout/js/tabify/jquery.tabify.min.js" type="text/javascript"></script><!-- /// Validity //////// -->
		<link href="_layout/js/validity/css.validity.css" rel="stylesheet" type="text/css" />
		<script src="_layout/js/validity/jquery.validity.js" type="text/javascript"></script><!-- /// gMap //////// -->
		<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>		
		';
		include 'includes/site/core/settings/header.php';
	}
?>

<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->

        <div id="content">
            <!-- /// CONTENT /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="page-header" style="background:url(_content/portfolio-inner/1920x485.jpg) no-repeat center center;">
            <div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>portfolio</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline alt-2"><span><strong>modern</strong>
                    art museum</span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span6">
                    <div class="portfolio alt-3">
                        <a href="_content/portfolio-inner/460x460.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/portfolio-inner/460x460.png" /></a>
                    </div><!-- end .portfolio -->

                    <div class="row">
                        <div class="span3">
                            <div class="portfolio alt-2">
                                <a href=
                                "_content/portfolio-inner/460x460.png" rel=
                                "imagebox"><img alt="" src=
                                "_content/portfolio-inner/460x460.png" /></a>
                            </div><!-- end .portfolio -->
                        </div><!-- end .span3 -->

                        <div class="span3">
                            <div class="portfolio alt-2">
                                <a href=
                                "_content/portfolio-inner/460x460.png" rel=
                                "imagebox"><img alt="" src=
                                "_content/portfolio-inner/460x460.png" /></a>
                            </div><!-- end .portfolio -->
                        </div><!-- end .span3 -->
                    </div><!-- end .row -->
                </div><!-- end .span6 -->

                <div class="span6">
                    <div class="portfolio alt-3">
                        <a href="_content/portfolio-inner/460x460.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/portfolio-inner/460x460.png" /></a>
                    </div><!-- end .portfolio -->

                    <div class="row">
                        <div class="span3">
                            <div class="portfolio alt-2">
                                <a href=
                                "_content/portfolio-inner/460x460.png" rel=
                                "imagebox"><img alt="" src=
                                "_content/portfolio-inner/460x460.png" /></a>
                            </div><!-- end .portfolio -->
                        </div><!-- end .span3 -->

                        <div class="span3">
                            <div class="portfolio alt-2">
                                <a href=
                                "_content/portfolio-inner/460x460.png" rel=
                                "imagebox"><img alt="" src=
                                "_content/portfolio-inner/460x460.png" /></a>
                            </div><!-- end .portfolio -->
                        </div><!-- end .span3 -->
                    </div><!-- end .row -->
                </div><!-- end .span6 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->

        <div id="footer">
            <!-- /// FOOTER ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
                    <h5 class="headline"><span>visit our blog for
                    <strong>latest articles</strong></span></h5>

                    <div class="blog-post">
                        <div class="date">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <p>06 aug</p>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div>

                        <div class="article">
                            <p><a href="#">Lorem ipsum dolor sit amet
                            adipiscing, consectetur adipiscing elit.</a></p>

                            <p>Phasellus eget turpis ut nulla gravida
                            scelerisque id nec libero. Praesent vestibulum enim
                            sollicitudin enim ullamcorper pretium.</p><a class=
                            "button-red" href="#">read more</a>
                        </div><!-- end .article -->
                    </div><!-- end. blog-post -->

                    <div class="blog-post">
                        <div class="date">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <p>02 aug</p>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div>

                        <div class="article">
                            <p><a href="#">Phasellus eget turpis ut nulla
                            gravida scelerisque id nec libero.</a></p>

                            <p>Cras ullamcorper massa eu tempus condimentum.
                            Quisque non condimentum leo. Donec
                            pellentesque.</p><a class="button-red" href=
                            "#">read more</a>
                        </div><!-- end .article -->
                    </div><!-- end. blog-post -->
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>