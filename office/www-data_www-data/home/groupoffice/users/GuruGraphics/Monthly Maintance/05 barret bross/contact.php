<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'contact';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		<script src="_layout/js/gmap/jquery.gmap.min.js" type="text/javascript"></script><!-- /// Waterwheel Carousel //////// -->
		<script src="_layout/js/flexslider/jquery.flexslider.min.js" type="text/javascript"></script><!-- /// Tabs and Accordion //////// -->
		<script src="_layout/js/accordion/jquery.accordion.min.js" type="text/javascript"></script>		
		<script src="_layout/js/tabify/jquery.tabify.min.js" type="text/javascript"></script><!-- /// Validity //////// -->
		<link href="_layout/js/validity/css.validity.css" rel="stylesheet" type="text/css" />
		<script src="_layout/js/validity/jquery.validity.js" type="text/javascript"></script><!-- /// gMap //////// -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYLXt56d6oFvHM6hR314iml-w7X2K7Kac&sensor=false" type="text/javascript"></script>		
		';
		include 'includes/site/core/settings/header.php';
	}
?>


<head>


</head>

<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->
        <div id="content">
            <div class="page-header" style="background:url(_content/contact/1920x485.jpg) no-repeat center center;">
            <div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>contact</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->

            <div class="map" id="google-map"></div>

            <div class="row">
                <div class="span12">
                    <div class="contact-hexagon fixed">
                        <div class="hexagon-top"></div>

                        <div class="hexagon-middle">
                            <h3>our address</h3>
				<?php
					if (empty($_POST) === false) {
					
						$errors = array ();
						
						$first_name 		= $_POST['first_name'];
						$email 				= $_POST['email'];
						$message 			= $_POST['message'];
					
							if (empty($errors) === true) {
										$subject 			= 'Website contact Form';
										$first_name 		= $_REQUEST['first_name'] ;
										$email 				= $_REQUEST['email'] ;
										$message 				= $_REQUEST['message'] ;
										$to 				= "donatas@creative-webuk.com";

										$comments = $_REQUEST['first_name' + 'email' + 'message'] ;
										
										$from 			= $_REQUEST['email'] ;
										$headers 		= "From:" . $from;
										
										$comments_BODY .= "Name: 			".$_POST["first_name"]."\n"; 
										$comments_BODY .= "email: 			".$_POST["email"]."\n"; 
										$comments_BODY .= "comments: 		".nl2br($_POST["message"])."\n\n";										
										
									mail($to,$subject,$comments_BODY,$headers);
									header('Location: contact.php?sent');
									exit();	
								}
						}
				?>
                            <ul class="contact">
							<?php
											if(isset($_GET['sent']) === true) {
											
												echo ' 
<li class="address">Thank you for contacting Barrett Bros.</li>
<li class="email">We have received your message safely and will endeavour to respond to you within the next 24 hours. <br/> Many thanks.</li>
<li class="phone">The Barrett Bros team </li>';
											} else {
											
												echo '                        <li class="address">198 Hither green lane<br />
						London , SE13 6QB </li>

                        <li class="email">barrettbros@btconnect.com</li>

                        <li class="phone">(020) 8 852 4271</li>';
											}
										?>								

                            </ul>
                        </div><!-- end .hexagon-middle -->

                        <div class="hexagon-middle alt">
                            <h3>send a message</h3>
                            <form  action="javascript:void(0);" class="comment-form alt" id="comment-form" action=""  method="POST">
                                <fieldset>
                                    <input id="name" name="name" onblur=
                                    "if(this.value=='')this.value='name';"
                                    onfocus=
                                    "if(this.value=='name')this.value='';"
                                    type="text" value="name" /> <input id=
                                    "email" name="email" onblur=
                                    "if(this.value=='')this.value='email address';"
                                    onfocus=
                                    "if(this.value=='email address')this.value='';"
                                    type="text" value="email address" /> 
                                    <textarea cols="10" id="message" name=
                                    "message" rows="5">
</textarea> <input name="submit" type="submit"
                                    value="send" /> <input name="clear" type=
                                    "reset" value="clear" />
                                </fieldset>
                            </form>
				
                        </div><!-- end .hexagon-middle -->

                        <div class="hexagon-bottom"></div>
                    </div><!-- end .contact-hexagon -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->

        <div id="footer">
            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->
                <div class="span4">
					<?php include 'includes/site/items/blog_posts.php';?>
                </div><!-- end .span4 -->
                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>