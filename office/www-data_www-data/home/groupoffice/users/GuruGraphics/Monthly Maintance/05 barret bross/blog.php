<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'blog';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		
		';
		include 'includes/site/core/settings/header.php';

	}
?>
<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->

        <div id="content">
            <!-- /// CONTENT /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="page-header" style=
            "background:url(_content/blog/1920x485.jpg) no-repeat center center;">
            <div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>blog</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline alt-2"><span>our latest
                    <strong>articles</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span9">
                    <div class="blog-post fixed">
                        <img alt="" src="_content/blog/300x250-1.png" />

                        <h3><a href="blog-inner.html">Beautiful interior design
                        inspiration.</a></h3>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus ullamcorper.
                        Sed eros nulla, dig. Nulla facilisi. Pellentesque
                        gravida eu odio et aliquet.</p>

                        <p>Cras facilisis ligula elit, nec fringilla tellus
                        consequat et. In hendrerit augue ac quam semper
                        viverra. Nam tincidunt scelerisque elit sed
                        lacinia.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus
                        ullamcorper.</p>

                        <p class="last">Aug 15, 2013 | <a href=
                        "#">Inspiration</a> | <a href="#">2
                        comments</a><a class="button-red" href=
                        "blog-inner.html">read more</a></p>

                        <div class="hexagon">
                            <div class="hexagon-left"></div>

                            <div class="hexagon-middle">
                                <h3>new</h3>
                            </div>

                            <div class="hexagon-right"></div>
                        </div><!-- end hexagon -->
                    </div><!-- end .blog-post -->

                    <div class="blog-post alt fixed">
                        <img alt="" src="_content/blog/300x250-2.png" />

                        <h3><a href="blog-inner.html">Latest work. Modern
                        house.</a></h3>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus ullamcorper.
                        Sed eros nulla, dig. Nulla facilisi. Pellentesque
                        gravida eu odio et aliquet.</p>

                        <p>Cras facilisis ligula elit, nec fringilla tellus
                        consequat et. In hendrerit augue ac quam semper
                        viverra. Nam tincidunt scelerisque elit sed
                        lacinia.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus
                        ullamcorper.</p>

                        <p class="last">Aug 06, 2013 | <a href="#">In
                        progress</a> | <a href="#">2 comments</a><a class=
                        "button-red" href="blog-inner.html">read more</a></p>
                    </div><!-- end .blog-post -->

                    <div class="blog-post fixed">
                        <img alt="" src="_content/blog/300x250-3.png" />

                        <h3><a href="blog-inner.html">Modern Bridge.</a></h3>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus ullamcorper.
                        Sed eros nulla, dig. Nulla facilisi. Pellentesque
                        gravida eu odio et aliquet.</p>

                        <p>Cras facilisis ligula elit, nec fringilla tellus
                        consequat et. In hendrerit augue ac quam semper
                        viverra. Nam tincidunt scelerisque elit sed
                        lacinia.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus
                        ullamcorper.</p>

                        <p class="last">Aug 01, 2013 | <a href="#">Styles</a> |
                        <a href="#">2 comments</a><a class="button-red" href=
                        "blog-inner.html">read more</a></p>
                    </div><!-- end .blog-post -->

                    <div class="blog-post alt fixed">
                        <img alt="" src="_content/blog/300x250-4.png" />

                        <h3><a href="blog-inner.html">Office building
                        facelift.</a></h3>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus ullamcorper.
                        Sed eros nulla, dig. Nulla facilisi. Pellentesque
                        gravida eu odio et aliquet.</p>

                        <p>Cras facilisis ligula elit, nec fringilla tellus
                        consequat et. In hendrerit augue ac quam semper
                        viverra. Nam tincidunt scelerisque elit sed
                        lacinia.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Integer vel erat id neque rhoncus
                        ullamcorper.</p>

                        <p class="last">Iul 25, 2013 | <a href="#">In
                        progress</a> | <a href="#">2 comments</a><a class=
                        "button-red" href="blog-inner.html">read more</a></p>
                    </div><!-- end .blog-post -->
                </div><!-- end .span9 -->

                <div class="span3">
                    <form action="" class="search">
                        <fieldset>
                            <input name="search" onblur=
                            "if(this.value=='')this.value='enter keyword here';"
                            onfocus=
                            "if(this.value=='enter keyword here')this.value='';"
                            type="text" value="enter keyword here" />
                            <input name="submit" type="submit" value="" />
                        </fieldset>
                    </form>

                    <h5 class="text-uppercase">categories</h5>

                    <ul class="categories alt">
                        <li><a href="#">Inspiration</a></li>

                        <li><a href="#">Interior Design</a></li>

                        <li><a href="#">Modern Architecture</a></li>

                        <li><a href="#">Styles</a></li>

                        <li><a href="#">In peogress</a></li>
                    </ul>

                    <h5 class="text-uppercase">text widget</h5>

                    <p class="text-widget">Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. Mauris eget quam orci. Quisque
                    porta varius dui, quis posuere nibh mollis quis. Mauris
                    commodo rhoncus porttitor. Maecenas et euismod elit. Nulla
                    facilisi. Vivamus lacus libero, ultrices non ullamcorper
                    ac, tempus sit amet enim. Consecteturlorem ipsum dolor sit
                    amet, consectetur adipiscing elit. Mauris eget quam orci.
                    Quisque porta varius dui, quis posuere nibh mollis
                    quis.</p>

                    <h5>Flickr</h5>

                    <ul class="gallery">
                        <li><a href="_content/blog/gallery/60x60-1.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-1.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-2.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-2.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-3.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-3.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-4.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-4.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-5.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-5.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-6.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-6.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-7.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-7.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-8.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-8.png" /></a></li>

                        <li><a href="_content/blog/gallery/60x60-9.png" rel=
                        "imagebox"><img alt="" src=
                        "_content/blog/gallery/60x60-9.png" /></a></li>
                    </ul>
                </div><!-- end .span3 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <ul class="pagination alt">
                        <li class="current"><a href="#">1</a></li>

                        <li><a href="#">2</a></li>

                        <li><a href="#">3</a></li>

                        <li><a href="#">4</a></li>

                        <li><a href="#">5</a></li>

                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->

        <div id="footer">
            <!-- /// FOOTER ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
                    <h5 class="headline"><span>visit our blog for
                    <strong>latest articles</strong></span></h5>

                    <div class="blog-post">
                        <div class="date">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <p>06 aug</p>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div>

                        <div class="article">
                            <p><a href="#">Lorem ipsum dolor sit amet
                            adipiscing, consectetur adipiscing elit.</a></p>

                            <p>Phasellus eget turpis ut nulla gravida
                            scelerisque id nec libero. Praesent vestibulum enim
                            sollicitudin enim ullamcorper pretium.</p><a class=
                            "button-red" href="#">read more</a>
                        </div><!-- end .article -->
                    </div><!-- end. blog-post -->

                    <div class="blog-post">
                        <div class="date">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <p>02 aug</p>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div>

                        <div class="article">
                            <p><a href="#">Phasellus eget turpis ut nulla
                            gravida scelerisque id nec libero.</a></p>

                            <p>Cras ullamcorper massa eu tempus condimentum.
                            Quisque non condimentum leo. Donec
                            pellentesque.</p><a class="button-red" href=
                            "#">read more</a>
                        </div><!-- end .article -->
                    </div><!-- end. blog-post -->
                </div><!-- end .span4 -->

                 <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>