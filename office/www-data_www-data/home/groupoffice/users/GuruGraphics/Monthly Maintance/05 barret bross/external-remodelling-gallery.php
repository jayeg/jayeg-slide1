<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'portfolio';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		<script src="_layout/js/gmap/jquery.gmap.min.js" type="text/javascript"></script><!-- /// Waterwheel Carousel //////// -->
		<script src="_layout/js/flexslider/jquery.flexslider.min.js" type="text/javascript"></script><!-- /// Tabs and Accordion //////// -->
		<script src="_layout/js/accordion/jquery.accordion.min.js" type="text/javascript"></script>		
		<script src="_layout/js/tabify/jquery.tabify.min.js" type="text/javascript"></script><!-- /// Validity //////// -->
		<link href="_layout/js/validity/css.validity.css" rel="stylesheet" type="text/css" />
		<script src="_layout/js/validity/jquery.validity.js" type="text/javascript"></script><!-- /// gMap //////// -->
		<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>		
		';
		include 'includes/site/core/settings/header.php';
	}
?>
<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->

        <div id="content">
            <!-- /// CONTENT /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="page-header" style=
            "background:url(_content/portfolio/1920x485.1.jpg) no-repeat center center;">
            <div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>portfolio</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->
            <div class="row">
                <div class="span12">
                    <h3 class="headline alt"><span>Home<strong>Remodelling</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span13">
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/1.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/1.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->					
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/2.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/2.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/3.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/3.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/4.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/4.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/5.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/5.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->		
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/6.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/6.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/7.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/7.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/8.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/8.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/9.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/9.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/10.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/10.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/11.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/11.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/12.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/12.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/13.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/13.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/14.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/14.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/15.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/15.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/16.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/16.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/17.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/17.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/18.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/18.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/19.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/19.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/20.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/20.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/21.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/21.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/22.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/22.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/23.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/23.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/24.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/24.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/25.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/25.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/26.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/26.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/27.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/27.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/28.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/28.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/29.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/29.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/30.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/30.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/31.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/31.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/32.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/32.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/33.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/33.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/34.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/34.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/35.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/35.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/36.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/36.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/37.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/37.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->				
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/38.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/38.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/39.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/39.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/40.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/40.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/41.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/41.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/42.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/42.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/43.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/43.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/44.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/44.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/45.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/45.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/46.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/46.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/47.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/47.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/48.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/48.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/49.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/49.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/50.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/50.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/51.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/51.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/52.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/52.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/53.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/53.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/external-remodeling/thumb/54.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/external-remodeling/54.jpg" rel="imagebox[Somerville]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->											
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <div class="row">
                <div class="span12">
                    <ul class="pagination">
                        <li class="current"><a href="external-remodelling-gallery.php">External Remodelling</a></li>
                        <li><a href="portfolio.php">Back</a></li>
                     <!--  <li><a href="#">&gt;</a></li> -->
                    </ul>
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->
        <div id="footer">
            <!-- /// FOOTER ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/blog_posts.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>