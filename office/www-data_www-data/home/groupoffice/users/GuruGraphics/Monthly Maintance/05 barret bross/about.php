<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'about';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		
		';
		include 'includes/site/core/settings/header.php';

	}
?>
<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->

        <div id="content">
            <!-- /// CONTENT /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="page-header" style="background:url(_content/about/1920x485.jpg) no-repeat center center;">
            <div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>about us</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline"><span>who we are</span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">


                <div class="span12">
                    <h2>Barrett Bros is a family run, building services organisation, incorporated in 1971.</h2>

                    <p>The founder members of the organisation are still involved in the management of the Company, and the original Managing Director, who formed the Company in 1971, is still in position. Long term continuity of management has ensured that our companies founding principles of excellent quality delivery at the workface, coupled with excellent customer service are adhered to.</p>

                    <p>
Barrett Bros have an excellent reputation for delivery of quality workmanship on site. We also recognise the importance of professional office administrative support of our site based activities, these two elements of our business complementing each other to create a superb contracting package. Barrett Bros have occupied our staffed offices since 1971, and our two full time administrators ensure any queries are dealt with professionally and efficiently. </p>

                    <p>Our professionally qualified surveyors and engineers are site and office based, and can provide a wide range of technical expertise, to ensure that your project is completed with our trade mark quality finish.</p>
<p>Barrett Bros recognise the importance of quality delivery, and our Managing Director’s, and entire Company and staff ethos is centred around this concept. Barrett Bros recognise that quality must also be verified externally and objectively, in order to be credible. Barrett Bros are members of the leading trade bodies (see Quality section), and monitor customer satisfaction levels on all jobs, as part of part of our National Quality Assurance System (ISO 9001:2000).</p>					
<p>Barrett Bros original core business activity was Electrical Contracting, and in 1971 Barrett Bros became members of the NICEIC, and the ECA, these bodies being universally recognised as the benchmark of quality Electrical Contracting. Within the last 35 years, Barrett Bros have branched out into other activities, and now undertake a wide variety of works (see services section), for an extremely diverse client base (see clients section). </p>					
<p>The growth of our company has been organic, and managed prudently by the original founders, to ensure all projects are managed personally by our in house surveyors, and our quality reputation is maintained. Please take your time perusing our website, you will find a small sample of the thousands of varied and interesting projects we have undertaken during the last three decades. We hope you consider using Barrett Bros for any projects that you may be contemplating, and our qualified in house Surveyors and Engineers would welcome the opportunity to discuss any such projects with you personally.</p>			
                </div><!-- end .span8 -->
            </div><!-- ned .row -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline"><span>meet <strong>our
                    team</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
                    <div class="team-member">
                        <img alt="" src="_content/about/team/220x222-1.png" />
                        <h5>Chris Barrett</h5>
                        <p>Director</p>
                        <p>Chris is the company director, and is degree qualified building surveyor. Before coming to Barrett Bros, Chris worked for many years in various Chartered Surveying practices, and has project managed several high profile refurbishment programmes.  Chris is also the Qualifying manager, under the NICEIC accreditation, and, is a qualified electrician in his own right. This broad knowledge base, ensures works are quoted accurately, and that jobs are managed correctly.</p>
                    </div><!-- end .team-member -->
                </div><!-- end .span3 -->
                <div class="span4">
                    <div class="team-member">
                         <img alt="" src="_content/about/team/220x222-3.png" />
                        <h5>Paul Stevenson</h5>
                        <p>Mechanical Engineer</p>
                        <p>Paul is a time served, mechanical engineer, and has worked on many commercial mechanical and electrical contracts, including The Glades shopping centre, Bluewater, Lakeside, and several hospital sites. Paul manages the mechanical / gas side of the business, and has vast experience of jobs ranging from the very small (new boiler installation) to the very large (complete new mechanical installation of schools, police stations etc).</p>
                    </div><!-- end .team-member -->
                </div><!-- end .span3 -->
                <div class="span4">
                    <div class="team-member">
					        <img alt="" src="_content/about/team/220x222-2.png" />
               
                        <h5>Anne Holiday</h5>
                        <p>Office manager</p>
                        <p>Anne has worked at Barrett Bros for 30 years, and knows the business inside out. Anne runs a very tight ship, and basically organises jobs, materials, and makes sure people are where they are supposed to be.</p>
                    </div><!-- end .team-member -->
                </div><!-- end .span3 -->
 
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->
        <div id="footer">
            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->
                <div class="span4">
					<?php include 'includes/site/items/blog_posts.php';?>
                </div><!-- end .span4 -->
                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>