<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'services';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		
		';
		include 'includes/site/core/settings/header.php';

	}	
?>
<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->

        <div id="content">
            <!-- /// CONTENT /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="page-header" style="background:url(_content/services/1920x485.jpg) no-repeat center center;">
				<div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>services</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline alt"><span> How we
                    <strong>do</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4"><img alt="" class="responsive-img" src=
                "_content/services/300x300.png" /></div><!-- end .span4 -->

                <div class="span8">
                    <h2 style="margin-bottom:20px;">We are building for the
                    future.</h2>

                    <p>We cover a wide array of services both domestic and commercial, mainly within the London and south east England area, however some of our key business activities are listed below:</p>

    
<!--<a class="button-red" href=
                    "services-inner.html">read more</a> -->
                </div><!-- end .span8 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <h3 class="headline alt"><span>who we <strong>do
                    do</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <div class="services-details fixed">
                        <div class="hexagon">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h5><a href="services-inner.html">Electrical</a></h5>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .heaxgon -->

                        <h1>1</h1>

                        <p>Electrical testing of fixed wiring installations to BS7671. | Installation of wiring installations to commercial, domestic and industrial properties |  PAT testing | Installation and testing certification for new consumer unit installations |  Recessed spotlight installations |  Fire and Security Installations |  Data communication and intelligent domestic home systems</p>
                    </div><!-- end .services-details -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <div class="services-details alt fixed">
                        <h1>2</h1>

                        <div class="hexagon alt">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h5><a href="services-inner.html">Decorating</a></h5>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .heaxgon -->

                        <p>Internal decorations to commercial, domestic and industrial properties | External decorations to commercial, domestic and industrial properties</p>
                    </div><!-- end .services-details -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <div class="services-details fixed">
                        <div class="hexagon">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h5><a href="services-inner.html">Building Works</a></h5>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .heaxgon -->

                        <h1>3</h1>

                        <p>Surveying and Engineering professional consultancy services |  New build properties |  Brickwork / Blockwork extensions | Removal of external doors / windows , toothing in stock brickwork to match existing |  Removal of internal loadbearing walls and installation of structural steelworks |  Liaison with Local Authority Building Control |  Whole house / individual room plastering works |  Supply / Design and Installation of new kitchens |  Installation only of kitchens (client supply)</p>
                    </div><!-- end .services-details -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <div class="services-details alt fixed">
                        <h1>4</h1>

                        <div class="hexagon alt">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h5><a href="services-inner.html">Roofing</a></h5>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .heaxgon -->

                        <p>Full scaffolding of works by LASC approved scaffolding contractors.
Removal of existing roofs |  Design / Supply and Installation of new timber roofs |  Installation of new tiled roofs |  Installation of new slate roofs | Installation of Velux windows by Velux approved operatives.
Installation of lead / zinc gulley’s |  Installation of new timber / UPVC soffit and fascia boards |  Installation of new guttering </p>
                    </div><!-- end .services-details -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <div class="services-details fixed">
                        <div class="hexagon">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h5><a href=
                                "services-inner.html">CORGI / plumbing </a></h5>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .heaxgon -->

                        <h1>5</h1>

                        <p>Removal and installation of new boilers by CORGI registered operatives | Installation of new central heating systems by CORGI registered operatives.
Installation of new bathrooms |  Wet room installations |  Power shower installations |  Aids and Adaptations to bathroom installations to facilitate use by disabled users </p>
                    </div><!-- end .services-details -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <div class="row">
                <div class="span12">
                    <div class="services-details alt fixed">
                        <h1>6</h1>

                        <div class="hexagon alt">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <h5><a href="services-inner.html">Refurbishment Works</a></h5>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div><!-- end .heaxgon -->

                        <p>Wall tiling |  Floor tiling | Altro flooring | Amtico flooring | Carpet installations | Floor sanding / varnishing </p>
                    </div><!-- end .services-details -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->


            
			
			
			<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->

        <div id="footer">
            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
                    <h5 class="headline"><span>visit our blog for
                    <strong>latest articles</strong></span></h5>

                    <div class="blog-post">
                        <div class="date">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <p>06 aug</p>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div>

                        <div class="article">
                            <p><a href="#">Lorem ipsum dolor sit amet
                            adipiscing, consectetur adipiscing elit.</a></p>

                            <p>Phasellus eget turpis ut nulla gravida
                            scelerisque id nec libero. Praesent vestibulum enim
                            sollicitudin enim ullamcorper pretium.</p><a class=
                            "button-red" href="#">read more</a>
                        </div><!-- end .article -->
                    </div><!-- end. blog-post -->

                    <div class="blog-post">
                        <div class="date">
                            <div class="hexagon-top"></div>

                            <div class="hexagon-middle">
                                <p>02 aug</p>
                            </div>

                            <div class="hexagon-bottom"></div>
                        </div>

                        <div class="article">
                            <p><a href="#">Phasellus eget turpis ut nulla
                            gravida scelerisque id nec libero.</a></p>

                            <p>Cras ullamcorper massa eu tempus condimentum.
                            Quisque non condimentum leo. Donec
                            pellentesque.</p><a class="button-red" href=
                            "#">read more</a>
                        </div><!-- end .article -->
                    </div><!-- end. blog-post -->
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>