<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'portfolio';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		<script src="_layout/js/gmap/jquery.gmap.min.js" type="text/javascript"></script><!-- /// Waterwheel Carousel //////// -->
		<script src="_layout/js/flexslider/jquery.flexslider.min.js" type="text/javascript"></script><!-- /// Tabs and Accordion //////// -->
		<script src="_layout/js/accordion/jquery.accordion.min.js" type="text/javascript"></script>		
		<script src="_layout/js/tabify/jquery.tabify.min.js" type="text/javascript"></script><!-- /// Validity //////// -->
		<link href="_layout/js/validity/css.validity.css" rel="stylesheet" type="text/css" />
		<script src="_layout/js/validity/jquery.validity.js" type="text/javascript"></script><!-- /// gMap //////// -->
		<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>		
		';
		include 'includes/site/core/settings/header.php';
	}
?>
<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->

        <div id="content">
            <!-- /// CONTENT /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="page-header" style=
            "background:url(_content/portfolio/1920x485.jpg) no-repeat center center;">
            <div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>portfolio</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->
            <div class="row">
                <div class="span12">
                    <h3 class="headline alt"><span>something we have <strong>worked on
                    do</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span13">
                    <div class="portfolio alt-2">
                        <a href="external-remodelling-gallery.php"><img alt="" src="_content/portfolio/thumb/313x322-1.jpg" /></a>
                        <div class="portfolio-hover">
                            <h5>External Remodelling</h5>
                            <p>Somerville</p>
                            <p>description coming soon</p>
                            <div class="portfolio-hover-bottom"></div>
                        </div><!-- end .portfolio-hover -->
                        <div class="portfolio-hover-controls">
							<a href="external-remodelling-gallery.php"><img alt="" src=
                            "_layout/images/icon-link.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->
                    <div class="portfolio alt-2">
                        <a href="internal-refurbishment-gallery.php"><img alt="" src="_content/portfolio/thumb/313x322-2.jpg" /></a>
                        <div class="portfolio-hover">
                            <h5>Internal Refurbishment</h5>
                            <p>Somerville</p>
                             <p>description coming soon</p>
                            <div class="portfolio-hover-bottom"></div>
                        </div><!-- end .portfolio-hover -->
                        <div class="portfolio-hover-controls">
							<a href="internal-refurbishment-gallery.php"><img alt="" src="_layout/images/icon-link.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->
					<div class="portfolio alt-2">
                        <a href="roof-tiling-gallery.php"><img alt="" src="_content/portfolio/thumb/313x322-3.jpg" /></a>
                        <div class="portfolio-hover">
                            <h5>Roof Tiling</h5>
                            <p>Somerville</p>
							<p>description coming soon</p>
                            <div class="portfolio-hover-bottom"></div>
                        </div><!-- end .portfolio-hover -->
                        <div class="portfolio-hover-controls">
						<a href="roof-tiling-gallery.php"><img alt="" src="_layout/images/icon-link.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->
					<div class="portfolio alt-2">
                        <a href="carpentry-gallery.php"><img alt="" src="_content/portfolio/thumb/313x322-4.jpg" /></a>
                        <div class="portfolio-hover">
                            <h5>Carpentry</h5>
                            <p>Somerville</p>
							<p>description coming soon</p>
                            <div class="portfolio-hover-bottom"></div>
                        </div><!-- end .portfolio-hover -->
                        <div class="portfolio-hover-controls">
							<a href="carpentry-gallery.php"><img alt="" src="_layout/images/icon-link.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->
                    <div class="portfolio alt-2">
                        <a href="mechanical-gallery.php"><img alt="" src="_content/portfolio/thumb/313x322-8.jpg" /></a>
                        <div class="portfolio-hover">
                            <h5>Mechanical</h5>
                             <p>All Random Jobs</p>
                            <div class="portfolio-hover-bottom"></div>
                        </div><!-- end .portfolio-hover -->
                        <div class="portfolio-hover-controls">
							<a href="mechanical-gallery.php"><img alt="" src="_layout/images/icon-link.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->					
                    <div class="portfolio alt-2">
                        <a href="roof-lead-gallery.php"><img alt="" src="_content/portfolio/thumb/313x322-7.jpg" /></a>
                        <div class="portfolio-hover">
                            <h5>Roof Lead</h5>
                            <p>Somerville</p>
                            <p>description coming soon</p>
                            <div class="portfolio-hover-bottom"></div>
                        </div><!-- end .portfolio-hover -->
                        <div class="portfolio-hover-controls">
							<a href="roof-lead-gallery.php"><img alt="" src=
                            "_layout/images/icon-link.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span12">
                    <ul class="pagination">
                        <li class="current"><a href="portfolio.php">1</a></li>

                        <li><a href="portfolio-page2.php">2</a></li>



                     <!--  <li><a href="#">&gt;</a></li> -->
                    </ul>
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->

        <div id="footer">
            <!-- /// FOOTER ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/blog_posts.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>