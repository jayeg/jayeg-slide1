<?php 
	{/* meta tags */
		$meta_title = 'index';
		$meta_description = 'meta_description';
		$meta_keywords = 'meta_keywords';
		$page = 'portfolio';
		$add_styles = '<link href="style.css" media="all" rel="stylesheet" type="text/css" />
		<link href="_layout/js/imagebox/imagebox.css" rel="stylesheet" type="text/css" />
		';
		
		$add_scripts = '
		<script src="_layout/js/jquery-1.8.0.min.js" type="text/javascript"></script>
		<script src="_layout/js/selectnav/selectnav.min.js" type="text/javascript"></script><!-- /// Imagebox //////// -->
		<script src="_layout/js/imagebox/jquery.imagebox.min.js" type="text/javascript"></script><!-- /// FlexSlider //////// -->
		<script src="_layout/js/scripts.js" type="text/javascript"></script>
		<script src="_layout/js/plugins.js" type="text/javascript"></script>
		<script src="_layout/js/gmap/jquery.gmap.min.js" type="text/javascript"></script><!-- /// Waterwheel Carousel //////// -->
		<script src="_layout/js/flexslider/jquery.flexslider.min.js" type="text/javascript"></script><!-- /// Tabs and Accordion //////// -->
		<script src="_layout/js/accordion/jquery.accordion.min.js" type="text/javascript"></script>		
		<script src="_layout/js/tabify/jquery.tabify.min.js" type="text/javascript"></script><!-- /// Validity //////// -->
		<link href="_layout/js/validity/css.validity.css" rel="stylesheet" type="text/css" />
		<script src="_layout/js/validity/jquery.validity.js" type="text/javascript"></script><!-- /// gMap //////// -->
		<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>		
		';
		include 'includes/site/core/settings/header.php';
	}
?>
<body>
    <div id="wrap">
        <div class="dropdown-fix" id="header">
            <!-- /// HEADER //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="row">
                <div class="span3">
					<?php include 'includes/site/items/logo.php'?>
                </div><!-- end .span3 -->
                <div class="span9">
                    <!-- // Dropdown Menu // -->
					<?php include 'includes/site/items/navigation.php';?>
                </div><!-- end .span9 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #header -->

        <div id="content">
            <!-- /// CONTENT /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <div class="page-header" style=
            "background:url(_content/portfolio/1920x485.2.jpg) no-repeat center center;">
            <div class="hexagon">
                    <div class="hexagon-top"></div>

                    <div class="hexagon-middle">
                        <h2>portfolio</h2>
                    </div>

                    <div class="hexagon-bottom"></div>
                </div><!-- end .heaxagon -->
            </div><!-- end page-header -->
            <div class="row">
                <div class="span12">
                    <h3 class="headline alt"><span>Roof<strong>Tiling</strong></span></h3>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span13">
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/1.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/1.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/2.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/2.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->					
	                <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/3.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/3.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/4.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/4.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/5.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/5.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/6.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/6.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/7.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/7.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->				
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/8.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/8.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/9.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/9.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/10.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/10.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/11.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/11.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/12.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/12.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/13.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/13.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/14.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/14.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/15.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/15.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/16.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/16.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/17.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/17.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/18.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/18.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/19.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/19.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/20.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/20.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->	
                    <div class="portfolio alt-2">
                        <a href="#"><img alt="" src="_content/portfolio/roof-tiling/thumb/21.jpg" /></a>
                        <div class="portfolio-hover-controls">
							<a href="_content/portfolio/roof-tiling/21.jpg" rel="imagebox[roof-tiling]"><img alt="" src="_layout/images/icon-zoom.png" /></a>
                        </div><!-- end .portfolio-hover-controls -->
                    </div><!-- end .portfolio -->						
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <div class="row">
                <div class="span12">
                    <ul class="pagination">
                        <li class="current"><a href="roof-tiling-gallery.php">Roof Tiling</a></li>
                        <li><a href="portfolio.php">Back</a></li>
                     <!--  <li><a href="#">&gt;</a></li> -->
                    </ul>
                </div><!-- end .span12 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #content -->
        <div id="footer">
            <!-- /// FOOTER ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
            <div class="row">
                <div class="span12">
					<?php include 'includes/site/items/footer_social_media.php';?>
                </div><!-- end .span12 -->
            </div><!-- end .row -->

            <div class="row">
                <div class="span4">
					<?php include 'includes/site/items/footer_about_and_subscribe.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/blog_posts.php';?>
                </div><!-- end .span4 -->

                <div class="span4">
					<?php include 'includes/site/items/footer_address.php';?>
                </div><!-- end .span4 -->
            </div><!-- end .row -->
            <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div><!-- end #footer -->
    </div><!-- end #wrap -->
</body>
</html>