<?php 
$title = 'Construction Companies in Tottenham | North London building company';
$description = 'Experienced in offering ‘all-inclusive’ building and maintenance packages to suit all types of budgets and styles. We undertake maintenance and renovations. We offer tailor made solutions to customers and offer honest and professional advice.';
$keywords = 'builders in tottenham, construction companies , construction companies in tottenham , builders in tottenham ,north london builders ,north london building services ,north london building company';
$add_styles = '<link href="layout/styles/main.css" rel="stylesheet" type="text/css" media="all">
			   <link href="layout/styles/mediaqueries.css" rel="stylesheet" type="text/css" media="all">
			   <link rel="stylesheet" type="text/css" href="css/global.css" media="screen" >
			   ';		 
$add_scripts = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/slides.min.jquery.js"></script>';
$page = 'index';
include 'includes/site_settings/head.php';		
?>
<body class="">
<div class="wrapper row1">
	<?php include 'includes/site_structure/header.php';?>
</div>
<!-- ################################################################################################ -->
<div class="wrapper row2">
	<?php include 'includes/site_structure/navigation.php';?>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container">
    <!-- ################################################################################################ -->
    <div id="homepage" class="clear">
		<?php include 'includes/site_features/main_slider.php';?>
      <!-- ################################################################################################ -->
      <section class="calltoaction opt3 clear">
        <div class="three_quarter first">
          <h1>Call now for free no obligation quote</h1>
          <p>We are always delighted to answer any questions you may have or arrange an on-site appointment in order to estimate or provide a detailed quotation. </p>
        </div>
        <div class="one_quarter"><a href="contact_us.php" class="button large gradient orange rnd8">Contact Us Today</a></div>
      </section>
      <!-- ################################################################################################ -->
      <section class="clear">
        <article class="one_third first">
          <div class="boxholder push30 rnd8"><img src="images/planning_domusaandsons.jpg" alt=""></div>
          <h2>Plan</h2>
          <p>Our builders in London go through several stages to make sure that you have the complete plan for any work that needs to be carried out. </p>
         <!-- <footer><a href="#" class="button medium gradient orange rnd8">Read More &raquo;</a></footer>-->
        </article>
        <article class="one_third">
          <div class="boxholder push30 rnd8"><img src="images/design_domusandsons.jpg" alt=""></div>
          <h2>Design</h2>
          <p>We are driven by a passion for what we do , so we design just for you. </p>
         <!-- <footer><a href="#" class="button medium gradient orange rnd8">Read More &raquo;</a></footer>-->
        </article>
        <article class="one_third">
          <div class="boxholder push30 rnd8"><img src="images/london_builders_domusandsons.jpg" alt=""></div>
          <h2>Build</h2>
          <p>Project Managers & Building Consultants deal with every part of your project and also communicate with your local council to make sure that everything is perfect. </p>
         <!-- <footer><a href="#" class="button medium gradient orange rnd8">Read More &raquo;</a></footer>-->
        </article>
      </section>
      <!-- ################################################################################################ -->
      <div class="divider2"></div>
      <article class="one_half first">
        <h2>Recent Project Completed</h2>
        <div class="boxholder rnd8"><img src="http://www.domusandsons.com/domus&sons/images/kitchen_design_and_build_domusandsons.jpg" alt=""></div>
        <p>We are fully committed to offering an attentive, personal service which is tailored to your specific requirements, and our builders will endeavour to treat your home as if it were their own; working relentlessly to achieve results which they can be truly proud of and you will love.</p>
        <p>If you're looking for qualified, professional builders in North London, look no further than Domus and Sons Construction & Design. Our friendly team will be happy to help you transform your home. Call us on  020 08365 1901 or contact us online today for a free, no-obligation quotation.</p>
        <footer><a href="contact_us.php" class="button medium gradient orange rnd8">Contact Us &raquo;</a></footer>
      </article>
      <div class="one_half">
	   <h2>Testimonials</h2>
        <article class="push30 clear">
       <!--   <div class="one_quarter first">
            <div class="boxholder rnd8"><a href="#"><img src="images/demo/120x120.gif" alt=""></a></div>
          </div> -->
          <div class="three_quarter">
            <h2 class="nospace font-medium">Fulham |  Anne Dawney</h2>
            <p>My oldest daughter needed a separate room so I asked builders from Domus & Sons Construction to build an extension to my house. They have done great job building it, painting, laying electric cables etc. and my Julie is now a very happy teenager!</p>
          <!--  <p><a href="#">Read More &raquo;</a></p> -->
          </div>
        </article>
        <article class="push30 clear">
           <!-- <div class="one_quarter first">
            <div class="boxholder rnd8"><a href="#"><img src="images/demo/120x120.gif" alt=""></a></div>
          </div> -->
          <div class="three_quarter">
            <h2 class="nospace font-medium">Hampstead | Lindsey and John Little</h2>
            <p>If you are still looking for good London builders, you have just found them! Domus & Sons construction is a very professional company that will do any construction/renovation job in your home with minimum distraction to you... and their services are very affordable. We have been their clients for two years now (we gradually renovate the whole house) and are always happy to recommend them to our friends and family. </p>
           <!-- <p><a href="#">Read More &raquo;</a></p> -->
          </div>
        </article>
        <article class="clear">
          <!--  <div class="one_quarter first">
            <div class="boxholder rnd8"><a href="#"><img src="images/demo/120x120.gif" alt=""></a></div>
          </div> -->
          <div class="three_quarter">
            <h2 class="nospace font-medium">Sloane Square | Struan Singh</h2>
            <p>"Domus & Sons have carried out work to three of my flats in London. During this time their team have given me first class service. There is always someone who answers your call and then deals with any issues or problems immediately. I have always been kept informed of issues if any additional work needed to be carried out.
			Domus & Sons also kept me updated with the current legislation required. Overall I am delighted with the service I receive as a Landlord." </p>
           <!-- <p><a href="#">Read More &raquo;</a></p> -->
          </div>
        </article>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </div>
</div>
<!-- Footer -->
<?php include 'includes/site_structure/footer.php'; ?>
</body>
</html>