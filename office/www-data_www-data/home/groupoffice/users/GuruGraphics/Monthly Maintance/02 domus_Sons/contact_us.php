<?php 
$title = 'Contact us | Construction Companies in Tottenham | North London building company';
$description = 'Experienced in offering ‘all-inclusive’ building and maintenance packages to suit all types of budgets and styles. We undertake maintenance and renovations. We offer tailor made solutions to customers and offer honest and professional advice.';
$keywords = 'builders in tottenham, construction companies , construction companies in tottenham , builders in tottenham ,north london builders ,north london building services ,north london building company, Contact us';

$add_styles = '<link href="layout/styles/main.css" rel="stylesheet" type="text/css" media="all">
			   <link href="layout/styles/mediaqueries.css" rel="stylesheet" type="text/css" media="all">
			   <link rel="stylesheet" type="text/css" href="css/global.css" media="screen" >
			   ';		 
$add_scripts = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/slides.min.jquery.js"></script>';
$page = 'contact_us';
include 'includes/site_settings/head.php';		
?>
<body class="">
<div class="wrapper row1">
	<?php include 'includes/site_structure/header.php';?>
</div>
<!-- ################################################################################################ -->
<div class="wrapper row2">
	<?php include 'includes/site_structure/navigation.php';?>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container">
    <!-- ################################################################################################ -->
    <div id="contact" class="clear">
      <div class="one_half first">
        <h1>Contact Domus & Sons</h1>
      
        <div id="respond">
		<?php
			if (empty($_POST) === false) {
					
				$errors = array ();
						
						$author 	= $_POST['author'];
						$email 			= $_POST['email'];
						$message 		= $_POST['message'];
						
				if(empty($author) === true || empty($email) === true || empty($message) === true) {

					$errors [] = 'All fields marked with <span style="color:red;">*</span> are required';	
				} else 	{
							if (ctype_alpha($author) ===  false)   {	
								$errors[] = 'Name must be from letters, dashes, spaces and must not start with dash';
							}
							if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
								$errors[] = 'That\'s  not a valid email address';
							}				
						} 
						
					if (empty($errors) === true) {
										$author 		= $_REQUEST['author'] ;
										$email 			= $_REQUEST['email'] ;
										$to 			= "domusandsons@gmail.com";
	
										$message = $_REQUEST['author' + 'email' + 'message'] ;
										
										$from 			= $_REQUEST['email'] ;
										$subject 		= $_REQUEST['subject'] ;
										$headers = "From:" . $from;
										
										$MESSAGE_BODY .= "Name: 			".$_POST["author"]."\n"; 
										$MESSAGE_BODY .= "email: 			".$_POST["email"]."\n"; 
										$MESSAGE_BODY .= "message: 			".$_POST["message"]."\n"; 									
										
									mail($to,$subject,$MESSAGE_BODY,$headers);
									header('Location: contact_us.php?sent');
									exit();	
								}						
						
			}
		?>
<?php
						if(isset($_GET['sent']) === true) {
						
							echo ' <p style="color:#000 padding:14px;">Thank you for contacting domus and sons.
													<br/>
													<br/>
												
									We have received your message safely and will endeavour to respond to you within the next 24 hours.
	<br/>
											
									Many thanks.
				<br/>
				<br/>
									
									The domus and sons team </p>
							';
						
						} else {
						
							echo ' <p>Please don\'t hesitate to contact us on the number provided, email us directly or request a cal back via the enquiry form. </p>';
						}
					?>		
<?php
			{ if (empty($errors) === false) {
			
				echo '
					<div class="forms_errors">
						<ul>
				
				
				';
				
				foreach($errors as $error) {
				
					echo '<li><span>',$error ,'</span></li>';
				
				}
				
				echo '
						</ul>
						<div class="clear"></div>
					</div>
		
			
			
				';
			}	?>		
          <form class="rnd5" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
            <div class="form-input clear">
              <label class="one_half first" for="author">Name <span class="required">*</span><br>
                <input type="text" name="author" id="author" value="" size="22">
              </label>
              <label class="one_half" for="email">Email <span class="required">*</span><br>
                <input type="text" name="email" id="email" value="" size="22">
              </label>
            </div>
            <div class="form-message">
              <textarea name="message" id="message" cols="25" rows="10"></textarea>
            </div>
            <p>
              <input type="submit" value="Submit">
              &nbsp;
              <input type="reset" value="Reset">
            </p>
          </form>
	<?php } ?>
        </div>
      </div>
      <div class="one_half">
       <!-- <div class="map push50"><img src="images/demo/1200x400.gif" alt=""></div>-->
        <section class="contact_details clear">
          <!--<h2>Puruselit mauris nulla hendimentesque</h2>-->
          <p>We are always delighted to answer any questions you may have or arrange an on-site appointment in order to estimate or provide a detailed quotation.  </p>
          <div class="one_half first">
            <address>
         Domus & Sons LTD
			<br>44 Montague Road
          <br>  Tottenham
			<br>N15 4BD
            </address>
          </div>
          <div class="one_half">
            <ul class="list none">
              <li>Tel: 020 8365 1901</li>
              <li>Email: <a href="#">domusandsons@gmail.com</a></li>
            </ul>
          </div>
        </section>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </div>
</div>
<!-- Footer -->
<?php include 'includes/site_structure/footer.php'; ?>
</body>
</html>