<?php 
$title = 'laminate flooring | Construction Companies in Tottenham | North London building company';
$description = 'Experienced in offering ‘all-inclusive’ building and maintenance packages to suit all types of budgets and styles. We undertake maintenance and renovations. We offer tailor made solutions to customers and offer honest and professional advice.';
$keywords = 'builders in tottenham, construction companies , construction companies in tottenham , builders in tottenham ,north london builders ,north london building services ,north london building company, laminate flooring';

$add_styles = '<link href="layout/styles/main.css" rel="stylesheet" type="text/css" media="all">
			   <link href="layout/styles/mediaqueries.css" rel="stylesheet" type="text/css" media="all">
			   <link rel="stylesheet" type="text/css" href="css/global.css" media="screen" >
			   ';		 
$add_scripts = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/slides.min.jquery.js"></script>';
$page = 'domus_and_sons_Laminate_Flooring';
include 'includes/site_settings/head.php';			
?>
<body class="">
<div class="wrapper row1">
	<?php include 'includes/site_structure/header.php';?>
</div>
<!-- ################################################################################################ -->
<div class="wrapper row2">
	<?php include 'includes/site_structure/navigation.php';?>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container">
    <!-- ################################################################################################ -->
    <div id="sidebar_1" class="sidebar one_quarter first">
      <aside>
        <!-- ########################################################################################## -->
			<?php include 'includes/site_structure/aside_menu.php';?>
        <!-- /nav -->
			<?php include 'includes/site_structure/aside_contact_details.php';?>
        <!-- /section -->
      <!--  <section>
          <article>
            <h2>Lorem ipsum dolor</h2>
            <p>Nuncsed sed conseque a at quismodo tris mauristibus sed habiturpiscinia sed.</p>
            <ul class="list indent disc">
              <li><a href="#">Lorem ipsum dolor sit</a></li>
              <li>Etiam vel sapien et</li>
              <li><a href="#">Etiam vel sapien et</a></li>
            </ul>
            <p>Nuncsed sed conseque a at quismodo tris mauristibus sed habiturpiscinia sed. Condimentumsantincidunt dui mattis magna intesque purus orci augue lor nibh.</p>
            <p class="more"><a href="#">Continue Reading &raquo;</a></p>
          </article>
        </section> --->
        <!-- /section -->
        <!-- ########################################################################################## -->
      </aside>
    </div>
    <!-- ################################################################################################ -->
    <div class="one_half">
      <section class="clear">
        <h1>Laminate Flooring</h1>
        <figure class="imgr boxholder"><img src="images/laminate_flooring_london/laminate_flooring_london_build_by_domus_and_sons_1.jpg" alt=""></figure>
        <p>If you are looking to lay new flooring in your property, then look no further domsbuilds London, will provide amazing quality flooring not found in high street shops. </p>
      </section>
     <!-- <section>
        <h1>Our Tiling Services Cover</h1>
        <table>
          <thead>
            <tr>
              <th>Bathroom</th>
              <th>Floor</th>
              <th>Repairs</th>
              <th>Header 4</th>
            </tr>
          </thead>
          <tbody>
            <tr class="light">
              <td>Value 1</td>
              <td>Value 2</td>
              <td>Value 3</td>
              <td>Value 4</td>
            </tr>
            <tr class="dark">
              <td>Value 5</td>
              <td>Value 6</td>
              <td>Value 7</td>
              <td>Value 8</td>
            </tr>
            <tr class="light">
              <td>Value 9</td>
              <td>Value 10</td>
              <td>Value 11</td>
              <td>Value 12</td>
            </tr>
            <tr class="dark">
              <td>Value 13</td>
              <td>Value 14</td>
              <td>Value 15</td>
              <td>Value 16</td>
            </tr>
          </tbody>
        </table>
      </section>-->
		<?php //include 'includes/site_forms/content_contact_form.php';?>
    </div>
    <!-- ################################################################################################ -->
    <div id="sidebar_2" class="sidebar one_quarter">
      <aside class="clear">
        <!-- ########################################################################################## -->
        <h2></h2>
        <section class="clear">
          <ul class="nospace">
            <li><a href="#"><img src="images/laminate_flooring_london/laminate_flooring_london_build_by_domus_and_sons_2.jpg" alt=""></a></li>
            <li><a href="#"><img src="images/laminate_flooring_london/laminate_flooring_london_build_by_domus_and_sons_3.jpg" alt=""></a></li>
            <li><a href="#"><img src="images/laminate_flooring_london/laminate_flooring_london_build_by_domus_and_sons_4.jpg" alt=""></a></li>
          </ul>
        </section>
        <!-- /section -->
        <!-- ########################################################################################## -->
      </aside>
    </div>
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </div>
</div>
<!-- Footer -->
<?php include 'includes/site_structure/footer.php'; ?>
</body>
</html>