<?php 
$title = 'Floor Design | Construction Companies in Tottenham | North London building company';
$description = 'Experienced in offering ‘all-inclusive’ building and maintenance packages to suit all types of budgets and styles. We undertake maintenance and renovations. We offer tailor made solutions to customers and offer honest and professional advice.';
$keywords = 'builders in tottenham, construction companies , construction companies in tottenham , builders in tottenham ,north london builders ,north london building services ,north london building company, Floor Design';

$add_styles = '<link href="layout/styles/main.css" rel="stylesheet" type="text/css" media="all">
			   <link href="layout/styles/mediaqueries.css" rel="stylesheet" type="text/css" media="all">
			   <link rel="stylesheet" type="text/css" href="css/global.css" media="screen" >
			   ';		 
$add_scripts = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/slides.min.jquery.js"></script>';
$page = 'domus_and_sons_flooring';
include 'includes/site_settings/head.php';			
?>
<body class="">
<div class="wrapper row1">
	<?php include 'includes/site_structure/header.php';?>
</div>
<!-- ################################################################################################ -->
<div class="wrapper row2">
	<?php include 'includes/site_structure/navigation.php';?>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container">
    <!-- ################################################################################################ -->
    <div id="gallery">
      <section>
        <figure>
          <h2>Floor Design & Build</h2>
          <ul class="clear">
            <li class="one_fifth first"><a href="#"><img src="images/flooring/thumbs/1.jpg" alt=""></a></li>
            <li class="one_fifth"><a href="#"><img src="images/flooring/thumbs/2.jpg" alt=""></a></li>

          </ul>
          <figcaption>
            <p></p>
          </figcaption>
        </figure>
      </section>
      <!-- ####################################################################################################### -->
     <!-- <nav class="pagination">
        <ul>
          <li class="prev"><a href="#">&laquo; Previous</a></li>
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li class="splitter"><strong>&hellip;</strong></li>
          <li><a href="#">6</a></li>
          <li class="current"><strong>7</strong></li>
          <li><a href="#">8</a></li>
          <li class="splitter"><strong>&hellip;</strong></li>
          <li><a href="#">14</a></li>
          <li><a href="#">15</a></li>
          <li class="next"><a href="#">Next &raquo;</a></li>
        </ul>
      </nav>-->
    </div>
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </div>
</div>
<!-- Footer -->
<?php include 'includes/site_structure/footer.php'; ?>
</body>
</html>