<?php 
$title = 'About us | Construction Companies in Tottenham | North London building company';
$description = 'Experienced in offering ‘all-inclusive’ building and maintenance packages to suit all types of budgets and styles. We undertake maintenance and renovations. We offer tailor made solutions to customers and offer honest and professional advice.';
$keywords = 'builders in tottenham, construction companies , construction companies in tottenham , builders in tottenham ,north london builders ,north london building services ,north london building company';

$add_styles = '<link href="layout/styles/main.css" rel="stylesheet" type="text/css" media="all">
			   <link href="layout/styles/mediaqueries.css" rel="stylesheet" type="text/css" media="all">
			   <link rel="stylesheet" type="text/css" href="css/global.css" media="screen" >
			   ';		 
$add_scripts = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/slides.min.jquery.js"></script>';
$page = 'about_us';
include 'includes/site_settings/head.php';		
?>
<body class="">
<div class="wrapper row1">
	<?php include 'includes/site_structure/header.php';?>
</div>
<!-- ################################################################################################ -->
<div class="wrapper row2">
	<?php include 'includes/site_structure/navigation.php';?>
</div>
<!-- content -->
<div class="wrapper row3">
  <div id="container">
    <!-- ################################################################################################ -->
    <section class="clear">
      <h1>About Us</h1>
      <div class="two_third first">
        <p>Domus & Sons LTD – small rapidly growing building company based in North London.
			<br/>
			<br/>
Experienced in offering ‘all-inclusive’ building and maintenance packages to suit all types of budgets and styles. We undertake maintenance and renovations. We offer tailor made solutions to customers and offer honest and professional advice.
			<br/>
			<br/>
Domus & Sons are contractors in many building or refurbishment projects in London for more than ten years we have technical as well as practical knowledge. We manage all the tradesmen, materials supply in the most efficient way - so that any project we undertake is completed on time, budget and to a high quality standard. Many good customer references prove that.
			<br/>
			<br/>
Our customers are private as well as commercial customers, projects range in value from £1000 to £100000.
			<br/>
			<br/>
Our scope of work includes the complete range starting from, design and drawing and approval by the city council, contruction and handing over. Our design takes into account the latest development in building construction standards around the world and keeps abreast with them. We strictly adhere to building standards and specifications.
			<br/>
			<br/>
Our vision is to become one of well know refurbishment companies, building contractors in London - we are prepared to go that extra mile to meet anyone's expectations. Give us a call and we will be happy to come and give you a free no obligation quote. </p>
      </div>
      <div class="one_third">
        <div class="calltoaction opt1">
          <div class="push20">
            <h1>Contact us today</h1>
            <p>For free no-obligation quote&hellip;</p>
          </div>
          <div><a href="contact_us.php" class="button large gradient orange">Contact</a></div>
        </div>
      </div>
    </section>
    <!-- ################################################################################################ -->
    <!--<section>
      <h2>Team Members</h2>
      <ul class="nospace clear">
        <li class="one_quarter first">
          <figure class="team-member"><img src="../images/demo/team-member.gif" alt="">
            <figcaption>
              <p class="team-name">Name Goes Here</p>
              <p class="team-title">Job Title Here</p>
              <p class="team-description">Vestassapede et donec ut est liberos sus et eget sed eget quisqueta&hellip;</p>
              <p class="read-more"><a href="#">Read More &raquo;</a></p>
            </figcaption>
          </figure>
        </li>
        <li class="one_quarter">
          <figure class="team-member"><img src="../images/demo/team-member.gif" alt="">
            <figcaption>
              <p class="team-name">Name Goes Here</p>
              <p class="team-title">Job Title Here</p>
              <p class="team-description">Vestassapede et donec ut est liberos sus et eget sed eget quisqueta&hellip;</p>
              <p class="read-more"><a href="#">Read More &raquo;</a></p>
            </figcaption>
          </figure>
        </li>
        <li class="one_quarter">
          <figure class="team-member"><img src="../images/demo/team-member.gif" alt="">
            <figcaption>
              <p class="team-name">Name Goes Here</p>
              <p class="team-title">Job Title Here</p>
              <p class="team-description">Vestassapede et donec ut est liberos sus et eget sed eget quisqueta&hellip;</p>
              <p class="read-more"><a href="#">Read More &raquo;</a></p>
            </figcaption>
          </figure>
        </li>
        <li class="one_quarter">
          <figure class="team-member"><img src="../images/demo/team-member.gif" alt="">
            <figcaption>
              <p class="team-name">Name Goes Here</p>
              <p class="team-title">Job Title Here</p>
              <p class="team-description">Vestassapede et donec ut est liberos sus et eget sed eget quisqueta&hellip;</p>
              <p class="read-more"><a href="#">Read More &raquo;</a></p>
            </figcaption>
          </figure>
        </li>
      </ul>
    </section>-->
    <!-- ################################################################################################ -->
    <!---<section class="last">
      <h2>Client Logos</h2>
      <ul class="nospace clear">
        <li class="one_fifth first"><img src="../images/demo/192x192.gif" alt=""></li>
        <li class="one_fifth"><img src="../images/demo/192x192.gif" alt=""></li>
        <li class="one_fifth"><img src="../images/demo/192x192.gif" alt=""></li>
        <li class="one_fifth"><img src="../images/demo/192x192.gif" alt=""></li>
        <li class="one_fifth"><img src="../images/demo/192x192.gif" alt=""></li>
      </ul>
    </section>--->
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </div>
</div>
<!-- Footer -->
<?php include 'includes/site_structure/footer.php'; ?>
</body>
</html>