<?php 
$page = 'our_products';
$meta_title = 'lamb butcher | in | Bexley | buy leg of lamb';
$meta_description = 'Andrews of Bromley, provides organic lamb meat online, lamb butcher is based in Bromley, and free range product delivery is available. ';
$meta_keywords = 'lamb meat , organic lamb online , lamb butcher , fresh lamb for sale  , buy leg of lamb , lamb suppliers , lamb online';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class=" home_slide effect5 ">
			<img src="images/lamb_product_banner.jpg" style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Andrews Lamb Selection</h2>			
						<p>
							Whether it's lamb shank, a leg of lamb, racks or chops; Andrews offers a wide variety of lamb meat products of different quality cuts and sizes. Why, don't you come and take your pick from our range of tender lamb meats or give us a call and place your order. Natural farms lambs graze from Romney marsh to the Sussex downs. Farms are monitored and lambs selected by our own Mansfield so we only get the best.  
						</p>			
				</article>			
			</div>
			<div class="row box_bg  borders"><table class="prod_list">
		<tr class="prod_list_titles">
			<th>JOINTS </th>
		</tr>
		<tr>
			<td class="titles">LEG</td>
			<td class="text"> 	Ideal family roast lean & tender, can be boned for easy carving </td>
		</tr>
		<tr>
			<td class="titles">SHOULDER </td>
			<td class="text"> 	Tender for slow roasting </td>
		</tr>
		<tr>
			<td class="titles"> RACK of LAMB </td>
			<td class="text"> Tastiest cut can be made into a crown for dinner parties We also coat our racks in a lovely mint glaze & a honey & mustard crust </td>
		</tr>		

		<tr class="prod_list_titles">
			<th> PRIME CUTS </th>
		</tr>		
		<tr>
			<td class="titles"> LOIN CHOPS </td>
			<td class="text"> Tender ideal for any form of cooking </td>
		</tr>	
		<tr>
			<td class="titles"> NECK CUTLETS </td>
			<td class="text"> Sweetest taste, a must for your bbq </td>
		</tr>	
		<tr>
			<td class="titles"> CHUMP CHOP </td>
			<td class="text"> Delicious as a chop but can be boned for a steak </td>
		</tr>	
		<tr>
			<td class="titles"> CANNON OF LAMB </td>
			<td class="text"> Lean & Tender </td>
		</tr>	
		<tr>
			<td class="titles"> LEG STEAKS </td>
			<td class="text"> Lean & tasty grill, fry or bake </td>
		</tr>			
		<tr>
			<td class="titles">NOISETTE/ SADDLE</td>
			<td class="text"> Boneless Loin formed into a circle </td>
		</tr>
		<tr>
			<td class="titles"> MINT CHOPS </td>
			<td class="text">  Tasty chops coated in a sweet mint glaze </td>
		</tr>	
		
		<tr class="prod_list_titles">
			<th> OTHER CUTS </th>
		</tr>		
		
		<tr>
			<td class="titles"> NECK FILLETS </td>
			<td class="text"> Traditional cut for stews , can also be sold on the bone </td>
		</tr>	
		<tr>
			<td class="titles"> DICED LAMB </td>
			<td class="text"> Ready for a curry or casserole </td>
		</tr>
		<tr>
			<td class="titles"> MINCED LAMB </td>
			<td class="text">  Ideal for your home made shepherds pie or Mousakka </td>
		</tr>

		<tr>
			<td class="titles">  LAMB SHANK  </td>
			<td class="text">   Slow roast for soft meat to fall of the bone  </td>
		</tr>
		
		<tr>
			<td class="titles">  KLEFTICO  </td>
			<td class="text">  Slow braised shoulder cuts in a Greek sauce </td>
		</tr>

		<tr class="prod_list_titles">
			<th> AWARD WINNING CUTS </th>
		</tr>	
		<tr>
			<td class="titles">NOISETTES</td>
			<td class="text"> Stuffed with minced lamb in a mint & a tasty seasoning </td>
		</tr>	
		<tr>
			<td class="titles"> LAMB CUTLETS </td>
			<td class="text"> Sweetest of the Lamb stuffed in a rack or individual chops (Awarded by Clarissa Dickson-Wright) </td>
		</tr>	
		<tr>
			<td class="titles"> LAMB BOAT </td>
			<td class="text"> Boneless double best end stuffed with minced lamb rosemary & sage </td>
		</tr>
		<tr>
			<td class="titles"> CARVERY LEG </td>
			<td class="text"> Boneless leg stuffed for easy carving </td>
		</tr>		
	</table>
			</div>	
			<div class="clear"></div>				
		</div>
		<aside>
			<?php include 'includes/site_features/aside/special_offers.php';?>		
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>			
			<?php include 'includes/site_features/aside/aside_operation_hours.php';?>

			
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>