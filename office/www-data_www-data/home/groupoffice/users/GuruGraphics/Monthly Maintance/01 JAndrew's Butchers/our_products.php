<?php 
$page = 'our_products';
$meta_title = 'Butchers that deliver | Bromley | free range meat delivery | Bexley ';
$meta_description = 'Andrews of Bromley, butchers that deliver meat to your door, order a stake online, organic meat and free range home delivery.';
$meta_keywords = 'Butchers that deliver, meat delivered to your door, steak online delivery , natural meats , free range meat delivery , organic free range meat delivery , meat home delivery';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class=" home_slide effect5 ">
			<img src="images/product_banner.jpg" style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Andrews Product Selection</h2>			
						<p>
							Jonathan who has run the family business for the last 20 years which has been growing and growing. This is mainly due to great locally produced meats and a friendly service. We offer an old fashioned service that can never be equalled by the supermarkets, a great range of different cuts of meat, quality and price.
								<br/>
								<br/>
All our meat is of premium quality and where possible local sourced from Kent and Sussex.
				<br/>
								<br/>
All our staff are here to offer you advice, and cut your meat to your exact requirements. 
						</p>			
				</article>			
			</div>
			<div class="row box_bg fl borders home_products">
				<h2>Beef Products</h2>
<a href="andrews_beef_products.php" title="andrews-beef-products"><img src="images/beef_products.jpg" alt="andrews-beef-products" title="andrews-beef-products"/>			
					<div id="">											
<br/>
<p>We pride ourselves in providing only premium quality meat, we only source our meat from quality farms based in Kent and Sussex...</p>	</a>	
<br/>											
					</div>		
			</div>
			<div class="row box_bg fr borders home_products" style="height:438px;" >
				<h2>Pork Products</h2>
				<a href="andrews_pork_products.php" title="andrews-beef-products"><img src="images/pork_products.jpg" alt="andrews-beef-products" title="andrews-beef-products">
					<div id="">											
					<br/><p>We supply and deliver to many of Bromley nursing homes, schools and other catering establishments.</p>		</a>		
					</div>

			</div>
			<div class="clear"></div>		
			<div class="row box_bg fl borders home_products">
				<h2>Lamb Products</h2>
		<a href="andrews_lamb_products.php" title="andrews-beef-products"><img src="images/lamb_products.jpg" title="andrews-beef-products" alt="andrews-beef-products">
					<div id="">
<br/>
<p>We pride ourselves in providing only premium quality meat, we only source our meat from quality farms based in Kent and Sussex...</p>	</a>
<br/>											
					</div>		
			</div>
			<div class="row box_bg fr borders home_products" style="height:437px;">
				<h2>Chicken Products</h2>
				<a href="andrews_chicken_products.php " title="andrews-beef-products"><img src="images/chicken_products.jpg" alt="andrews-beef-products" title="andrews-beef-products"/>
					<div id="">											
					<br/><p>We supply and deliver to many of Bromley nursing homes, schools and other catering establishments.</p>			
				</a>
					</div>

			</div>
			<div class="clear"></div>				
		</div>
		<aside>
					<?php include 'includes/site_features/aside/special_offers.php';?>
			<?php include 'includes/site_features/aside/aside_operation_hours.php';?>
				<div class="aside_box_contain borders box_bg delivery_radius" style="height:561px;">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;" title=""/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>

		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>