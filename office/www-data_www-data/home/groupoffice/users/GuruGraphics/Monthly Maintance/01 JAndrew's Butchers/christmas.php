<?php 
$page = 'christmas';
$meta_title = 'Christmas turkey online | fresh turkeys for Christmas | Bexley | Bexleyheath';
$meta_description = 'Andrews of Bromley provides only fresh Christmas turkeys, in Bromley, Bexley and Bexleyheath areas, please call in for turkey delivery. ';
$meta_keywords = 'christmas turkeys kent - christmas turkey online kent - fresh turkeys for Christmas in kent - fresh turkeys for Christmas in Kent';

include 'includes/site_settings/head.php';
?>
<body class="christmas">
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class="home_slide">
			<img src="images/christmas-page-banner.jpg"  style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Christmas at Andrews of Bromley</h2>		
			<img src="https://scontent-b-cdg.xx.fbcdn.net/hphotos-ash3/p235x350/1381925_231117337045165_1898662746_n.jpg" style="float:right; margin:8px; width:280px;"/>					
						<p>Fully matured turkeys from Appledoor in Kent, have the benefit of a deliciously dense meat, along with a natural fat layer under the skin which helps retain moisture whilst cooking – succulence without the need for basting!
							<br/>
							<br/>
						Each turkey is wrapped in greaseproof paper and presented in a carry home box, along with a sprig of rosemary, vacuum-packed giblets, a recipe leaflet with cooking instructions and a pop-up cooking timer. Everything you need to cook your turkey perfectly on Christmas day.
	<br/>
							<br/>
						Weight Range: 5kg (10-12lb) to 15kg (30-35lb). 
						
							<br/>
							<br/>
							If you need any advice on your Christmas Fayre, please give us a call today. 
						
		
						</p>
				</article>			
			</div>
			<div class="row box_bg  borders">
				<table class="prod_list">
						<tr class="prod_list_titles">
							<th>FOR TURKEY BREAST LOVERS </th>
						</tr>
						<tr>
							<td class="titles">TURKEY </td>
							<td class="text">Our award winning Turkey Butterflies are available plain or stuffed with one of our homemade stuffings</td>
						</tr>
						<tr>
							<td class="titles">BUTTERFLY </td>
							<td class="text">Whole Butterfly weight Range: 1kg to 9kg. </td>
						</tr>
						<tr class="prod_list_titles">
							<th>OTHER POULTRY, WILDFOWL & GAME</th>
						</tr>		
						<tr>
							<td class="titles">FREE RANGE GEESE</td>
							<td class="text">Weight Range: 4.5 to 6kg. </td>
						</tr>	
						<tr>
							<td class="titles">FRESH DUCKS </td>
							<td class="text">Weight Range: 2 to 3kg. </td>
						</tr>	
						<tr>
							<td class="titles">CAPON-STYLE CHICKENS </td>
							<td class="text">Weight Range 2.5 to 4.5kg. Smaller chickens available. </td>
						</tr>	
						<tr>
							<td class="titles">GAME TO ORDER </td>
							<td class="text">Please enquire - most game available including Pheasants, Venison, Tame Rabbits, Ostrich & Haggis </td>
						</tr>	

						
						<tr class="prod_list_titles">
							<th>CHRISTMAS SPECIALS</th>
						</tr>		
						
						<tr>
							<td class="titles">TURKEY, DUCK & CHICKEN </td>
							<td class="text">Three bird breast roast with Cranberry & Orange stuffing (Minimum weight 3kg) </td>
						</tr>	
						<tr>
							<td class="titles">TURKEY, PHEASANT & CHICKEN </td>
							<td class="text">Three bird breast roast with Apricot, Raisin & Brandy stuffing (Minimum weight 3kg) </td>
						</tr>	
						<tr>
							<td class="titles">TURKEY, GOOSE & CHICKEN </td>
							<td class="text">Three bird breast roast with Sage & Onion stuffing
(Minimum weight 3kg) 
</td>
						</tr>	
						<tr>
							<td class="titles">WHOLE DUCK </td>
							<td class="text">Part-boned for easy carving, stuffed with two extra breasts and Cranberry and Orange stuffing. </td>
						</tr>	
						<tr>
							<td class="titles">WHOLE TURKEY </td>
							<td class="text">Boneless, stuffed with Sage and Onion and award-winning sausage meat.
We also give you the drumsticks. 
</td>
						</tr>	
<tr class="prod_list_titles">
							<th>MEAT JOINTS </th>
						</tr>		
						
						<tr>
							<td class="titles">BEEF  & LAMB </td>
							<td class="text">Only cuts with rich marbeling and even fat covering are selected for Andrews of bromley. All roasting cuts are matured on the bone for a minimum of 4 weeks. All cuts fully traceable to farm origin.</td>
						</tr>	
						<tr>
							<td class="titles">CARVERY SCOTTISH 
LEG OF LAMB 
</td>
							<td class="text">With Mint Lamb stuffing. Easy carve and great for New Year too. 
							</td>
						</tr>	
						<tr>
							<td class="titles">FREE RANGE PORK </td>
							<td class="text">Traditional cuts are available: legs of pork whole on the bone or boneless joints from 1kg.
</td>
						</tr>	
<tr class="prod_list_titles">
							<th>CHRISTMAS DINNER ACCOMPANIMENTS  </th>
						</tr>							
						
						
						
						<tr>
							<td class="titles">PIGS IN BLANKETS </td>
							<td class="text">traditional pigs in blankets </td>
						</tr>	
						<tr>
							<td class="titles">DRY CURED BACON  </td>
							<td class="text">International Gold Award Winning.Home cured by ourselves the way bacon used to be - no added water, just a pure bacon taste. 

Supplied in rashers for Christmas dinner, or loin steaks and joints for a traditional Boxing Day lunch. 

</td>
						<tr>
							<td class="titles">HOMEMADE GAMMON HAM  </td>
							<td class="text">ANY SIZE 	International Award Winning. Always available, can be ordered for Christmas. Any size cut from whole gammons. Also available in honey roast. </td>
						</tr>	

						</tr>							
					</table>		
			</div>	
			<div class="clear"></div>				
		</div>
		<aside>
				<div class="aside_box_contain borders box_bg ">
					<h2>Christmas Starts Here!</h2>
						<img src="images/beef3-460x250.jpg" alt="5 mile radius" style="width:100%;"/>
						<article>
							<p>Free Range appledore turkey's from Kent! 
								<br/>
								<br/>
							Any size available from 4KG - 10KG
								<br/>
							Boned & Rolled and stuffed if required.
						</article>
				</div>	
			<?php include 'includes/site_features/aside/aside_operation_hours.php';?>
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>