<?php 
$page = 'about_us';
$meta_title = 'Home meat delivery | fresh meat online | free range meat delivery';
$meta_description = 'Andrew’s butchers, provides home meat delivery service, in around Bromley area. We stock beef, pork, chicken in free range meat, all of our meet is organic, fresh meat guaranteed ';
$meta_keywords = 'home delivery of meat , buy fresh meat online, meat online delivery ,beef online delivery , quality butchers ,online butchers kent, organic meat delivery ,natural meats ,Bromley, Bexley';
include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class=" home_slide">
			<img src="images/about_us.jpg"  style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">About Andrews</h2>			
						<p>
							Jonathan who has run the family business for the last 20 years which has been growing and growing. This is mainly due to great locally produced meats and a friendly service. We offer an old fashioned service that can never be equalled by the supermarkets, a great range of different cuts of meat, quality and price.
								<br/>
								<br/>
All our meat is of premium quality and where possible local sourced from Kent and Sussex.
				<br/>
								<br/>
All our staff are here to offer you advice, and cut your meat to your exact requirements. 
						</p>			
				</article>			
			</div>
			<?php include 'includes/new_structure/farms_and_product_box.php';?>
			<div class="clear"></div>				
		</div>
		<aside>
				<div class="aside_box_contain borders box_bg operation_hours">
					<h2>Operation Hours</h2>
						<article>
						<br/>
						<ul>
						<li>Monday: <span>08:00 - 17:00</span><br /></li>
						<li>Tuesday - Friday: <span>08:00 - 18:00</span><br /></li>
						<li>Saturday: <span>08:00 - 16:30</span><br /></li>
						<li>Sunday: <span>10:00 - 15:00</span></li>
							<br/>
							<br/>
						<li class="call_us">Call Now:<strong> 020 8402 4123</strong></li>
						<li class="email_us">info@jandrewbutchers.co.uk</li>
							<br/>
							<br/>

					</article>		
				</div>
	
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>