<?php 
$page = 'contact_andrews';
$meta_title = 'Andrews Of Bromley Butchers, butchers kent';
$meta_description = 'J.Andrews has been serving the population of Bromley and its surrounding areas for over  60 years. The Family and Staff pride themselves on providing the highest quality fresh meat products.';
$meta_keywords = '';



include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class="home_slide  ">
     <img src="images/contact_us.jpg" alt="image03"  style="width:100%;"/>
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Contact Andrews</h2>			
						<p>
						You can give us a ring, write to us or send us an email; the details are all listed on the right-hand side bottom of this page. 
						We would be pleased to hear from you and answer any queries regarding ordering or even recipes and cooking of our products. We check all our emails on a regular basis and our phones are also answered by real people in the UK.
						</p>			
				</article>			
			</div>
			<div class="row box_bg fl borders contact" style="height:414px;">
									<?php
									$contact_form_fields = array(
									  array('name'    => 'Name:',
											'type'    => 'name',
											'require' => 1),
									  array('name'    => 'E-mail:',
											'type'    => 'email',
											'require' => 1),
									  array('name'    => 'Tel/fax:',
											'type'    => 'input',
											'require' => 0),
									  array('name'    => 'Subject:',
											'type'    => 'subject',
											'require' => 0),
									  array('name'    => 'Message:',
											'type'    => 'textarea',
											'require' => 1),
									  array('name'    => 'reCAPTCHA:',
											'type'    => 'turing',
											'require' => 1,
											'url'     => 'contact-form/image.php',
											'prompt'  => 'Enter the number displayed above'));

									$contact_form_graph           = true;
									$contact_form_xhtml           = false;

									$contact_form_email           = "info@jandrewsbutchers.co.uk";
									$contact_form_encoding        = "utf-8";
									$contact_form_default_subject = "Web Contact Form";
									$contact_form_message_prefix  = "Sent from contact form\r\n==============================\r\n\r\n";

									include_once "contact-form/contact-form.php";
									?>				
			</div>
			<div class="row box_bg fr borders contact_map" style="width:278px;height:414px;">
				<img src="images/tmp/map.PNG" style="width:88%;"/>
				<ul style="width:50%; text-align:center; margin:0 auto;">
										<li>81  Burnt Ash Lane</li>
										<li>Bromley</li>
										<li>Kent</li>
										<li>BR1 5AA</li>
										<strong>020 8402 4123 </strong>
				</ul>
			</div>			
			<div class="clear"></div>				
		</div>
		<aside>
				<div class="aside_box_contain borders box_bg operation_hours">
					<h2>Operation Hours</h2>
						<article>
						<br/>
						<ul>
						<li>Monday: <span>08:00 - 17:00</span><br /></li>
						<li>Tuesday - Friday: <span>08:00 - 18:00</span><br /></li>
						<li>Saturday: <span>08:00 - 16:30</span><br /></li>
						<li>Sunday: <span>10:00 - 15:00</span></li>
							<br/>
							<br/>
						<li class="call_us">Call Now:<strong> 020 8402 4123</strong></li>
						<li class="email_us">info@jandrewbutchers.co.uk</li>
							<br/>
							<br/>

					</article>		
				</div>
	
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>