<?php 
$page = 'our_gallery';
$meta_title = 'Andrews Of Bromley Butchers, butchers kent';
$meta_description = 'J.Andrews has been serving the population of Bromley and its surrounding areas for over  60 years. The Family and Staff pride themselves on providing the highest quality fresh meat products.';
$meta_keywords = '';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class=" home_slide  ">
			<img src="images/andrews_gallery.jpg" style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="borders">
				<h2 style="text-align:left;">Product Gallery</h2>
<img src="images/gallery_images.jpg" style="width:100%;" alt="Gallery Image">				
<img src="images/gallery_images2.jpg" style="width:100%;" alt="Gallery Image">				
			</div>
			<div class="clear"></div>				
		</div>
		<aside>
			<?php include 'includes/site_features/aside/special_offers.php';?>		
			<?php include 'includes/site_features/aside/aside_operation_hours.php';?>
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>

		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>