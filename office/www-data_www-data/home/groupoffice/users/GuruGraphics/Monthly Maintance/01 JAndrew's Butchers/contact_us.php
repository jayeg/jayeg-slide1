<?php 
$page = 'contact_us';
$meta_title = 'Contact butchers in Bromley | Bexley | Bexleyheath';
$meta_description = 'J.Andrews has been serving the population of Bromley and its surrounding areas for over  60 years. The Family and Staff pride themselves on providing the highest quality fresh meat products.';

$meta_keywords = '';

session_start();

include 'includes/site_settings/head.php';
?>
<body>
	<div class="head_bg">
		<?php include 'includes/site_features/header.php';?>
	</div>
	<section>
		<div class="home_slider_holder">
        <div class="slides">
            <ul> <!-- slides -->
                <li><img src="images/slider/home_slide_1.jpg" alt="image01" /> </li>
                <li><img src="images/slider/home_slide_3.jpg" alt="image02" /> </li>
                <li><img src="images/slider/home_slide_4.jpg" alt="image03" /></li>
                <li><img src="images/slider/home_slide_5.jpg" alt="image04" /></li>
            </ul>
        </div>	
		</div>
		<div class="clear"></div>
			<br/>
		<div style="width:68.7em; margin-left:5px;">		
			<div id="section_container_main_content">
					<div class="inner">
						<article id="content_article">
							<div style="width:52.5em;">
								<h1>Contact Us </h1>

								<div id="text_hold" class="box_bg rounded_bordered">
								<p>Contact J.Andrews - to find out more about our locally produced products.
								You can give us a ring, write to us or send us an email; the details are all listed on the right-hand side bottom of this page.
								We would be pleased to hear from you and answer any queries regarding ordering or even recipes and cooking of our products. 
				
								We check all our emails on a regular basis and our phones are also answered by real people in the UK.</p>	
									<br/>
									<div class="fl">
									<?php
									$contact_form_fields = array(
									  array('name'    => 'Name:',
											'type'    => 'name',
											'require' => 1),
									  array('name'    => 'E-mail:',
											'type'    => 'email',
											'require' => 1),
									  array('name'    => 'Tel/fax:',
											'type'    => 'input',
											'require' => 0),
									  array('name'    => 'Subject:',
											'type'    => 'subject',
											'require' => 0),
									  array('name'    => 'Message:',
											'type'    => 'textarea',
											'require' => 1),
									  array('name'    => 'reCAPTCHA:',
											'type'    => 'turing',
											'require' => 1,
											'url'     => 'contact-form/image.php',
											'prompt'  => 'Enter the number displayed above'));

									$contact_form_graph           = true;
									$contact_form_xhtml           = false;

									$contact_form_email           = "info@jandrewsbutchers.co.uk";
									$contact_form_encoding        = "utf-8";
									$contact_form_default_subject = "Web Contact Form";
									$contact_form_message_prefix  = "Sent from contact form\r\n==============================\r\n\r\n";

									include_once "contact-form/contact-form.php";
									?>
									
									
									</div>
									<div class="fr" style="width:23.4em; text-align:center;">
										<img src="images/tmp/map.PNG" style=""/>
										<ul>
										<li>81  Burnt Ash Lane</li>
										<li>Bromley</li>
										<li>Kent</li>
										<li>BR1 5AA</li>
										<strong>020 8402 4123 </strong>
									</div>
									<div class="clear"></div>
								</div>
							</div>

						</article>
						<div class="clear"></div>
					</div>			
			</div>
							<!--- aside ---  -->
			<aside>
				<?php include 'includes/site_features/aside_content.php';?>
			</aside>
		</div>
	</section>
	<div class="clear"></div>
		<?php include 'includes/site_features/footer.php';?>
</body>
 </html>