<?php 
$page = 'index';
$meta_title = 'Andrews of Bromley | free range meat delivery | butchers in Bexley ';
$meta_description = 'Andrews of Bromley, the local butcher based in Kent, free range meat delivery, anywhere in around Bromley, Bexley and Bexleyheath';
$meta_keywords = 'Butchers home delivery, home meat delivery, steak home delivery, free range meat delivery, Bromley, Bexley, Bexleyheath  ';
include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class="home_slide">
        <div class="slides">
            <ul> <!-- slides -->
                <li><img src="images/slider/home_slide_1.jpg" alt="J Andrews of Bromley" title=" J Andrews of Bromley"/></li>
                <li><img src="images/slider/home_slide_3.jpg" alt="J Andrews of bromley shop" title="J Andrews of bromley shop" /></li>
                <li><img src="images/slider/home_slide_4.jpg" alt="J Andrews of bromley customers"  title="J Andrews of bromley customers"/></li>
                <li><img src="images/slider/home_slide_5.jpg" alt="J Andrews of bromley products" title="J Andrews of bromley products"/>
                </li>
            </ul>
        </div>
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Welcome To Andrews of Bromley</h2>			
						<p>
							Andrews has been serving the population of Bromley and its surrounding areas for over 60 years, this is a family run business. Pride ourselves to provide our customers organic quality meat, quality service and value since 1948. </p>		
							<br/>
								<strong style="margin-left:17px;">Why choose us? </strong>
								<ul class="listed-list home-welcome-list" >
									<li>We locally source out meat from farmers that believe in the welfare of their animals. </li>
									<li>We have traditional and experienced butchers you can talk direct to on a phone, everything from special cuts of meat to delivery. </li>
									<li>We cut, prepare and package our own fresh meat products, on a daily basis.</li>
									<li>We have 5-5 food hygiene rating. </li>
									<li>We make deliveries daily in our own refrigerated van.</li>
								</ul>
					<br/>
					<p>	At Jonathan Andrews, we now stock all locally sourced meat pies, and cheese from Kent, why not gives us a call today, or follow this link to a <a href="">Contact form</a> and send us a message. 	</p>
					<ul class="shareThis">
						<li><span class='st_facebook_hcount' displayText='Facebook'></span></li>
						<li><span class='st_linkedin_hcount' displayText='LinkedIn'></span></li>
						<li><span class='st_twitter_hcount' displayText='Tweet'></span></li>
						<li><span class='st_googleplus_hcount' displayText='Google +'></span></li>
						<li><span class='st_email_hcount' displayText='Email'></span></li>
					</ul>
				</article>			
			</div>
			<?php include 'includes/new_structure/farms_and_product_box.php';?>
			<div class="clear"></div>
			<div class="row fl facebook">
				<h2>Facebook Updates</h2>
<div class="fb-like-box" data-href="https://www.facebook.com/pages/JAndrews-Of-Bromley/194978553992377" data-width="500"  data-height="545"data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
			</div>	
			<div class="row fr box_bg borders testimonials">
				<h2>Testimonials </h2>
				<article class="testimonials">
					<ul>
						<li><strong>Caron - Mole</strong> <br/>
						
						<p>Lovely local butchers. I live the other side of Bromley but always come here for a good deal and friendly service. X </p></li>
			
						<li><strong>Nicola - Hayes</strong><br/>
						<p>
							Fabulous fresh food and amazing friendly service! BBQ season is here and this is the butchers to go to! x </p>
						</li>
						
						<li style="padding-bottom:8px;"><strong>Sandy - Brooker</strong><br/>
							<p>Highest quality tip top service highly recommended best butcher ever our business thrives on best quality who supply us we wouldn't buy anywhere else brilliant butchers x</p>
					
						</li>
					</ul>
				</article>
			</div>				
		</div>
		<aside>
			<?php include 'includes/site_features/aside_content.php';?>	
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>