<?php 
$page = 'our_products';
$meta_title = 'Free range chicken in Bexley | meat delivered to your door | Turkey';
$meta_description = 'Organic chicken - organic chicken delivery - organic chickens - meat delivered to your door - turkey delivered to your door - organic meats delivered - free range chicken - free range chicken delivery';
$meta_keywords = 'Andrews of Bromley, is proud itself to provide only free range chicken, delivered to your door.';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class=" home_slide effect5 ">
			<img src="images/chicken_product_banner.jpg" style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Andrews Chicken Selection</h2>			
						<p>
							Chicken is popular for a reason - it's great taste, and most people will love our selection of the tastiest free range chicken from Andrews! You can choose from a range of quality chicken cuts from chicken nuggets to drumsticks, chicken fillet, wings, chicken breast and chicken thighs at Andrews of Bromley.
			<br/>
			<br/>
We also can offer pre-cut chicken with our diced and stir-fry chicken pieces. All you have to do is give us a ring and our drivers will deliver the meat right to your doorstep. Our chicken are barn housed without the use of routine antibiotics and growth promoters. You can be assured that our birds have been treated humanely and with the utmost respect. 
						</p>			
				</article>			
			</div>
			<div class="row box_bg  borders"><table class="prod_list">
		<tr class="prod_list_titles">
			<th> CHICKEN  </th>
		</tr>
		<tr>
			<td class="titles"> FREE RANGE ROASTING CHICKEN </td>
			<td class="text">Approx. 1 to 2 kg</td>
		</tr>
		<tr>
			<td class="titles"> SUPREMES </td>
			<td class="text"> 	Breast part boned with skin </td>
		</tr>
		<tr>
			<td class="titles">LEGS</td>
			<td class="text"> Part boned </td>
		</tr>		
		<tr>
			<td class="titles">DRUMSTICKS</td>
			<td class="text"> 	Can be coated in one of our marinades </td>
		</tr>
		<tr>
			<td class="titles">WINGS</td>
			<td class="text"> Also can be marinated </td>
		</tr>
		<tr>
			<td class="titles"> BREAST FILLETS </td>
			<td class="text"></td>
		</tr>		
	</table>
			</div>	
			<div class="clear"></div>				
		</div>
		<aside>
			<?php include 'includes/site_features/aside/special_offers.php';?>		
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>			
			<?php include 'includes/site_features/aside/aside_operation_hours.php';?>

			
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>