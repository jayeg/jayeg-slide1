<?php 
$page = 'our_products';
$meta_title = 'Organic Pork | Bexleyheath | Pork Delivery | Bexley';
$meta_description = 'Andrews of Bromley provides best online steaks, of organic pork meat, pork delivery service is available.';
$meta_keywords = 'Best online steaks , best online steak , pork meat , organic pork , pork delivery , buy pork online';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class=" home_slide effect5 ">
			<img src="images/pork_product_banner.jpg" style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Andrews Pork Selection</h2>			
						<p>
							Andrews ensure that all their pork meat is sourced from Natural farms. We stock and sell an extensive range of pork varieties for you to choose. We sell pork joints like spatchcock chump, pork loin joint, pork chops, spare rib steak, sausage meat, chipolatas, diced pork, stir fry pork, bacon and gammon. Our pigs are reared outdoors until they are moved into open housing to be graded for fat cover and size. They are fed a wheat and baley based diet. 
						</p>			
				</article>			
			</div>
			<div class="row box_bg  borders">
<table class="prod_list">
		<tr class="prod_list_titles">
			<th> TRADITIONAL CUTS  </th>
		</tr>
		<tr>
			<td class="titles">LEG</td>
			<td class="text"> Ideal roasting joint boneless </td>
		</tr>
		<tr>
			<td class="titles"> SPARE RIB </td>
			<td class="text"> Roasting joint marbled for taste & texture, can also be cut into steaks, ideal for the BBQ </td>
		</tr>
		<tr>
			<td class="titles"> RACK of PORK </td>
			<td class="text"> Sweetest part of the pork sold as a joint or chops </td>
		</tr>		
		<tr>
			<td class="titles"> LOIN of PORK </td>
			<td class="text"> Chops bone in or boneless </td>
		</tr>
		<tr>
			<td class="titles">BELLY</td>
			<td class="text"> Delicious roasted crispy </td>
		</tr>
		<tr>
			<td class="titles">SHOULDER</td>
			<td class="text"> This an extra large joint, can last all week </td>
		</tr>	
		<tr>
			<td class="titles"> RUMP STEAKS </td>
			<td class="text">  Same cut as beef for frying </td>
		</tr>	
		<tr>
			<td class="titles">TENDERLOIN</td>
			<td class="text">  	The fillet steak of the pork </td>
		</tr>	

	
		<tr class="prod_list_titles">
			<th> OUR SPECIALS </th>
		</tr>		
		<tr>
			<td class="titles"> CHINESE RIBS </td>
			<td class="text"> Traditional ribs coated in our famous Chinese glaze </td>
		</tr>	
		<tr>
			<td class="titles"> HAWAIIAN PORK </td>
			<td class="text"> Loin boneless steak perfect for braising in a sweet chilli & pineapple sauce </td>
		</tr>	
		<tr>
			<td class="titles"> LOIN STEAK </td>
			<td class="text"> With a creamy mushroom sauce also perfect for braising </td>
		</tr>	
		<tr>
			<td class="titles"> BONELESS LOIN STEAKS </td>
			<td class="text"> Marinated with a choice of : <br/>
				<br/>Hot & Spicy
				<br/>Thai
				<br/>Sticky Maple
				<br/>Smoky BBQ
			</td>
		</tr>	
		<tr>
			<td class="titles"> STUFFED LOIN </td>
			<td class="text">Boneless loin stuffed with one of our Gold Award stuffings: <br/>
			<br/>Sage & Onion
			<br/>Apricot
			<br/>Raisins & Brandy
			<br/>Cranberry & Orange
			<br/>Garlic & Wild Mushroom
			or for the Scottish lover - Haggis 
</td>
		</tr>					
	</table>		
			</div>	
			<div class="clear"></div>				
		</div>
		<aside>
			<?php include 'includes/site_features/aside/special_offers.php';?>		
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>			
			<?php include 'includes/site_features/aside/aside_operation_hours.php';?>

			
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>