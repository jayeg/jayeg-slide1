<?php 
$page = 'catering_services';
$meta_title = 'Catering Supplies Kent | School Catering in Bromley | Bexley | Bexleyheath';
$meta_description = 'Andrews of Bromley, supplies catering food, to schools, nursing homes, for food catering supplies in Bromley, Bexley  and Bexleyheath, contact us. ';
$meta_keywords = 'catering supplies kent , catering supplies bromley , school catering in bromley , schools catering in bromley , food catering supplies bromley , food catering supplies in bromley , supplies catering in Bromley';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class="home_slide  ">
     <img src="images/catering.jpg" alt="image03"  style="width:100%;"/>
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Catering with Andrews of Bromley</h2>			
						<p>
							We supply and deliver to many local business, schools, nursing home and other catering establishments. All meat can be cut and packed to customers own personal needs and kept under strict temperature control and delivered you within 1 hour of packing, ensuring you receive a fresh and safe consignment of goods.
								<br/>
								<br/>
							Please call for more details  
						</p>			
				</article>			
			</div>
			<?php include 'includes/new_structure/farms_and_product_box.php';?>
			<div class="clear"></div>				
		</div>
		<aside>
				<div class="aside_box_contain borders box_bg operation_hours" style="height:200px;"> 
					<h2>Operation Hours</h2>
						<article>
						<br/>
						<ul>
						<li>Monday: <span>08:00 - 17:00</span><br /></li>
						<li>Tuesday - Friday: <span>08:00 - 18:00</span><br /></li>
						<li>Saturday: <span>08:00 - 16:30</span><br /></li>
						<li>Sunday: <span>10:00 - 15:00</span></li>
							<br/>
							<br/>
						<li class="call_us">Call Now:<strong> 020 8402 4123</strong></li>
						<li class="email_us">info@jandrewbutchers.co.uk</li>
							<br/>
							<br/>

					</article>		
				</div>
	
				<div class="aside_box_contain borders box_bg delivery_radius" >
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>