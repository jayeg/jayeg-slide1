<?php 
$page = 'our_products';
$meta_title = 'Free Range beef | Steak Online | Bexley | Bexleyheath';
$meta_description = 'Andrews of Bromley, online meat ordering buy the best online steaks, of natural beef which comes in free range products.';
$meta_keywords = 'Order beef online , order steak online ,online meat ordering ,online meat order , best online steaks , best online steak , natural beef , free range beef';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class=" home_slide effect5 ">
			<img src="images/beef_product_banner.jpg" style="width:100%;">
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<article>
					<h2 style="text-align:left;">Andrews Beef Selection</h2>			
						<p>
							At Andrews of Bromley we have a variety of beef products. We stock beef steaks like t-bone, sirloin, rump, fillet, rib eye, chuck, and a lot more.
									<br/>
				
			<br/>We also have a variety of joints like topside, silverside parcel, back rib, aitchbone, pot roast and top rump.

			<br/>We even stock ox heart, ox kidney, steak mince, stir fry beef, ox tails and beef goulash, for the more adventurous beef eater.
			<br/>We only provide premium quality meat, this means is we try to promote Kent meats. Natural farms our main supplier.
						</p>			
				</article>			
			</div>
			<div class="row box_bg  borders">
				<table class="prod_list">
						<tr class="prod_list_titles">
							<th>JOINTS </th>
						</tr>
						<tr>
							<td class="titles">RIB ROAST</td>
							<td class="text">Ideal for a tasty Sunday roast, can be on the bone or boned & rolled.</td>
						</tr>
						<tr>
							<td class="titles">TOPSIDE</td>
							<td class="text">Economical and lean. Great option for buffets as it is easy to carve.</td>
						</tr>
						<tr>
							<td class="titles">FILLET</td>
							<td class="text">The most exclusive cut, as it the most leanest and tender. Ideal for those special occasions.</td>
						</tr>		
						<tr>
							<td class="titles">ROLLED SIRLOIN</td>
							<td class="text">Perfect for roasting and full of flavour.</td>
						</tr>
						<tr>
							<td class="titles">ROLLED BRISKET</td>
							<td class="text">A lean cut, which is ideal for pot roasting.</td>
						</tr>
						<tr>
							<td class="titles">SALT BEEF</td>
							<td class="text">An old favourite, which we cure on our premises.</td>
						</tr>	

						<tr class="prod_list_titles">
							<th>STEAKS</th>
						</tr>		
						<tr>
							<td class="titles">SIRLOIN STEAK</td>
							<td class="text">Perfect for grilling or frying.</td>
						</tr>	
						<tr>
							<td class="titles">FILLET STEAK</td>
							<td class="text">The most exclusive cut, as it the most lean and tender.</td>
						</tr>	
						<tr>
							<td class="titles">RIBEYE STEAK</td>
							<td class="text">A tasty cut, due to perfect marbling, which gives it its succulent taste.</td>
						</tr>	
						<tr>
							<td class="titles">T-BONE</td>
							<td class="text">The ultimate of all steaks, a must for steak lovers everywhere.</td>
						</tr>	
						<tr>
							<td class="titles">RUMP STEAK</td>
							<td class="text">A juicy, lean cut. For a cheaper alternative.</td>
						</tr>			
						<tr>
							<td class="titles">BRAISING STEAK</td>
							<td class="text">Ideal for stews or slow cooking.</td>
						</tr>
						
						<tr class="prod_list_titles">
							<th>OTHER BEEF PRODUCTS</th>
						</tr>		
						
						<tr>
							<td class="titles">OXTAIL</td>
							<td class="text">Great for soups and slow braising.</td>
						</tr>	
						<tr>
							<td class="titles">SHIN OF BEEF</td>
							<td class="text">A sometimes forgotten cut, which is perfect for casseroles and goulash.</td>
						</tr>	
						<tr class="prod_list_titles">
							<th>SCOTCH BEEF STEAK MINCE	</th>
						</tr>	
						<tr>
							<td class="titles">SCOTCH BEEF BURGERS</td>
							<td class="text">Hand made using our finest scotch beef steak mince.</td>
						</tr>	
						<tr>
							<td class="titles">BEEF CUSHIONS</td>
							<td class="text">Award Winning! Quick fried steak stuffed with seasoned mince and Stilton cheese with a herb crust.</td>
						</tr>	
						<tr>
							<td class="titles">SKIRT OF BEEF</td>
							<td class="text">Also known as bavette, best for braising.</td>
						</tr>
						<tr>
							<td class="titles">BEEF OLIVES</td>
							<td class="text">Special seasoned beef covered with thin layer of steak.</td>
						</tr>		
					</table>		
			</div>	
			<div class="clear"></div>				
		</div>
		<aside>
			<?php include 'includes/site_features/aside/special_offers.php';?>		
				<div class="aside_box_contain borders box_bg delivery_radius">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>			
			<?php include 'includes/site_features/aside/aside_operation_hours.php';?>

			
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>