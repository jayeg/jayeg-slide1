<?php 
$page = 'testimonials';
$meta_title = 'Andrews Of Bromley Butchers, butchers kent';
$meta_description = 'J.Andrews has been serving the population of Bromley and its surrounding areas for over  60 years. The Family and Staff pride themselves on providing the highest quality fresh meat products.';
$meta_keywords = '';

include 'includes/site_settings/head.php';
?>
<body>
<!--<header>-->
		<?php include 'includes/new_structure/header.php'; ?> 
<!--</header>-->
	<div class="home_slide  ">

         
     <img src="images/slider/home_slide_4.jpg" alt="image03"  style="width:100%;"/>
  

     
		<div class="clear"></div>
	</div><!-- slider -->
	<section>
		<div class="section_in">
			<div class="row box_bg welcome borders">
				<h2 style="text-align:left;">Testimonials </h2>
				<article class="testimonials_page">
					<ul>
						<li><strong>Caron - Mole</strong> <br/>
						
						<p>Lovely local butchers. I live the other side of Bromley but always come here for a good deal and friendly service. X </p></li>					
						<li><strong>Gavin - Prosser</strong>
								<br/>
					<p>my father Colin has been coming here for years and nothing is ever to much for you guys. hats off to a local business doing so well</p>
						</li>
						<li><strong>Nicola - Hayes</strong><br/>
						<p>
							Fabulous fresh food and amazing friendly service! BBQ season is here and this is the butchers to go to! x </p>
						</li>
						
						<li><strong>Sandy - Brooker</strong><br/>
							<p>Highest quality tip top service highly recommended best butcher ever our business thrives on best quality who supply us we wouldn't buy anywhere else brilliant butchers x</p>
						</li>
					</ul>
				</article>		
			</div>
			<?php include 'includes/new_structure/farms_and_product_box.php';?>
			<div class="clear"></div>				
		</div>
		<aside>
				<div class="aside_box_contain borders box_bg operation_hours">
					<h2>Operation Hours</h2>
						<article>
						<br/>
						<ul>
						<li>Monday: <span>08:00 - 17:00</span><br /></li>
						<li>Tuesday - Friday: <span>08:00 - 18:00</span><br /></li>
						<li>Saturday: <span>08:00 - 16:30</span><br /></li>
						<li>Sunday: <span>10:00 - 15:00</span></li>
							<br/>
							<br/>
						<li class="call_us">Call Now:<strong> 020 8402 4123</strong></li>
						<li class="email_us">info@jandrewbutchers.co.uk</li>
							<br/>
							<br/>

					</article>		
				</div>
	
				<div class="aside_box_contain borders box_bg delivery_radius" style="height:459px;">
					<h2>Delivery Radius</h2>
						<img src="images/tmp/5Mile_radius.PNG" alt="5 mile radius" style="width:100%;"/>
						<article>
						<p>Free Delivery 5 Mile From Shop <br/>
							<strong>Min Spend £20.00</strong>
						</p>
						<p>We can go the extra mile for you</p>
						</article>
				</div>
		</aside>
		<div class="clear"></div>
	</section>
	<?php include 'includes/site_features/footer.php';?>
</body>
 </html>