          <div class="navbar navbar-inverse nav-bar">
            <div class="navbar-inner nav-bar-inner">
              <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <div class="nav-collapse collapse top-nav">
                <ul class="nav">
                  <li class="dropdown active"> <a class="dropdown-toggle" href="index.html"> Home </a>

                  </li>
                  <li class="dropdown"> <a class="dropdown-toggle" href="menu.html" > Menu </a>
                    <ul class="dropdown-menu">
                      <li><a href="menu-full.html">Full Menu</a></li>
                      <li><a href="menu-list.html">Menu List</a></li>
                      <li><a href="menu-full-list.html">Full Menu List</a></li>
                      <li><a href="recepie.html">Recepie</a></li>
                      <li><a href="reserve-table.html">Reserve Table</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a class="dropdown-toggle" href="blog.html" > Blog </a>
                    <ul class="dropdown-menu">
                      <li><a href="blog-detail.html">Blog Detail</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a class="dropdown-toggle" href="event.html"> Events </a>
                    <ul class="dropdown-menu">
                      <li><a href="event-deatail.html">Event Detail</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a class="dropdown-toggle" href="#" > Shop </a>
                    <ul class="dropdown-menu">
                      <li><a href="cart.html">Cart</a></li>
                      <li><a href="checkout.html">Checkout</a></li>
                      <li><a href="grid-view.html">Grid View</a></li>
                      <li><a href="list.html">List View</a></li>
                      <li><a href="product-detail.html">Product Detail</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a class="dropdown-toggle" href="#" > Pages </a>
                    <ul class="dropdown-menu">
                      <li><a href="gallery.html">Gallery</a>
                        <ul class="dropdown-menu">
                          <li><a href="tow-col.html">Two Column</a></li>
                          <li><a href="three-col.html">Three Column</a></li>
                          <li><a href="four-col.html">Four Column</a></li>
                        </ul>
                      </li>
                      <li><a href="about.html">About Us</a></li>
                      <li><a href="404.html">404 Page</a></li>
                      <li><a href="faq.html">FAQ</a></li>
                      <li><a href="search-result.html">Search Result</a></li>
                      <li><a href="testimonial.html">Testimonial</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a class="dropdown-toggle" href="contact.html" > Contact Us</a></li>
                </ul>
              </div>
              <!--/.nav-collapse --> 
            </div>
            <!-- /.navbar-inner --> 
          </div>