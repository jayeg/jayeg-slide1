<?php 


  {/* meta tags */

    $meta_title       = '';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Office Moving Services';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic" rel="stylesheet" type="text/css">
    <link href="css/icomoon.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/gozha-nav.css" rel="stylesheet" />
    <link href="css/base.css?v=1" rel="stylesheet" />
    <link href="css/style.css?v=1" rel="stylesheet" />
    ';
    
    $add_scripts = '
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.menu.js"></script>
    <script type="text/javascript" src="js/jquery.inview.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-open.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-pro.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="js/custom.js"></script>
    ';

  } 
?>
<!doctype html>
<html><head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title><?php echo $meta_title; ?></title>
        <meta name="description" content="<?php echo $meta_description; ?>">
        <meta name="keywords" content="<?php echo $meta_keywords; ?>">
        <meta name="author" content="<?php echo $meta_author; ?>">

 
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
        <script src="js/modernizr.custom.05768.js"></script>
        <?php echo $add_styles; ?>
    
    <!-- Fonts -->
        <!-- SourceSansPro -->
        
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
 
</head>

<body class=''>
    <div class='wrapper'>
        <?php include('includes/header.php'); ?>
        
        <section class="container waypoint" data-animate-up="nav-strict" data-animate-down="nav-show">
            <div class="service-information">
                 <div class='col-sm-1'></div>
                <div class="col-sm-10">
          
                       <p>At The Right Removal Company not only do we specialise in house moving services we can also provide office moving services. We can offer bespoke removal services which allow us to operate day or night. We can offer a removal crate hire service to the complete dismantling and reassembling of all office furniture if required for your furniture removal.</p>
                       <p>If you require storage facilities during your office moving services we can help with your storage solutions. We offer secure storage and all our units are monitored by cctv and are checked on a weekly basis.</p>
                       <p>We can also provide you with a professional space planning service allowing your office to work efficiently and avoid any cramped unusable spaces. All our space planning is set out and presented using British Standards & Regulations.</p>
                    
                      </div>
                       <div class='col-sm-1'></div>
                </div>

            </div>

            <div class="devider"></div>

            <!--<div class="service-detail">
                <div class="detail-article first-article">
                     <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="preview-image">
                            <img alt='' src="images/service/site-view-small.jpg">
                            <div class="large-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part1.png">
                                </div>
                            </div>
                            <div class="small-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part2.png">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <h3>AppCorner</h3>
                        <p class="decript">app which suits your needs</p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque libero a ante viverra vel scelerisque orci tristique. Nullam nisl massa, ullamcorper id condimentum porta, cursus a lorem. Mauris gravida ipsum vitae libero molestie eget dignissim ipsum egestas. Quisque in eros id velit molestie placerat a ut arcu. Etiam consequat, turpis ac commodo rhoncus, justo enim consequat tortor, vitae interdum odio tellus at sem.
                            <br / >
                            <br / >
                            Duis a elit purus. Sed condimentum magna quis porttitor tincidunt. Donec vehicula dui nulla, sit amet posuere nisl facilisis nec. Duis ultricies varius mauris, ut aliquam tortor ultricies sed. Praesent ultricies rhoncus nunc a vulputate. Pellentesque consectetur augue venenatis, rhoncus lorem eget, lobortis ligula. Cras imperdiet est elit, nec placerat dolor tincidunt eu. 
                        </p>
                    </div>

                   
                </div>

                <div class="detail-article second-article">
                    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <h3>Maecenas mollis </h3>
                        <p class="decript">ornare quis venenatis id, vulputate non turpis</p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque libero a ante viverra vel scelerisque orci tristique. Nullam nisl massa, ullamcorper id condimentum porta, cursus a lorem. Mauris gravida ipsum vitae libero molestie eget dignissim ipsum egestas. Quisque in eros id velit molestie placerat a ut arcu. Etiam consequat, turpis ac commodo rhoncus, justo enim consequat tortor, vitae interdum odio tellus at sem.
                            <br / >
                            <br / >
                            Duis a elit purus. Sed condimentum magna quis porttitor tincidunt. Donec vehicula dui nulla, sit amet posuere nisl facilisis nec. Duis ultricies varius mauris, ut aliquam tortor ultricies sed. Praesent ultricies rhoncus nunc a vulputate. Pellentesque consectetur augue venenatis, rhoncus lorem eget, lobortis ligula. Cras imperdiet est elit, nec placerat dolor tincidunt eu. 
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="preview-image">
                            <img alt='' src="images/service/site-view-small.jpg">
                            <div class="large-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part3.png">
                                </div>
                            </div>
                            <div class="small-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part4.png">
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>-->
            </div>
        </section>

         <!--<div  class='promobox-one'>
          <div class='container'>
            <p class='promo show'>Check appcorner here </p>
            <hr />
            <button type="button" class="btn btn-success btn-lg promo-btn icon-circled-right btn-animate scale">live demo</button>
          </div>
        </div>-->

        <!--<section class="container">
            <div class="timetable-wrap">
                <div class="timetable">
                    <div id="wijlinechart" style="width: 100%; height: 365px"></div>
                    <hr class="seperate-line" />
                    <div class="background-timetable"></div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque libero a ante viverra vel scelerisque orci tristique. Nullam nisl massa, ullamcorper id condimentum porta, cursus a lorem. Mauris gravida ipsum vitae libero molestie eget dignissim ipsum egestas. Quisque in eros id velit molestie placerat a ut arcu.
                <br />
                <br /> 
                Etiam consequat, turpis ac commodo rhoncus, justo enim consequat tortor, vitae interdum odio tellus at sem. Aliquam lacus quam, lobortis sed lacus eget, commodo volutpat nunc. Proin accumsan diam vel nulla ultrices congue. Praesent ut lacus a elit adipiscing venenatis sed id quam.</p>
                <a href="#" class="read-more">read more</a>
            </div>
        </section>-->

        <?php include('includes/footer.php'); ?>
    </div>
    <script>window.jQuery || document.write('<script src="js/jquery-1.10.1.min.js"><\/script>')</script>
    <?php echo $add_scripts; ?>



        <script type="text/javascript">
            $(document).ready(function() {
                initSingleService();
            });
        </script>

</body>