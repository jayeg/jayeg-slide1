<?php 


  {/* meta tags */

    $meta_title       = '';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Our Terms and Conditions';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic" rel="stylesheet" type="text/css">
    <link href="css/icomoon.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/gozha-nav.css" rel="stylesheet" />
    <link href="css/base.css?v=1" rel="stylesheet" />
    <link href="css/style.css?v=1" rel="stylesheet" />
    ';
    
    $add_scripts = '
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.menu.js"></script>
    <script type="text/javascript" src="js/jquery.inview.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-open.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-pro.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="js/custom.js"></script>
    ';

  } 
?>
<!doctype html>
<html><head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title><?php echo $meta_title; ?></title>
        <meta name="description" content="<?php echo $meta_description; ?>">
        <meta name="keywords" content="<?php echo $meta_keywords; ?>">
        <meta name="author" content="<?php echo $meta_author; ?>">

 
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
        <script src="js/modernizr.custom.05768.js"></script>
        <?php echo $add_styles; ?>
    
    <!-- Fonts -->
        <!-- SourceSansPro -->
        
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
 
</head>

<body class=''>
    <div class='wrapper'>
        <?php include('includes/header.php'); ?>
        
        <section class="container waypoint" data-animate-up="nav-strict" data-animate-down="nav-show">
            <div class="service-information">
                 <div class='col-sm-1'></div>
                <div class="col-sm-10">
                       <p>These Terms and Conditions may be changed with the agreement of both parties (The Customer and TheRightRemovalCompany). Please notify us if you wish to make changes at the time of booking By confirming a booking with TheRightRemovalCompany you are agreeing to the following terms and conditions. Where we use the words 'You' or 'Your' it means the customer. 'We, Us or Our' means TheRightRemovalCompany </p>
<br /><br />
<p>1. The Quote: unless otherwise agreed your removal Service will be based on an hourly fee and your quote will include for packing materials, Removal Boxes. Storage Costs & Packing Services. Your quote does not include for Parking Tickets, Parking bay Suspensions, Customs and Excise, Congestion Charges or any other additional charges whatsoever. 
<br />

<p>a) The removal is not carried out or completed within three months due to your delay. </p>
<p>ab) Our costs change because of changes in taxation or other costs beyond our control. </p>
<p>ac) We have to collect or deliver goods to floors higher than those agreed, in writing, at the time of booking. </p>
<p>ad) Additional services, including moving or storing extra goods, are supplied by us (these conditions will also apply to these services). </p>
<p>ae) Inadequate access preventing free movement of the goods without mechanical equipment or structural alteration, or the approach, road or drive prevents our vehicles to load and/or unload within 20 metres of the doorway, meaning that we have to do extra work. </p>
<p>af) Breakdown of lifts or escalators which were working at the time of quote, meaning extra work. </p>
<p>ag) Any parking charges or parking fines that we have to pay in order to carry out services on your behalf (see condition 17). </p>
<p>ah) Events outside our reasonable control cause delays. </p>
<p>aj) We agree in writing to increase our limit of liability as set out in clause 10. In all the above circumstances the price will be adjusted accordingly. </p>
<br /><br />
<p>2. Work excluded from the quote unless agreed by us in writing we will not: </p>
<p>a) Disconnect or reconnect appliances, fixtures, fittings or electrical equipment.</p> 
<p>b) Take up or lay fitted floor coverings. c) Move storage heaters (unless previously dismantled). </p>
<p>d) Move items from or to a loft (unless well illuminated with safe access). </p>
<p>e) Move or store any items excluded under clause 5. </p>
<br /><br />
<p>3. Our responsibility. It will be our responsibility to deliver or produce your goods to you undamaged. This means that we will deliver or produce the goods to you in the condition they were in at the time of packing or made ready for transportation. </p>
<br /><br />
<p>4. Your responsibility. It will be your own responsibility (and, where relevant, expense) to: </p>
<br />
<p>a) Declare to us valuations of all goods being removed. The value to be used is the current value (not a new for old value). </p>
<p>b) Insure the goods submitted for removal against all insurable risks where you do not wish to accept 'Inclusive Cover'. </p>
<p>c) Obtain all paperwork (licences, permits, etc.) necessary for the removal to be completed. </p>
<p>d) Be yourself present or represented throughout the whole removal. e) Take all reasonable steps to ensure that nothing is left behind or taken away in error. </p>
<p>f) Arrange protection for goods left in unattended premises or where other people not bound by these terms and conditions will be present. </p>
<p>g) Prepare and stabilise all electric equipment prior to its removal. </p>
<p>b) Items which have potential to damage, explode or be dangerous including gas bottles, aerosols, paints, firearms and ammunition. </p>
<p>c) Prohibited or stolen goods. </p>
<p>d) Drugs. </p>
<p>e) Plants or goods which may be likely to encourage vermin, pests or cause infestation. </p>
<p>f) Food or drink which is refrigerated or frozen. g) Any pets or animals (including reptiles, fish and birds) whether in cage, tank or animal carrier. </p>
<p>h) Goods which require government permission or licence for export or import.   </p>
<br />
<p>Any of the above-listed goods will not be removed by us except with our prior written agreement. If these goods are removed we will not accept liability for loss or damage, unless we are negligent or in breach of contract, due to the special nature of the goods concerned. If such goods are removed without our knowledge and prior written consent we will not be liable for any loss or damage and you will indemnify us against any charges, expenses, damages or penalties claimed against us. Furthermore, we would have the right to dispose of goods which are listed under paragraphs 5(b), 5(c), 5(d), 5(e) and 5(f) without notice.</p>
<br /><br />
<p>5. Non-submission of certain goods for removal. The following items are excluded from this contract: a) Jewellery, watches, trinkets, precious stones or metals, money, deeds, securities, stamps, coins or goods or collections of any similar kind. b) Items which have potential to damage, explode or be dangerous including gas bottles, aerosols, paints, firearms and ammunition. c) Prohibited or stolen goods. d) Drugs. e) Plants or goods which may be likely to encourage vermin, pests or cause infestation. f) Food or drink which is refrigerated or frozen. g) Any pets or animals (including reptiles, fish and birds) whether in cage, tank or animal carrier. h) Goods which require government permission or licence for export or import. Any of the above-listed goods will not be removed by us except with our prior written agreement. If these goods are removed we will not accept liability for loss or damage, unless we are negligent or in breach of contract, due to the special nature of the goods concerned. If such goods are removed without our knowledge and prior written consent we will not be liable for any loss or damage and you will indemnify us against any charges, expenses, damages or penalties claimed against us. Furthermore, we would have the right to dispose of goods which are listed under paragraphs 5(b), 5(c), 5(d), 5(e) and 5(f) without notice.6. </p>
<br /><br />
<p>6.Ownership. You declare, upon acceptance of this contract, that: </p>
<br />
<p>a) all goods to be removed are your own property or, </p>
<p>b) you have been given the authority to make this contract by the person(s) who own or have an interest in the goods and that they have been made aware of these conditions. You will meet any claims for damages and / or costs against us if these statements are not true. </p>
<br /><br />
<p>7. Initial Deposit. A 25% deposit is required for jobs whose anticipated value will exceed £250 (exc. VAT). The deposit should be cleared funds in our bank account 14 days before the removal date. </p>
<br /><br />
<p>8. Paying for the removal. Unless otherwise agreed by us in writing: </p>
<p>a) Payment may be made cash, bank transfer, or debit card. In all cases we require cleared funds in our bank account, prior to removal of the goods. </p>
<p>b) You may not withhold any part of the agreed price. 
<p>c) Where we have varied these conditions as to payment we may charge daily interest, calculated at 4% per annum above the base rate for Halifax Bank plc, with respect to all overdue amounts owed to us. </p>
<p>d) The final payment for your Removal Service i.e. 75% must be paid within 24hrs of your Removal completion. </p>
<br /><br />
<p>9. Charges if you postpone or cancel the removal. Charges are made if this contract is postponed or cancelled. Charges are as follows: </p>
<br />
<p>a) Notice given more than 14 days before the removal was due to start: NIL </p>
<p>b) Notice given less than 14 days before the removal was due to start: 25% of the removal charge. </p>
<p>c) Notice given less than 7 days before the removal was due to start: 50% of the removal charge. 10. Our liability for loss or damage. Where you produce to us an itemised valued inventory (as set out in clause 4(a)) our liability to you with respect to our breaching clause 3 will be determined by clause 11 below. Where we are not provided with a valued inventory or you request us not to accept 'Inclusive Cover' liability then our liability is as set out below: </p>
<br />
<p>a) If we are liable, we will pay up to a maximum sum of £50 sterling for each item which is lost or damaged due to our negligence or breach of contract.</p> 
<p>b) We may repair or replace an item that is damaged. However, if an item is repaired we will not be held liable for depreciation in value. </p>
<p>c) We will not be liable for any loss, damage or failure to produce the goods if caused by fire or explosion unless caused by our negligence or breach of contract. </p>
<br /><br />
<p>11. Inclusive Cover. In the event that you accept the 'Inclusive Cover' by providing to us an itemised valued inventory (clause 4(a)) then our liability to you through our breach of clause 3 shall be as follows subject to a maximum liability of £20,000. </p>
<p>a) In the event of loss or damage to your goods our liability will be determined by the cost of repair or replacement taking into account the age and condition at the time such loss or damage was incurred. </p>
<p>b) It shall be your responsibility to determine whether the 'Inclusive Cover' provided is sufficient to cover your goods and risks to them.</p>
<br /><br />
<p>12. Exclusions to our liability. We will not be liable for any loss, damage or failure to produce the goods if caused by </p>
<p>i) Fire or explosion unless caused by our negligence or breach of contract. </p>
<p>ii) War, hostilities, terrorism, Act of God, industrial action or other such events outside our reasonable control. </p>
<p>iii) Normal wear and tear or deterioration, leakage or evaporation or from unstable or perishable goods. </p>
<p>iv) Infestation by moth, vermin or anything similar. </p>
<p>vi) Goods not both packed and unpacked by us, including those in wardrobes, drawers, or appliances, or in a package, bundle, case or other container. </p>
<p>vii) Electrical / mechanical faults to any appliance or mechanical instrument, unless there is evidence of external impact. </p>
<p>viii) To jewellery, watches, trinkets, precious stones or metals, money, deeds, securities, stamps, coins or goods or collections of any similar kind, unless we have confirmed in writing that we accept responsibility and you have given us description and value of those articles. </p>
<p>ix) Any goods already proven defective or goods which are inherently defective. </p>
<p>x) To pets or animals (including reptiles, fish and birds) whether in cage, tank or animal carrier. </p>
<p>xi) Plants. </p>
<p>xii) To refrigerated or frozen food or drink. We will not be liable for any damages or costs occurred as a result of loss, damage or failure to produce the goods, other than by reason of our negligence. No employee of Casey's Removals shall be separately liable to you for any loss, damage, mis-delivery, errors or omissions.</p>
<br /><br />
<p>13. Time limit for making a claim </p>
<br />
<p>a) Any loss, damage or failure to produce goods which we are delivering must be noted at the time of delivery. </p>
<p>b) Notwithstanding clause 10, we will not be liable for any loss of or damage to the goods unless you notify us in writing as soon as such loss or damage is discovered (or with reasonable diligence ought to have been discovered) and in any event within 7 days of collection or delivery of the goods by us. </p>
<p>c) The time limits referred to in clauses 13 (a) and 13 (b) above shall be essential to the contract. </p>
<p>d) You may make a written request to extend your time for compliance with clause 13 (b). Such a request shall not be unreasonably refused provided it is received within 7 days of collection or delivery of the goods by us.</p>
<br /><br />
<p>14. Delays in transit. </p>
<br />
<p>a) We are not liable for delays in transit other than by reason of our own negligence. </p>
<p>b) If we are unable to deliver your goods through no fault of our own, we will take them into storage. At this point the contract will be fulfilled. Any additional service(s), including storage and delivery, will be at your further expense.</p>
<br /><br />
<p>15. Damage to property other than the goods. </p>
<p>a) For property other than those goods submitted for removal and / or storage we will only be liable for damage where it can be proven that we have been negligent. </p>
<p>b) If it is the case that we advise against moving certain goods as it will inevitably cause damage but you give us your express instruction to move said goods and damage is caused, we shall not accept that we were negligent.</p> 
<p>c) For property other than those goods submitted for removal and / or storage, where we are responsible for causing damage, you must note this on the worksheet or delivery receipt. This is essential to the contract.</p>
<br /><br />
<p>16. Holding the goods. Until all charges, including charges we have paid out on your behalf, or payments due under this or any other contract have been met, we shall have the right to withhold and/or ultimately dispose of some or all of the goods. You will further be liable to pay all storage charges and other costs incurred by our withholding your goods until we receive payment. These terms and conditions shall continue to apply. Where we provide storage facilities as part of the contract and you do not use us for the removal of your goods we will make a charge of £75 per hour (minimum 2 hours) to provide access to you, or any agent of yours, for the removal of all goods. This charge will apply for each visit by you or your agent. </p>
<br /><br />
<p>17. Parking. The following conditions shall apply to parking provisions at both the collection and drop off locations: a) Most local boroughs allow 20 minutes loading and unloading time on single and double yellow lines and in residential, pay and display and loading bays. If your removal time is expected to be twenty minutes or less and these parking conditions apply directly outside the removal location then you need not make any provisions for parking, however this is done at your own risk (see condition 17 (c)). </p>
<br />
<p>b) If your removal time is expected to be more than twenty minutes, provisions for the parking of all removal vehicles needed to undertake your removal in a position directly accessible to the removal location and for the duration of the removal must be made prior to the removal date at your own expense. In the event that provisions have not been made extra charges may be incurred (see condition 1(h)). </p>
<p>c) If a Penalty Charge Notice (parking ticket) is given under the circumstances in conditions 17 (a) or (b) we will endeavour to the best of our ability to appeal against the issuing of the ticket. However, if an appeal is not upheld, payment of the Penalty Charge Notice will be your responsibility. </p>
<br /><br />
<p>18. Disputes. If there is a dispute relating to this agreement which cannot be resolved by either party then either party may refer it to an independent arbitrator. </p>
<br /><br />
<p>19. Sub-contracting the work.</p>
<br /> 
<p>a) We reserve the right to sub-contract some or all of the work. </p>
<p>b) In the event that we sub-contract, these terms and conditions will still apply.</p> 
<br /><br />
<p>20. Route and method. </p>
<br />
<p>a) We have the right to choose the route for delivery. </p>
<p>b) Unless it has been specifically agreed in writing on our quotation, customers may share space with other consignments on our vehicles. </p>
<br /><br />
<p>21. Advice and information. Advice and information, in whatever form it may be given, is provided by the company for the customer only. Any oral advice given without special arrangement is provided gratuitously and without contractual liability. </p>
<br /><br />
<p>22. Applicable law. This contract is subject to the Law and Jurisdiction of England. </p>
<br /><br />
<p>23. Disputes. If there is a dispute relating to this agreement which cannot be resolved by either party then either party may refer it to an independent arbitrator. </p>
<br /><br />
<p>24. Sub-contracting the work.</p> 
<br />
<p>a) We reserve the right to sub-contract some or all of the work. </p>
<p>b) In the event that we sub-contract, these terms and conditions will still apply. </p>
<br /><br />
<p>25. Route and method. 
<br />
<p>a) We have the right to choose the route for delivery. </p>
<p>b) Unless it has been specifically agreed in writing on our quotation, customers may share space with other consignments on our vehicles. </p>
<br /><br />
<p>26. Advice and information. Advice and information, in whatever form it may be given, is provided by the company for the customer only. Any oral advice given without special arrangement is provided gratuitously and without contractual liability.</p>
<br /><br />
<p>27. Applicable law. This contract is subject to the Law and Jurisdiction of England.</p>
                      </div>
                </div>
             <div class='col-sm-1'></div>

            <div class="devider"></div>

            <!--<div class="service-detail">
                <div class="detail-article first-article">
                     <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="preview-image">
                            <img alt='' src="images/service/site-view-small.jpg">
                            <div class="large-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part1.png">
                                </div>
                            </div>
                            <div class="small-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part2.png">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <h3>AppCorner</h3>
                        <p class="decript">app which suits your needs</p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque libero a ante viverra vel scelerisque orci tristique. Nullam nisl massa, ullamcorper id condimentum porta, cursus a lorem. Mauris gravida ipsum vitae libero molestie eget dignissim ipsum egestas. Quisque in eros id velit molestie placerat a ut arcu. Etiam consequat, turpis ac commodo rhoncus, justo enim consequat tortor, vitae interdum odio tellus at sem.
                            <br / >
                            <br / >
                            Duis a elit purus. Sed condimentum magna quis porttitor tincidunt. Donec vehicula dui nulla, sit amet posuere nisl facilisis nec. Duis ultricies varius mauris, ut aliquam tortor ultricies sed. Praesent ultricies rhoncus nunc a vulputate. Pellentesque consectetur augue venenatis, rhoncus lorem eget, lobortis ligula. Cras imperdiet est elit, nec placerat dolor tincidunt eu. 
                        </p>
                    </div>

                   
                </div>

                <div class="detail-article second-article">
                    
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <h3>Maecenas mollis </h3>
                        <p class="decript">ornare quis venenatis id, vulputate non turpis</p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque libero a ante viverra vel scelerisque orci tristique. Nullam nisl massa, ullamcorper id condimentum porta, cursus a lorem. Mauris gravida ipsum vitae libero molestie eget dignissim ipsum egestas. Quisque in eros id velit molestie placerat a ut arcu. Etiam consequat, turpis ac commodo rhoncus, justo enim consequat tortor, vitae interdum odio tellus at sem.
                            <br / >
                            <br / >
                            Duis a elit purus. Sed condimentum magna quis porttitor tincidunt. Donec vehicula dui nulla, sit amet posuere nisl facilisis nec. Duis ultricies varius mauris, ut aliquam tortor ultricies sed. Praesent ultricies rhoncus nunc a vulputate. Pellentesque consectetur augue venenatis, rhoncus lorem eget, lobortis ligula. Cras imperdiet est elit, nec placerat dolor tincidunt eu. 
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="preview-image">
                            <img alt='' src="images/service/site-view-small.jpg">
                            <div class="large-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part3.png">
                                </div>
                            </div>
                            <div class="small-zoom">
                                <div class="zoom-holder">
                                    <img alt='' src="images/service/part4.png">
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>-->
            </div>
        </section>

         <!--<div  class='promobox-one'>
          <div class='container'>
            <p class='promo show'>Check appcorner here </p>
            <hr />
            <button type="button" class="btn btn-success btn-lg promo-btn icon-circled-right btn-animate scale">live demo</button>
          </div>
        </div>-->

        <!--<section class="container">
            <div class="timetable-wrap">
                <div class="timetable">
                    <div id="wijlinechart" style="width: 100%; height: 365px"></div>
                    <hr class="seperate-line" />
                    <div class="background-timetable"></div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras scelerisque libero a ante viverra vel scelerisque orci tristique. Nullam nisl massa, ullamcorper id condimentum porta, cursus a lorem. Mauris gravida ipsum vitae libero molestie eget dignissim ipsum egestas. Quisque in eros id velit molestie placerat a ut arcu.
                <br />
                <br /> 
                Etiam consequat, turpis ac commodo rhoncus, justo enim consequat tortor, vitae interdum odio tellus at sem. Aliquam lacus quam, lobortis sed lacus eget, commodo volutpat nunc. Proin accumsan diam vel nulla ultrices congue. Praesent ut lacus a elit adipiscing venenatis sed id quam.</p>
                <a href="#" class="read-more">read more</a>
            </div>
        </section>-->

        <?php include('includes/footer.php'); ?>
    </div>
    <script>window.jQuery || document.write('<script src="js/jquery-1.10.1.min.js"><\/script>')</script>
    <?php echo $add_scripts; ?>



        <script type="text/javascript">
            $(document).ready(function() {
                initSingleService();
            });
        </script>

</body>