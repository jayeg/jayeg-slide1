<?php 


  {/* meta tags */

    $meta_title       = '';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Online Quotation Form';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic" rel="stylesheet" type="text/css">
    <link href="css/icomoon.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/gozha-nav.css" rel="stylesheet" />
    <link href="css/base.css?v=1" rel="stylesheet" />
    <link href="css/style.css?v=1" rel="stylesheet" />
    ';
    
    $add_scripts = '
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.menu.js"></script>
    <script type="text/javascript" src="js/jquery.inview.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-open.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-pro.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="js/custom.js"></script>
    ';

  } 
?>
<!doctype html>
<html><head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title><?php echo $meta_title; ?></title>
        <meta name="description" content="<?php echo $meta_description; ?>">
        <meta name="keywords" content="<?php echo $meta_keywords; ?>">
        <meta name="author" content="<?php echo $meta_author; ?>">

 
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
        <script src="js/modernizr.custom.05768.js"></script>
        <?php echo $add_styles; ?>
        <style>
        .form-padd {
            padding: 7px;
        }
        </style>
    
    <!-- Fonts -->
        <!-- SourceSansPro -->
        
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
 
</head>

<body class=''>
    <div class='wrapper'>
        <?php include('includes/header.php'); ?>
        
        <section class="container waypoint" data-animate-up="nav-strict" data-animate-down="nav-show">
            <div class="service-information">
                <!-- step 1 -->
                <div class="row">
                <div class='col-sm-2'></div>
                <div class="col-sm-4 form padd">

                    <form action="" method="POST">
                        <h3>Step 1</h3>
                        <h2>Contact Details:</h2>
                        <p><label>First Name <span style="color:red;"> * </span></label><input type="text" name="first_name" class="quatation_form_field text_field" /></p>
                        <p><label>Second Name </label><input type="text" name="second_name" class="quatation_form_field text_field"></p>
                        <p><label>Email <span style="color:red;"> * </span></label><input type="text" name="email" class="quatation_form_field text_field"></p>
                        <p><label>Phone Number <span style="color:red;"> * </span></label><input type="text" name="phone_number" class="quatation_form_field text_field" onkeypress="return isNumberKey(event)" ></p>

                </div>
                <div class='col-sm-4'></div>
                <div class='col-sm-2'></div>
                </div>
                <!-- end of step 1 -->
                
                <hr />


                <!-- step 2 -->
                <div class="row">
                <div class='col-sm-2'></div>
                <div class="col-sm-4 form-padd">

                    <h3>Step 2</h3>
                    <h2>Moving From:</h2>
                    <p><label>First Line <span style="color:red;"> * </span></label><input type="text" name="first_line" class="quatation_form_field text_field" /></p>
                    <p><label>Second Line</label><input type="text" name="second_line" class="quatation_form_field text_field"></p>
                    <p><label>Town </label><input type="text" name="town" class="quatation_form_field text_field"></p>
                    <p><label>City <span style="color:red;"> * </span></label><input type="text" name="city" class="quatation_form_field text_field" ></p>
                    <p><label>Post Code <span style="color:red;"> * </span></label><input type="text" name="post_code" class="quatation_form_field text_field" ></p>
                    <h2>Property type:</h2>
                    <p><input type="checkbox" class="chk" name="bungalow" value="Bungalow" /> Bungalow</p>
                    <p><input type="checkbox" class="chk" name="maisonette" value="Maisonette" /> Maisonette</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="1st floor flat" /> 1st floor flat</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="2nd floor flat" /> 2nd floor flat</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="3rd floor flat" /> 3nd floor flat</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="4th floor flat" /> 4nd floor flat</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="1 bed house" /> 1 bed house</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="2 bed house" /> 2 bed house</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="3 bed house" /> 3 bed house</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="4 bed house" /> 4 bed house</p>
                    <p><input type="checkbox" class="chk" name="type_from[]" value="5 bed house" /> 5 bed house</p>

                </div>
                <!-- end of step 2 -->


                <!-- step 3 -->
                <div class='col-sm-4 form-padd'>

                    <h3>Step 3</h3>
                    <h2>Moving To:</h2>
                    <p><label>First Line</label><input type="text" name="to_first_line" class="quatation_form_field text_field"></p>
                    <p><label>Second Line</label><input type="text" name="to_second_line" class="quatation_form_field text_field"></p>
                    <p><label>Town</label><input type="text" name="to_town"class="quatation_form_field text_field"></p>
                    <p><label>City</label><input type="text" name="to_city" class="quatation_form_field text_field"></p>
                    <p><label>Post Code</label><input type="text" name="to_post_code" class="quatation_form_field text_field"></p>
                    <h2>Property type:</h2>
                    <p><input type="checkbox" class="chk" name="type[]" value="Bungalow" /> Bungalow</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="Maisonette" /> Maisonette</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="1st floor flat" /> 1st floor flat</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="2nd floor flat" /> 2nd floor flat</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="3rd floor flat" /> 3nd floor flat</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="4th floor flat" /> 4nd floor flat</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="1 bed house" /> 1 bed house</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="2 bed house" /> 2 bed house</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="3 bed house" /> 3 bed house</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="4 bed house" /> 4 bed house</p>
                    <p><input type="checkbox" class="chk" name="type[]" value="5 bed house" /> 5 bed house</p>

                </div>
                <div class='col-sm-2'></div>
                </div>
                <!-- end of step 3 -->

                <hr />







            </div>
        </section>

     

        <?php include('includes/footer.php'); ?>
    </div>
    <script>window.jQuery || document.write('<script src="js/jquery-1.10.1.min.js"><\/script>')</script>
    <?php echo $add_scripts; ?>



        <script type="text/javascript">
            $(document).ready(function() {
                initSingleService();
            });
        </script>

</body>