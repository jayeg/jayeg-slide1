<?php 


  {/* meta tags */

    $meta_title       = '';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Furniture List';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic" rel="stylesheet" type="text/css">
    <link href="css/icomoon.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/gozha-nav.css" rel="stylesheet" />
    <link href="css/base.css?v=1" rel="stylesheet" />
    <link href="css/style.css?v=1" rel="stylesheet" />
    ';
    
    $add_scripts = '
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.menu.js"></script>
    <script type="text/javascript" src="js/jquery.inview.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-open.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="http://cdn.wijmo.com/jquery.wijmo-pro.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="js/custom.js"></script>
    ';

  } 
?>
<!doctype html>
<html><head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title><?php echo $meta_title; ?></title>
        <meta name="description" content="<?php echo $meta_description; ?>">
        <meta name="keywords" content="<?php echo $meta_keywords; ?>">
        <meta name="author" content="<?php echo $meta_author; ?>">

 
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
        <script src="js/modernizr.custom.05768.js"></script>
        <?php echo $add_styles; ?>
        <style>
        .form-padd {
            padding: 15px;
            margin-top: 19px;
        }
        </style>
    
    <!-- Fonts -->
        <!-- SourceSansPro -->
        
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
 
</head>

<body class=''>
    <div class='wrapper'>
        <?php include('includes/header.php'); ?>
                            <form action="" method="POST">
        <section class="container waypoint" data-animate-up="nav-strict" data-animate-down="nav-show">
            <div class="service-information">
                <div class='row'>
                    <div class='col-md-4'></div>
                    
                    <div class='col-md-4'>
                        <form id="contact-form" novalidate="" name="contact-form" method="post" >
                        <p><label>Customer Name</label><input type='text' name='name'></p>
                        <p><label>Email Address</label><input type='text' name='email'></p>
                        <p><label>Telephone Number</label><input type='text' name='tel'></p>

                        <p><label>Moving Date</label><input type='text' name='date'></p>
                        
                    </div>
                    <div class='col-md-4'></div>
                </div>
                <!-- column 1 -->
                <div class="row">
                <div class="col-sm-4 form-padd">

                        <div class='col-md-12'>
                        <h3>Living Room</h3>
                        <p><input type='checkbox' class='chk' /> Sofa x2 Seater </p>
                        <p><input type='checkbox' class='chk' /> Sofa x3 Seater </p>
                        <p><input type='checkbox' class='chk' /> Armchair </p>
                        <p><input type='checkbox' class='chk' /> Coffee Table </p>
                        <p><input type='checkbox' class='chk' /> Display Cabinets </p>
                        <p><input type='checkbox' class='chk' /> Picture Frames </p>
                        <p><input type='checkbox' class='chk' /> TV Consoles </p>
                        <p><input type='checkbox' class='chk' /> Lamps </p>
                        <p><input type='checkbox' class='chk' /> Mirrors </p>
                        <p><input type='checkbox' class='chk' /> Chest of Dwrs </p>
                        <p><input type='checkbox' class='chk' /> Tables </p>
                        <p><input type='checkbox' class='chk' /> Other </p>

                </div>   
                </div>
                        

                    <div class="col-sm-4 form-padd">
  
                    <div class='col-md-12'>
                    <h3>Kitchen</h3>
                    <p><input type='checkbox' class='chk' /> Oven </p>
                    <p><input type='checkbox' class='chk' /> Fridge </p>
                    <p><input type='checkbox' class='chk' /> Freezer </p>
                    <p><input type='checkbox' class='chk' /> Fr/Frz Combined </p>
                    <p><input type='checkbox' class='chk' /> Washing Machine </p>
                    <p><input type='checkbox' class='chk' /> Tumble Dryer </p>
                    <p><input type='checkbox' class='chk' /> Dish Washer </p>
                    <p><input type='checkbox' class='chk' /> Microwave </p>
                    <p><input type='checkbox' class='chk' /> Coffee Machine </p>
                    <p><input type='checkbox' class='chk' /> No. Of Cupboards </p>
                    <p><input type='checkbox' class='chk' /> Table & Chairs </p>
                    <p><input type='checkbox' class='chk' /> Display Cabinets </p>
                    <p><input type='checkbox' class='chk' /> TV </p>
                    <p><input type='checkbox' class='chk' /> Other </p>
                </div>
                </div>


                    <div class="col-sm-4 form-padd">
                    <h3>Dinning Room</h3>
                    <div class='col-md-8'>
                    <p><input type='checkbox' class='chk' /> Table and Chairs </p>
                    <p><input type='checkbox' class='chk' /> Display Cabinets </p>
                    <p><input type='checkbox' class='chk' /> Pictures </p>
                    <p><input type='checkbox' class='chk' /> Mirrors </p>
                    <p><input type='checkbox' class='chk' /> Lamps </p>
                    <p><input type='checkbox' class='chk' /> Chest Of Drawers </p>
                    <p><input type='checkbox' class='chk' /> TV </p>
                    <p><input type='checkbox' class='chk' /> Other </p>
                </div>
                </div>


                </div>

                <!-- end of column 1 -->


                <hr />


                <!-- column 2 -->
                
                <div class="row">
                <div class="col-sm-4 form-padd">


                        <h3>Bedroom One</h3>
                        <p><input type='checkbox' class='chk' /> Bed Kingsize </p>
                        <p><input type='checkbox' class='chk' /> Bed Queensize </p>
                        <p><input type='checkbox' class='chk' /> Bed Double </p>
                        <p><input type='checkbox' class='chk' /> Bed Single </p>
                        <p><input type='checkbox' class='chk' /> Bedside Cabinets </p>
                        <p><input type='checkbox' class='chk' /> TV </p>
                        <p><input type='checkbox' class='chk' /> Wardrobe </p>
                        <p><input type='checkbox' class='chk' /> Chest Of Drawers </p>
                        <p><input type='checkbox' class='chk' /> Table / Desk </p>
                        <p><input type='checkbox' class='chk' /> Chairs </p>
                        <p><input type='checkbox' class='chk' /> Mirrors </p>
                        <p><input type='checkbox' class='chk' /> Pictures </p>
                        <p><input type='checkbox' class='chk' /> Lamps </p>
                        <p><input type='checkbox' class='chk' /> Other </p>

                   


                </div>
                        

                <div class="col-sm-4 form-padd">
                    <h3>Bedroom Two</h3>
                    <p><input type='checkbox' class='chk' /> Bed Kingsize </p>
                    <p><input type='checkbox' class='chk' /> Bed Queensize </p>
                    <p><input type='checkbox' class='chk' /> Bed Double </p>
                    <p><input type='checkbox' class='chk' /> Bed Single </p>
                    <p><input type='checkbox' class='chk' /> Bedside Cabinets </p>
                    <p><input type='checkbox' class='chk' /> TV </p>
                    <p><input type='checkbox' class='chk' /> Wardrobe </p>
                    <p><input type='checkbox' class='chk' /> Chest Of Drawers </p>
                    <p><input type='checkbox' class='chk' /> Table / Desk </p>
                    <p><input type='checkbox' class='chk' /> Chairs </p>
                    <p><input type='checkbox' class='chk' /> Mirrors </p>
                    <p><input type='checkbox' class='chk' /> Pictures </p>
                    <p><input type='checkbox' class='chk' /> Lamps </p>
                    <p><input type='checkbox' class='chk' /> Other </p>
                </div>


                 <div class="col-sm-4 form-padd">
                    <h3>Bedroom Three</h3>
                    <p><input type='checkbox' class='chk' /> Bed Kingsize </p>
                    <p><input type='checkbox' class='chk' /> Bed Queensize </p>
                    <p><input type='checkbox' class='chk' /> Bed Double </p>
                    <p><input type='checkbox' class='chk' /> Bed Single </p>
                    <p><input type='checkbox' class='chk' /> Bedside Cabinets </p>
                    <p><input type='checkbox' class='chk' /> TV </p>
                    <p><input type='checkbox' class='chk' /> Wardrobe </p>
                    <p><input type='checkbox' class='chk' /> Chest Of Drawers </p>
                    <p><input type='checkbox' class='chk' /> Table / Desk </p>
                    <p><input type='checkbox' class='chk' /> Chairs </p>
                    <p><input type='checkbox' class='chk' /> Mirrors </p>
                    <p><input type='checkbox' class='chk' /> Pictures </p>
                    <p><input type='checkbox' class='chk' /> Lamps </p>
                    <p><input type='checkbox' class='chk' /> Other </p>
                </div>



                </div>
                <!-- end of column 2 -->



                <hr />


                <!-- column 3 -->
                
                <div class="row">
                <div class="col-sm-4 form-padd">


                        <h3>Study</h3>
                        <p><input type='checkbox' class='chk' /> Desk & Chair </p>
                        <p><input type='checkbox' class='chk' /> Book Shelves </p>
                        <p><input type='checkbox' class='chk' /> TV / Computer </p>
                        <p><input type='checkbox' class='chk' /> Chest Of Drawers </p>
                        <p><input type='checkbox' class='chk' /> Pedestals </p>
                        <p><input type='checkbox' class='chk' /> Pictures </p>
                        <p><input type='checkbox' class='chk' /> Mirrors </p>
                        <p><input type='checkbox' class='chk' /> Other </p>

                   


                </div>
                        

                <div class="col-sm-4 form-padd">
                    <h3>Garden</h3>
                    <p><input type='checkbox' class='chk' /> Bikes </p>
                    <p><input type='checkbox' class='chk' /> Large Toys </p>
                    <p><input type='checkbox' class='chk' /> Parasol </p>
                    <p><input type='checkbox' class='chk' /> Table & Chairs </p>
                    <p><input type='checkbox' class='chk' /> BBQ </p>
                    <p><input type='checkbox' class='chk' /> Plant Pots </p>
                    <p><input type='checkbox' class='chk' /> Bench </p>
                    <p><input type='checkbox' class='chk' /> Other </p>
                </div>


                <div class="col-sm-4 form-padd">
                    <p><input type='checkbox' class='chk' /> Ladders </p>
                    <p><input type='checkbox' class='chk' /> Weights </p>
                    <p><input type='checkbox' class='chk' /> Lawnmower </p>
                    <p><input type='checkbox' class='chk' /> Large Tools </p>
                    <p><input type='checkbox' class='chk' /> BBQ </p>
                    <p><input type='checkbox' class='chk' /> Shelves </p>
                    <p><input type='checkbox' class='chk' /> Tools </p>
                    <p><input type='checkbox' class='chk' /> Other </p>
                    </div>


                </div>
                <!-- end of column 3 -->
                <button type="submit" class='btn btn-large btn-success btn-form icon-circled-right btn-animate pull-right'>submit</button>
            </form>


            </div>
        </section>
 </form>
     

        <?php include('includes/footer.php'); ?>
    </div>
    <script>window.jQuery || document.write('<script src="js/jquery-1.10.1.min.js"><\/script>')</script>
    <?php echo $add_scripts; ?>



        <script type="text/javascript">
            $(document).ready(function() {
                initSingleService();
            });
        </script>

</body>