<!DOCTYPE html>
<html>
    <head>
        <title>Precise | Contact Us</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700" rel="stylesheet" media="all">
        <link rel="stylesheet" href="css/buttons/social-icons.css">
        <link rel="stylesheet" href="css/buttons/animation.css"><!--[if IE 7]>
        <link rel="stylesheet" href="css/buttons/social-icons-ie7.css"><![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/media.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <![endif]-->
        <!--[if (gte IE 6)&(lte IE 8)]>
            <script type="text/javascript" src="js/selectivizr-min.js"></script>
        <![endif]-->
    </head>
    <body>
<?php include 'includes/sites/single/files/navbar-static-topnavbar.php';?>
        <div class="site-container">
            <div class="header-eight-box">
                <ul id="header-eight">
                    <li>
                        <img src="content/examples-of-standart-pages.jpg" alt="Examples of standart pages">
                        <div class="slider-border"></div>
                        <div class="title">
                            <h3>examples of standard pages</h3>
                            <div class="clearfix"></div>
                            <h6>Responsive Multi-Purpose Theme</h6>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="site-container">
            <ul class="breadcrumb container-box">
                <li><a href="#">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>
        </div>
        <div class="site-container">
            <div class="container entry-content">
                <div class="row">
                    <div class="span12">
                        <h1 class="page-title">Contact Us</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="span6 contact">
                        <h3>Address</h3>
                        <address>45a College Road, Bromley , BR1 3PU</address>
                        <hr>
                        <h3>Phones</h3>
                        <span><b>Support:</b> +777 (100) 1234</span><br>
                        <span><b>Sales manager:</b> +777 (100) 4321</span><br>
                        <span><b>Director:</b> +777 (100) 1243</span><br>
                        <hr>
                        <h3>Email Addresses</h3>
                        <span><b>Support:</b> <a href="mailto:support@example.com">support@example.com</a></span><br>
                        <span><b>Sales manager:</b> <a href="mailto:manager@example.com">manager@example.com</a></span><br>
                        <span><b>Director:</b> <a href="mailto:chief@example.com">chief@example.com</a></span>
                    </div>
                    <div class="span6 contact-form">
                        <h2>Quick Contact</h2>
                        <form id="contactform" method="POST">
                            <label>Name: <span class="required">*</span></label>
                            <input type="text" name="name">
                            <label>Email Adress: <span class="required">*</span></label>
                            <input type="email" name="email">
                            <label>Telephone</label>
                            <input type="text" name="phone">
                            <label>Comment</label>
                            <textarea name="comment"></textarea>
                            <input class="btn btn-primary" id="submit" type="submit" value="Submit">
                            <div class="required-field pull-right">* Required Field</div>
                        </form>
                        <div id="success"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="site-container">
			<div class="container newsletter-bottom-block">
				<div class="row">
					<div class="span12">
						<div class="image pull-left"></div>
						<div class="title pull-left">
							<h4>Newsletter Sugnup</h4>
							<p>Proin rutrum mattis libero, nec imperdie ipsum vel justo facilisis sed.</p>
						</div>
						<div class="form pull-left">
							<form action="#">
								<input type="text" name="" placeholder="Enter your email adress...">
								<input type="submit" value="Subscribe" class="btn btn-primary">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
        <footer class="site-container">
			<?php include 'includes/containers/footer.php';?>
		</footer>
		<div class="container footer-second">
				<?php include 'includes/containers/footer-copyright.php';?>
		</div>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.formstyler.js"></script>
        <script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
        <script src="js/jquery.touchSwipe.min.js"></script>
        <script src="js/jquery.cookie.js"></script>
        <script src="js/jquery.minicolors.js"></script>
        <script src="js/script.js"></script>
    </body>
</html>