<!DOCTYPE html>
<html>
    <head>
        <title>Precise | Default Store Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="favicon.ico">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700" rel="stylesheet" media="all">
        <link rel="stylesheet" href="css/buttons/social-icons.css">
        <link rel="stylesheet" href="css/buttons/animation.css"><!--[if IE 7]>
        <link rel="stylesheet" href="css/buttons/social-icons-ie7.css"><![endif]-->
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/media.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <![endif]-->
        <!--[if (gte IE 6)&(lte IE 8)]>
            <script type="text/javascript" src="js/selectivizr-min.js"></script>
        <![endif]-->
    </head>
<body>  
<?php include 'includes/sites/single/files/navbar-static-topnavbar.php';?>
	   <div class="second-top-box">
            <div class="container-box">
                <span class="welcome-msg pull-left">Welcome to z4fancy London, please follow us on: </span>
                <ul class="pull-right">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Gooogle+</a></li>
                </ul>
            </div>
        </div>
        <div class="container-box header-two-menu">
			<?php include 'includes/sites/menius/home-prod-nav.php';?>
        </div>
        <div id="boxed" class="header-two-box">
            <ul id="header-two">
                <li>
                    <img src="content/slides/ls-image-2.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Order NR: #455</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-2.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Order NR: #455</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-2.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Order NR: #455</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-2.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Order NR: #455</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-5.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Buy Now</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-6.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Buy Now</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-7.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Buy Now</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-8.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Buy Now</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-9.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Buy Now</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-11.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Buy Now</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="content/slides/ls-image-1.jpg" alt="">
                    <div class="slider-border"><div class="border"></div></div>
                    <div class="title">
                        <span>PRECISE</span>
                        <h3>Product Name</h3>
                        <div class="clearfix"></div>
                        <h6>product sloga</h6>
                        <div class="slider-button-box">
                            <div class="btn-ads-container">
                                <a href="http://themeforest.net/item/precise-multipurpose-responsive-template/4704032?sso?WT.ac=portfolio_item&WT.seg_1=portfolio_item&WT.z_author=InfoStyle" class="btn-ads">
                                    <span class="flip-container">
                                        <span class="back">Only from £17</span>
                                        <span class="front">Buy Now</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="header-two-box-prev"></div>
            <div class="header-two-box-next"></div>
            <div class="header-two-prev visible-desktop"></div>
            <div class="header-two-next visible-desktop"></div>
            <div class="header-two-pager"></div>
        </div>
        <div class="site-container">
            <div class="container entry-content">
                <div class="row">
                    <div class="span12">
                        <div class="row home-banner">
                            <a href="#" class="span4 banner"><img src="content/sample-benner-1.png" alt=""></a>
                            <a href="#" class="span4 banner"><img src="content/sample-benner-2.png" alt=""></a>
                            <a href="#" class="span4 banner"><img src="content/sample-benner-3.png" alt=""></a>
                        </div>
                        <div class="carousel-grid home">
                            <div class="title-two"><div>&nbsp;</div><div>Recommended Products</div><div class="carousel-pager one"></div></div>
                            <ul class="product-grid">
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Order NR: #122</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                      <a href="#" class="btn btn-primary">Order NR: #122</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                      <a href="#" class="btn btn-primary">Order NR: #122</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                   <a href="#" class="btn btn-primary">Order NR: #122</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                                <li>
                                    <article>
                                        <div class="border-top"></div>
                                        <div class="border-left"></div>
                                        <div class="border-right"></div>
                                        <div class="border-bottom"></div>
         <a href="product-view.html"><img src="https://scontent-b-lhr.xx.fbcdn.net/hphotos-ash3/t1/994695_781391308541443_1294144762_n.jpg" alt=""></a>
                                        <div class="product-caption">
                                            <div class="price">
                                                <span>£399.00</span>
                                            </div>
                                            <div class="inner">
                                                <h3 class="title"><a href="product-view.html" title="Caramel & Black Collared BODY Dress">Caramel & Black Collared BODY Dress</a></h3>
                                                <div class="cart-button">
                                                    <a href="#" class="btn btn-primary">Add to cart</a>
                                                   <!-- <a href="#" class="wishlist-link">Add to wishlist</a> -->
                                                   <!--  <a href="#" class="compaire-link">Add to compare</a> -->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                            <div class="carousel-prev">&lt;</div>
                            <div class="carousel-next">&gt;</div>
                        </div>
                    </div>
                    <a href="#" class="banner large-banner span12"><img src="content/banner-full-width.png" alt="Full-width banner"></a>
                    <div class="clearfix"></div>
                    <div class="span12">
                        <div class="carousel-grid home brands">
                            <div class="title-two"><div>&nbsp;</div><div>Best <br class="visible-567"> Seller</div><div class="carousel-pager one"></div></div>
                            <ul class="content-list">
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
<li><a href="#"><img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-ash4/t1/1425602_782829978397576_1724691607_n.jpg" alt=""></a></li>
                            </ul>
                            <div class="carousel-prev">&lt;</div>
                            <div class="carousel-next">&gt;</div>
                        </div>
                    </div>
                </div>
                <div class="row home-bottom-block-box">
                    <div class="span12 home-bottom-block">
                        <div class="row">
                            <div class="span4 about-us">
                                <div class="title">About Us</div>
                                <div class="page-description">In congue nisl varius quis aliqut lentesque a tellus quam  variu. VelL varius lobortis gravida. </div>
                                <p>
                                    Lorem ipsum, libero et luctus molestie, turpis mi sagittis nisl, a scelerisque leo nulla et 
                                    lectus pendis dictum posuere elit, in congue nisl varius lentesque a tellus eget quam varius aliquet.
                                </p>
                                <p>
                                    Pellentesque tristique, libero et luctus molestie, a scelerisque leo nulla et lectus pendisse 
                                    dictum posuere elit. It is a long established fact that a reade will be distracted by the 
                                    readable content of a page when looking at its layout.  
                                </p>
                                <a href="#" class="btn btn-link btn-mini">More</a>
                            </div>
                            <div class="span4 news">
                                <div class="title">News</div>
                                <ul>
                                    <li>
                                        <div class="img">
                                            <a href="#"><img src="content/news-img-4.png" alt=""></a>
                                        </div>
                                        <div class="content">
                                            <time datetime="2013-10-03">3 january 2013</time>
                                            <p>
												<a href="#">Fermentum parturient lacus tristique habit nullam morbi  mus dictum.</a>
											</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img">
                                            <a href="#"><img src="content/news-img-3.png" alt=""></a>
                                        </div>
                                        <div class="content">
                                            <time datetime="2013-10-02">2 january 2013</time>
                                            <p>
												<a href="#">Cras ac hendrerit dui. Duis lacus ligula, luctus ac tristique eget, posuere id erat.</a>
											</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img">
                                            <a href="#"><img src="content/news-img-2.png" alt=""></a>
                                        </div>
                                        <div class="content">
                                            <time datetime="2013-10-01">1 january 2013</time>
                                            <p>
												<a href="#">Many desktop publishing packages and web page editors now use.</a>
											</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="img">
                                            <a href="#"><img src="content/news-img-1.png" alt=""></a>
                                        </div>
                                        <div class="content">
                                            <time datetime="2013-10-01">1 january 2013</time>
                                            <p>
												<a href="#">Lorem ipsum, libero sagittis nisl, a scelerisque leo nulla et lectus.</a>
											</p>
                                        </div>
                                    </li>
                                </ul>
                                <a href="#" class="btn btn-link btn-mini">More</a>
                            </div>
                            <div class="span4 follow-us">
                                <div class="title">Follow Us</div>
<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FZ4fancylondon%3Fskip_nax_wizard%3Dtrue&amp;width&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:290px;" allowTransparency="true"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row call-us-box">
                    <div class="span12 call-us">
                        <span>Call Us: 020 3609 0040</span>
                    </div>
                </div>
            </div>
        </div>
        <footer class="site-container">
			<?php include 'includes/containers/footer.php';?>
		</footer>
		<div class="container footer-second">
			<?php include 'includes/containers/footer-copyright.php';?>
		</div>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.formstyler.js"></script>
        <script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
        <script src="js/jquery.touchSwipe.min.js"></script>
        <script src="js/jquery.cookie.js"></script>
        <script src="js/jquery.minicolors.js"></script>
        <script src="js/script.js"></script>
    </body>
</html>