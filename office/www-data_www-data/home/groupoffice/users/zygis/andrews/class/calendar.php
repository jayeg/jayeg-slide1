<?php
class Calendar {

	public $todays_day;
	public $opening_hours;

	public function setTodaysDay() {

		$this->todays_day = date('D');

	}

	public function openingHours() {

		if($this->todays_day == 'Mon') { $this->opening_hours = '8am - 5pm'; }
		if($this->todays_day == 'Tue') { $this->opening_hours = '8am - 6pm'; }
		if($this->todays_day == 'Wed') { $this->opening_hours = '8am - 6pm'; }
		if($this->todays_day == 'Thu') { $this->opening_hours = '8am - 6pm'; }
		if($this->todays_day == 'Fri') { $this->opening_hours = '8am - 6pm'; }
		if($this->todays_day == 'Sat') { $this->opening_hours = '8am - 4.30pm'; }
		if($this->todays_day == 'Sun') { $this->opening_hours = '8am - 3pm'; }

	}

    public function getOpeningHours() {

    	return $this->opening_hours;
    }
	
}

?>