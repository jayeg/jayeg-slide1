<?php

{/* meta tags */

    $meta_title       = 'contact';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'contact';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Oswald|Lato|Roboto:400,500,700|Open+Sans:600,700,300" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <link href="css/datepicker.css" rel="stylesheet">
    ';
    
    $add_scripts = '
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script defer src="js/jquery.flexslider.js"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/fullcalendar.js"></script>
    <script type="text/javascript" src="js/google.map.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    ';

  } 

  ?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>

<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">
<meta name="author" content="<?php echo $meta_author; ?>">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php echo $add_styles; ?>
<!-- Html5 Js -->
<script src="js/html5.js" type="text/javascript"></script>
</head>

<body>
<!--Wrapper Start-->
<div id="wrapper"> 
  <!--Header Start-->
  <header id="header">
    <div class="container">
      <div class="row-fluid">
        <div class="span3">
          <div class="logo"><a href="index.php"><img src="images/logo-img.png" alt="img"></a></div>
        </div>
        <div class="span9 margin-non">
          <div class="top-container"></div>
          <!-- Nav Start -->
          <?php include('includes/navigation.php'); ?>
          <!--Nav End--> 
        </div>
      </div>
    </div>
  </header>
  <!--Header End--> 
  
  <!--Banner Start-->
  <section id="banner" class="height">
    <div class="contact-banner"><img src="images/inner-banner-1.jpg" alt="img"></div>
  </section>
  <!--Banner End--> 
  
  <!--Content Area Start-->
  <section id="content" class="container"> 
    <!--Contact Map Area Start-->
    <section class="map-section">
      <div class="row-fluid">
        <div class="span12">
          <div class="heading">
            <h1>Contact Us</h1>
            <span class="flag-icon">Flag</span></div>
          <div class="map-box-2">
            <div id="map_canvas" style="width: 100%; height:426px; vertical-align:central;"><br/>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--Contact Map Area End--> 
    
    <!--Opening Section Start-->
    <section class="opening-section">
      <div class="row-fluid">
        <div class="span8">
          <div class="row-fluid">
            <div class="span6">
              <div class="book-tabel-box">
                <div class="head">
                  <h2>Book a Table</h2>
                  <span class="book-calendar"></span> </div>
                <form action="#" class="book-table-form">
                  <div class="book-box-1">
                    <label>Date</label>
                    <div class="input-append date date-box" id="dp3" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
                      <input class="span12 date-box-input" size="16" type="text" value="12-02-2012">
                      <span class="add-on date-box-add"><i class="icon-th"></i></span> </div>
                  </div>
                  <div class="book-box-1">
                    <label>No. of Person</label>
                    <div class="date-box">
                      <select name="">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                      </select>
                    </div>
                  </div>
                  <input class="btn-continue" type="submit" value="Continue">
                </form>
              </div>
            </div>
            <div class="span6">
              <div class="social-box-area">
                <div class="head">
                  <h2>We are Social</h2>
                  <span class="page"></span> </div>
                <ul class="social-area">
                  <li class="fb"><a href="https://www.facebook.com/pages/JAndrews-Of-Bromley/194978553992377" target="_blank"></a></li>
  
        
                  <li class="gtalk"><a href="#"></a></li>
         
                </ul>
                <div class="social-input">
                  <form class="social-form">
                    <label>Get all the latest news</label>
                    <input type="text" placeholder="Type Email Address" class="span8 input-social">
                    <button class="btn btn-submit" type="submit">Submit</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="span12 margin-non">
            <div class="happy-hours-box">
              <h3>Happy Hours</h3>
              <span class="clock">clock</span>
              <p>Join us for Happy Hour! Enjoy discount baked goods &amp; have it with a smile on your face without<br>
                any burdon in your pocket . 5pm-7pm daily</p>
            </div>
          </div>
        </div>
        <div class="span4">
          <div class="opening-hours-box">
            <div class="head">
              <h2>Opening Hours</h2>
              <span class="watch"></span> </div>
            <div class="chart-sheet">
              <ul>
                <li> <strong class="day">Monday</strong> <strong class="time">8am - 5pm</strong> </li>
                <li class="opening-active"> <strong class="day">Tuesday</strong> <strong class="time">8am - 6pm</strong> </li>
                <li> <strong class="day">Wednesday</strong> <strong class="time">8am - 6pm</strong> </li>
                <li class="opening-active"> <strong class="day">Thursday</strong> <strong class="time">8am - 6pm</strong> </li>
                <li> <strong class="day">Friday</strong> <strong class="time">8am - 6pm</strong> </li>
                <li class="opening-active"> <strong class="day">Satuarday</strong> <strong class="time">8am - 4.30pm</strong> </li>
                <li> <strong class="day">Sunday</strong> <strong class="time">10am - 3pm</strong> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--Opening Section End--> 
    
    <!--Form Section Start-->
    <section class="contact-form-section">
      <div class="row-fluid">
        <div class="span8">
          <div class="contact-form">
            <div class="head">
              <h2>Leave Us a Message</h2>
              <span class="email">email</span> </div>
            <form action="contact.php" method="post">
              <ul class="form-list">
                <li class="span4">
                  <input type="text" required pattern="[a-zA-Z ]+" name="name" value="Name*" class="contact-input">
                </li>
                <li class="span4">
                  <input type="text" required pattern="^[a-zA-Z0-9-\_.]+@[a-zA-Z0-9-\_.]+\.[a-zA-Z0-9.]{2,5}$" name="email" value="Email*" class="contact-input">
                </li>
                <li class="span4">
                  <input type="text" name="website" value="Website" class="contact-input">
                </li>
                <li class="span12 margin-non">
                  <textarea rows="10" cols="10" name="comments" class="textarea">Comment*</textarea>
                </li>
                <li class="span2 margin-non">
                  <input type="submit" name="submit" value="Continue" class="btns btn-continue">
                </li>
              </ul>
            </form>
          </div>
        </div>
        <div class="span4">
          <div class="address-box">
            <div class="head">
              <h3>Address</h3>
              <span class="location">location</span> </div>
            <div class="address-area">
              <ul>
                <li class="location">81 Burnt Ash Lane<br>
                  Bromley, Kent, BR1 5AA</li>
                <li class="call"> <strong class="phone">020 8402 4123</strong> </li>
                <li class="email"> <a href="#" class="email">info@jandrewbutchers.co.uk</a> <span class="small">(And we will respond you right away)</span> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--Form Section End--> 
  </section>
  <!--Content Area End--> 
  
<?php include('includes/footer.php'); ?>
  
</div>
<?php echo $add_scripts; ?>

<!--Wrapper End-->
</body>
</html>
