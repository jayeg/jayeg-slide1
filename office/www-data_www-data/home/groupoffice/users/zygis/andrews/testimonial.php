<?php

{/* meta tags */

    $meta_title       = 'index';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'index';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Oswald|Lato|Roboto:400,500,700|Open+Sans:600,700,300" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <link href="css/datepicker.css" rel="stylesheet">
    ';
    
    $add_scripts = '
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script defer src="js/jquery.flexslider.js"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/fullcalendar.js"></script>
    <script type="text/javascript" src="js/google.map.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    ';

  } 

  ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>

<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">
<meta name="author" content="<?php echo $meta_author; ?>">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php echo $add_styles; ?>
<!-- Html5 Js -->
<script src="js/html5.js" type="text/javascript"></script>
</head>

<body>
<!--Wrapper Start-->
<div id="wrapper"> 
  <!--Header Start-->
  <header id="header">
    <div class="container">
      <div class="row-fluid">
        <div class="span3">
          <div class="logo"><a href="index.php"><img src="images/logo-img.png" alt="img"></a></div>
        </div>
        <div class="span9 margin-non">
          <div class="top-container"></div>
           <!-- Nav Start -->
          <?php include('includes/navigation.php'); ?>
          <!--Nav End--> 
        </div>
      </div>
    </div>
  </header>
  <!--Header End--> 
  
  <!--Banner Start-->
  <section id="banner" class="height">
    <div class="contact-banner"><img src="images/inner-banner-1.jpg" alt="img"></div>
  </section>
  <!--Banner End--> 
  
  <!--Content Area Start-->
  <section id="content" class="container"> 
    <!--Blog Page Section Start-->
    <section class="blog-page-section">
      <div class="row-fluid">
        <div class="span12">
          <div class="heading">
            <h1>Testimonials</h1>
          </div>
        </div>
        <!--Blog Post End--> 
        
      </div>
    </section>
    <!--Blog Page Section End--> 
    
    <!--Testimonial Start-->
    <section class="testimonial-page">
      <div class="row-fluid">
        <ul>
          <li>
            <div class="span2">
              <div class="testo-frame-box">
                <!--<div class="testo-frame"><img src="images/testimonial-page-img1.jpg" alt="img"></div>-->
                <strong class="name"></strong></div>
            </div>
            <div class="span10">
              <div class="testo-text">
                <blockquote>
                  <p>Lovely local butchers. I live the other side of Bromley but always come here for a good deal and friendly service. X</p>
                </blockquote>
                <div class="testo-bottom">
                  <div class="testo-rating-box"> <strong class="designation">Rating :</strong>
                    <ul>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                    </ul>
                  </div>
                  <!--<div class="replies-box"> <strong class="replies">3 replies</strong> </div>-->
                  <strong class="name">Caron - Mole</strong> </div>
              </div>
            </div>
          </li>
          <li>
            <div class="span2">
              <div class="testo-frame-box">
                <!--<div class="testo-frame"><img src="images/testimonial-page-img5.jpg" alt="img"></div>-->
                <strong class="name"></strong></div>
            </div>
            <div class="span10">
              <div class="testo-text">
                <blockquote>
                  <p>My father Colin has been coming here for years and nothing is ever to much for you guys. hats off to a local business doing so well.</p>
                </blockquote>
                <div class="testo-bottom">
                  <div class="testo-rating-box"> <strong class="designation">Rating :</strong>
                    <ul>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                    </ul>
                  </div>
                  <!--<div class="replies-box"> <strong class="replies">3 replies</strong> </div>-->
                  <strong class="name">Gavin - Prosser</strong> </div>
              </div>
            </div>
          </li>
          <li>
            <div class="span2">
              <div class="testo-frame-box">
                <!--<div class="testo-frame"><img src="images/testimonial-page-img2.jpg" alt="img"></div>-->
                <strong class="name"></strong></div>
            </div>
            <div class="span10">
              <div class="testo-text">
                <blockquote>
                  <p>Fabulous fresh food and amazing friendly service! BBQ season is here and this is the butchers to go to! x</p>
                </blockquote>
                <div class="testo-bottom">
                  <div class="testo-rating-box"> <strong class="designation">Rating :</strong>
                    <ul>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li><a href="#">star</a></li>
                    </ul>
                  </div>
                  <!--<div class="replies-box"> <strong class="replies">3 replies</strong> </div>-->
                  <strong class="name">Nicola - Hayes</strong> </div>
              </div>
            </div>
          </li>
          <li>
            <div class="span2">
              <div class="testo-frame-box">
                <!--<div class="testo-frame"><img src="images/testimonial-page-img3.jpg" alt="img"></div>-->
                <strong class="name"></strong></div>
            </div>
            <div class="span10">
              <div class="testo-text">
                <blockquote>
                  <p>Highest quality tip top service highly recommended best butcher ever our business thrives on best quality who supply us we wouldn't buy anywhere else brilliant butchers x</p>
                </blockquote>
                <div class="testo-bottom">
                  <div class="testo-rating-box"> <strong class="designation">Rating :</strong>
                    <ul>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                      <li class="active"><a href="#">star</a></li>
                    </ul>
                  </div>
                  <!--<div class="replies-box"> <strong class="replies">3 replies</strong> </div>-->
                  <strong class="name">Sandy - Brooker</strong> </div>
              </div>
            </div>
          </li>
        
        </ul>
      </div>
    </section>
    <!--Testimonial End--> 
    
  </section>
  <!--Content Area End--> 
  
<?php include('includes/footer.php'); ?>
  
</div>


<!--Wrapper End-->
<?php echo $add_scripts; ?>
</body>
</html>
