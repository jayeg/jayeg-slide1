<?php

{/* meta tags */

    $meta_title       = 'Gallery';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Gallery';

    $add_styles       = '
    <link href="http://fonts.googleapis.com/css?family=Oswald|Lato|Roboto:400,500,700|Open+Sans:600,700,300" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <link href="css/datepicker.css" rel="stylesheet">
    ';
    
    $add_scripts      = '
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script defer src="js/jquery.flexslider.js"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/fullcalendar.js"></script>
    <script type="text/javascript" src="js/google.map.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    ';

  } 

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>

<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">
<meta name="author" content="<?php echo $meta_author; ?>">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php echo $add_styles; ?>


<!-- Html5 Js -->
<script src="js/html5.js" type="text/javascript"></script>
</head>

<body>
<!--Wrapper Start-->
<div id="wrapper"> 
  <!--Header Start-->
  <header id="header">
    <div class="container">
      <div class="row-fluid">
        <div class="span3">
          <div class="logo"><a href="index.php"><img src="images/logo-img.png" alt="img"></a></div>
        </div>
        <div class="span9 margin-non">
          <div class="top-container"></div>
          <!-- Nav Start -->
          <?php include('includes/navigation.php'); ?>
          <!--Nav End--> 
        </div>
      </div>
    </div>
  </header>
  <!--Header End--> 
  
  <!--Banner Start-->
  <section id="banner" class="height">
    <div class="contact-banner"><img src="images/inner-banner-1.jpg" alt="img"></div>
  </section>
  <!--Banner End--> 
  
  <!--Content Area Start-->
  <section id="content"> 
    <!--Blog Page Section Start-->
    <section class="blog-page-section">
      <div class="container">
        <div class="row-fluid">
          <div class="span12">
            <div class="heading">
              <h1>Gallery</h1>
            </div>
          </div>
        </div>
      </div>
      <!--Blog Post End--> 
      
      <!--Gallery Start-->
      <section class="gallery">
        <div class="container">
          <div class="row-fluid">
            <ul class="bxslider">
              <li> <img src="images/gallery-img-1.jpg" alt="img" />
                <div class="caption">
                  <div class="gallery-caption"> <strong class="title">Rem isoum dolor </strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at nibh varius odio sollicitudin blandit</p>
                  </div>
                </div>
              </li>
              <li> <img src="images/gallery-img-2.jpg" alt="img" />
                <div class="caption">
                  <div class="gallery-caption"> <strong class="title">Rem isoum dolor </strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at nibh varius odio sollicitudin blandit</p>
                  </div>
                </div>
              </li>
              <li><img src="images/gallery-img-3.jpg" alt="img" />
                <div class="caption">
                  <div class="gallery-caption"> <strong class="title">Rem isoum dolor </strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at nibh varius odio sollicitudin blandit</p>
                  </div>
                </div>
              </li>
              <li> <img src="images/gallery-img-4.jpg" alt="img" />
                <div class="caption">
                  <div class="gallery-caption"> <strong class="title">Rem isoum dolor </strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at nibh varius odio sollicitudin blandit</p>
                  </div>
                </div>
              </li>
              <li> <img src="images/gallery-img-5.jpg" alt="img" />
                <div class="caption">
                  <div class="gallery-caption"> <strong class="title">Rem isoum dolor </strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at nibh varius odio sollicitudin blandit</p>
                  </div>
                </div>
              </li>
            </ul>
            <div id="bx-pager"> <a data-slide-index="0" href=""><img src="images/gallery-thumbnil-img-1.jpg" alt="img" /></a> <a data-slide-index="1" href=""><img src="images/gallery-thumbnil-img-2.jpg" alt="img" /></a> <a data-slide-index="2" href=""><img src="images/gallery-thumbnil-img-3.jpg" alt="img" /></a> <a data-slide-index="3" href=""><img src="images/gallery-thumbnil-img-4.jpg" alt="img" /></a> <a data-slide-index="4" href=""><img src="images/gallery-thumbnil-img-5.jpg" alt="img" /></a> </div>
          </div>
        </div>
      </section>
      <!--Gallery End--> 
    </section>
    <!--Blog Page Section End--> 
  </section>
  <!--Content Area End--> 
  
  <?php include('includes/footer.php'); ?>
  
</div>

<?php echo $add_scripts; ?>

<script>
$('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
  
});
</script> 
<!--Wrapper End-->
</body>
</html>
