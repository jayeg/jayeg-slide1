<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);
require_once('class/calendar.php'); 
$calendar = new Calendar;
$calendar->setTodaysDay();
$calendar->openingHours();

$opening_hours = $calendar->getOpeningHours();

  {/* meta tags */

    $meta_title       = 'index';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'index';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Oswald|Lato|Roboto:400,500,700|Open+Sans:600,700,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <link rel="stylesheet" type="text/css" href="skins/tango/skin.css" />
    ';
    
    $add_scripts = '
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script defer src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/fullcalendar.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/excanvas.js"></script>
    <script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="js/jquery.fitvids.js"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
    ';

  } 

?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>

<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">
<meta name="author" content="<?php echo $meta_author; ?>">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php echo $add_styles; ?>

<script src="js/html5.js" type="text/javascript"></script>
</head>

<body>
<!--Wrapper Start-->
<div id="wrapper"> 
  <!--Header Start-->
  <header id="header"> 
    <div class="container">
      <div class="row-fluid">
        <div class="span3">
          <div class="logo"><a href="index.php"><img src="images/aoblogo.jpg" alt="img"></a></div>
        </div>
        <div class="span9 margin-non">
          <div class="top-container">
            <div class="span5">&nbsp;</div>
            <div class="span7">&nbsp;</div>
          </div>
          <!-- Nav Start -->
          <?php include('includes/navigation.php'); ?>
          <!--Nav End--> 
        </div>
      </div>
    </div>
  </header>
  <!--Header End--> 
  
  <!--Banner Start-->
  <section id="banner">
    <div class="caption">
      <div class="banner-caption">
        <div class="container">
          <div class="row-fluid">
            <div class="span3 banner-left">
              <ul class="bxslider-2">
                <li>
                  <div class="caption-left">
                    <div class="deal-tag"> <strong class="title">Weekly Specials</strong> </div>
                    <div class="left-caption-box"> <strong class="title">Deliciousness<br>
                      made for you</strong> <a href="#" class="find">Order Now</a> <img src="images/chef-img.png" alt="img" class="chef-img"> </div>
                  </div>
                </li>
                <li>
                  <div class="caption-left">
                    <div class="deal-tag"> <strong class="title">Weekly Specials</strong> </div>
                    <div class="left-caption-box"> <strong class="title">Deliciousness<br>
                      made for you</strong> <a href="#" class="find">Order Now</a> <img src="images/chef-img.png" alt="img" class="chef-img"> </div>
                  </div>
                </li>
                <li>
                  <div class="caption-left">
                    <div class="deal-tag"> <strong class="title">Weekly Specials</strong> </div>
                    <div class="left-caption-box"> <strong class="title">Deliciousness<br>
                      made for you</strong> <a href="#" class="find">Order Now</a> <img src="images/chef-img.png" alt="img" class="chef-img"> </div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="span9 right-panel">
              <div class="caption-right"> <strong class="title"><span class="text">fresh products everyday</span></strong> <strong class="open">Open all week</strong> </div>
              <div class="caption-center"> <strong class="title">Andrews has been serving the population of Bromley and its surrounding areas for over 60 years. Pride ourselves to provide our customers organic quality meat, quality service and value since 1948.</span></strong> <a href="menu-full.php" class="download">CHECK OUT OUR MENU</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <ul class="bxslider">
      <li><img src="images/home-banner-1.jpg" alt="img" /></li>
      <li><img src="images/banner-img-2.jpg" alt="img" /></li>
      <li><img src="images/banner-img-3.jpg" alt="img" /></li>
      <li><img src="images/banner-img-4.jpg" alt="img" /></li>
      <li><img src="images/banner-img-5.jpg" alt="img" /></li>
      <li><img src="images/banner-img-6.jpg" alt="img" /></li>
    </ul>
  </section>
  <!--Banner End--> 
  
  <!--Content Area Start-->
  
  <section id="content" class="container">
    <section class="featured-section">
      <div class="row-fluid">
        <div class="span3"> 
          
          <!-- Start Accordian List Menu -->
          <div class="accordion accordion-area" id="accordion2">
            <div class="accordion-group">
              <div class="accordion-heading active"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Beef Menu</a> </div>
              <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                  <div class="content-2 scroll" id="content_2">
                    <ul class="accordion-list">
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Pork Menu</a> </div>
              <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                  <div class="content-2 scroll" id="content_3">
                    <ul class="accordion-list">
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">Lamb Menu</a> </div>
              <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                  <div class="content-2 scroll" id="content_4">
                    <ul class="accordion-list">
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapsefour">Chicken Menu</a> </div>
              <div id="collapsefour" class="accordion-body collapse">
                <div class="accordion-inner">
                  <div class="content-2 scroll" id="content_5">
                    <ul class="accordion-list">
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-2.jpg" alt="img">
                          <div class="caption"><strong class="title">Mango juice with honey</strong></div>
                          <strong class="price">&pound;9.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                      <li>
                        <div class="frame"> <img src="images/breakfast-img-1.jpg" alt="img">
                          <div class="caption"><strong class="title">Crosso with expresso</strong></div>
                          <strong class="price">&pound;15.00</strong> </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- End Accordian List Menu --> 
        </div>
        <div class="span9">
          <div class="heading">
            <h2>Featured Products</h2>
          </div>
          <div class="featured-slider">
            <div id="wrap">
              <ul id="mycarousel" class="jcarousel-skin-tango">
                <li><img src="images/featured-dishes-img1.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;35.00</strong> </div>
                  </div>
                  <strong class="hover-panel">WINGS</strong> </li>
                <li><img src="images/featured-dishes-img2.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;17.00</strong> </div>
                  </div>
                  <strong class="hover-panel">DRUMSTICKS</strong> </li>
                <li><img src="images/featured-dishes-img3.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;12.00</strong> </div>
                  </div>
                  <strong class="hover-panel">SUPREMES</strong> </li>
                <li><img src="images/featured-dishes-img1.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;35.00</strong> </div>
                  </div>
                  <strong class="hover-panel">CHINESE RIBS</strong> </li>
                <li><img src="images/featured-dishes-img2.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;17.00</strong> </div>
                  </div>
                  <strong class="hover-panel">SPARE RIB</strong> </li>
                <li><img src="images/featured-dishes-img3.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;12.00</strong> </div>
                  </div>
                  <strong class="hover-panel">LOIN STEAK</strong> </li>
                <li><img src="images/featured-dishes-img1.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;35.00</strong> </div>
                  </div>
                  <strong class="hover-panel">HAWAIIAN PORK</strong> </li>
                <li><img src="images/featured-dishes-img2.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;17.00</strong> </div>
                  </div>
                  <strong class="hover-panel">STUFFED LION</strong> </li>
                <li><img src="images/featured-dishes-img3.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;12.00</strong> </div>
                  </div>
                  <strong class="hover-panel">RUMP STEAKS</strong> </li>
                <li><img src="images/featured-dishes-img1.jpg" alt="img" />
                  <div class="caption">
                    <div class="fetured-caption"> <strong class="price">&pound;35.00</strong> </div>
                  </div>
                  <strong class="hover-panel">RACK OF PORK</strong> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--Blog Section Start-->
    <section class="blog-section">
      <div class="row-fluid">
        <div class="span8">
          <div class="span12">
            <div class="row-fluid">
              <div class="head">
                <h2>Latest from Our Blog</h2>
              </div>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span12">
              <div class="span8">
                <div class="left-box">
                  <div class="head"> <strong class="date">21<span class="small">SEP 2013</span></strong>
                    <div class="top-row"> <strong class="name">By: <span class="light">James Howard</span></strong> <strong class="name">Comments: <span class="light">20</span></strong>
                      <h3>Food for cardio concious people</h3>
                    </div>
                  </div>
                  <div class="row-fluid">
                    <div class="blog-frame span6"><img src="images/post-img.jpg" alt="img"></div>
                    <div class="blog-text span6">
                      <h5>Cheese SALAD</h5>
                      <p>Curabitur blandit tempus porttitor. Donec sed odio dui. Mcen nterdum. Nullam id dolor id nibh ultricies vehicula ut id el esti ta felis euismod semper. Morbi leo risus, porta ac constur ac os. </p>
                      <p>Nullam id dolor id nibh ultricies vehicula ut id elit. osuer e consectetur est at lobortis. Donec id elit non mi portvida a t eget metus. </p>
                      <p>Praesent commodo cursus magna. <a href="#" class="plus">[+]</a></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="span4">
                <div class="food-detail">
                  <ul>
                    <li> <strong class="date">15<span class="mnt">SEP</span></strong>
                      <div class="frame"><img src="images/food-img-1.jpg" alt="img"></div>
                      <div class="text"> <strong class="title">Food for thought</strong> <strong class="name">by: Anna Smith</strong> </div>
                    </li>
                    <li> <strong class="date">11<span class="mnt">SEP</span></strong>
                      <div class="frame"><img src="images/food-img-2.jpg" alt="img"></div>
                      <div class="text"> <strong class="title">Bombay Biryani</strong> <strong class="name">by: Surabh Dev</strong> </div>
                    </li>
                    <li> <strong class="date">06<span class="mnt">SEP</span></strong>
                      <div class="frame"><img src="images/food-img-3.jpg" alt="img"></div>
                      <div class="text"> <strong class="title">Special Coffee</strong> <strong class="name">by: Tai Ko Ann</strong> </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="span4">
          <div class="food-critic-box">
            <div class="food-critic-header">
              <h3>Caron - Mole </h3>
              <strong class="description">Testimonial</strong> </div>
            <div class="empty-border"></div>
            <div class="review-box">
              <div class="client-review-frame"><img src="images/client-review-img1.png" alt="img"></div>
              <a href="#" class="chat">chat</a>
              <ul class="review-star">
                <li><a href="#">start</a></li>
                <li><a href="#">start</a></li>
                <li><a href="#">start</a></li>
                <li><a href="#">start</a></li>
                <li><a href="#">start</a></li>
              </ul>
              <blockquote class="review-blockquote">
                <p>Lovely local butchers. I live the other side of Bromley but always come here for a good deal and friendly service. X</p>
              </blockquote>
              <form action="#" class="form-receipe">
                <textarea class="textarea height-less" name="comments" cols="10" rows="10" title="textarea"></textarea>
                <input type="submit" class="btn btn-review" value="+ ADD YOUR REVIEW" name="submit" title="submit">
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--Blog Section End--> 
    
    <!--Reservation Section Start
    <section class="reservation-section">
      <div class="row-fluid"> <img src="images/happy-hour-banner.png" alt="img"> </div>
    </section>
    Reservation Section End-->
    
    <section class="event-section">
      <div class="row-fluid">
        <div class="span5">
          <div class="row-fluid">
            <div class="span6">
              <div class="calendar-left"> <strong class="day"><?php echo date('D'); ?></strong> <strong class="date"><?php echo date('d'); ?></strong> <strong class="time"><?php echo $opening_hours; ?></strong> <strong class="title">FOOD GALA</strong> </div>
            </div>
            <div class="span6 margin-non">
              <div id='calendar'></div>
            </div>
          </div>
        </div>
        <div class="span4">
          <div class="events-box">
            <div class="events-box-head">
              <div class="bg-opacity-3">
                <h5>Events &amp; Happenings</h5>
              </div>
            </div>
            <div id="content_1" class="content scroll">
              <ul>
                <li> <img src="images/events-img.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">24<span class="mnt">sep</span></strong> <strong class="title">Thai Food Carnival</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
                <li> <img src="images/events-img2.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">16<span class="mnt">sep</span></strong> <strong class="title">Baking Competition</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
                <li> <img src="images/events-img2.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">16<span class="mnt">sep</span></strong> <strong class="title">Baking Competition</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
                <li> <img src="images/events-img2.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">16<span class="mnt">sep</span></strong> <strong class="title">Baking Competition</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
                <li> <img src="images/events-img2.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">16<span class="mnt">sep</span></strong> <strong class="title">Baking Competition</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
                <li> <img src="images/events-img2.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">16<span class="mnt">sep</span></strong> <strong class="title">Baking Competition</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
                <li> <img src="images/events-img2.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">16<span class="mnt">sep</span></strong> <strong class="title">Baking Competition</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
                <li> <img src="images/events-img2.jpg" alt="img">
                  <div class="caption">
                    <div class="text"> <strong class="date">16<span class="mnt">sep</span></strong> <strong class="title">Baking Competition</strong>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="span3">
          <div class="location-map">
            <div class="location-map-head">
              <div class="left-side">
                <h5>Meet us!</h5>
                <p>81 Burnt Ash Lane</p>
                <p>Bromley, Kent, BR1 5AA</p>
                
              </div>
              <div class="right-side"> <a href="#" class="mark">&nbsp;</a> <a href="#" class="location">&nbsp;</a> </div>
            </div>
            <div class="map-box">
              <div id="map_canvas" style="width:100%; height:205px; vertical-align:central;"><br/>
              </div>
            </div>
            <div class="bottom-sec"> <strong class="time"></strong> <a href="#" class="week">ALL WEEK</a> </div>
          </div>
        </div>
      </div>
    </section>
    <!--Events Section Start-->
    <section class="chef-section">
      <div class="row-fluid">
        <div class="span9">
          <div id="myCarousel-02" class="carousel slide chef-slider"> 
            
            <!-- Carousel items -->
            <div class="carousel-inner">
              <div class="active item">
                <div class="chef-box">
                  <div class="frame"> <img src="images/chef-img-1.jpg" alt="img">
                    <div class="social-box">
                      <ul>
                        <li class="facebook"><a href="https://www.facebook.com/pages/JAndrews-Of-Bromley/194978553992377" target="_blank">Facebook</a></li>
                        <li class="gtalk"><a href="#">Gtalk</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="text">
                    <div class="head">
                      <h5>Jonathan Andrews - Director</h5>
                      <strong class="designation">Senior Chef</strong> </div>
                    <div class="expertise-list">
                      <h5>Major expertise of our chef</h5>
                      <ul>
                        <li><i class="icon-arrow-right"></i>Quisque at massa ipsum </li>
                        <li><i class="icon-arrow-right"></i>Maecenas a lorem augue, egestas</li>
                      </ul>
                      <ul>
                        <li><i class="icon-arrow-right"></i>Quisque at massa ipsum </li>
                        <li><i class="icon-arrow-right"></i>Maecenas a lorem augue, egestas</li>
                      </ul>
                    </div>
                    <div class="performance-box">
                      <div class="container-pie">
                        <div class="chart">
                          <div class="percentage" data-percent="50"><span>50</span>%</div>
                          <strong class="title">Baking Expert</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="70"><span>70</span>%</div>
                          <strong class="title">Chinese FOOD</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="80"><span>80</span>%</div>
                          <strong class="title">Baking Expert</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="100"><span>100</span>%</div>
                          <strong class="title">Chinese FOOD</strong> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="chef-box">
                  <div class="frame"> <img src="images/chef-img-1.jpg" alt="img">
                    <div class="social-box">
                      <ul>
                        <li class="facebook"><a href="#">Facebook</a></li>
                        <li class="twitter"><a href="#">Twitter</a></li>
                        <li class="gtalk"><a href="#">Gtalk</a></li>
                        <li class="in"><a href="#">in</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="text">
                    <div class="head">
                      <h5>Jonathan Andrews - Director</h5>
                      <strong class="designation">Senior Chef</strong> </div>
                    <div class="expertise-list">
                      <h5>Major expertise of our chef</h5>
                      <ul>
                        <li><i class="icon-arrow-right"></i>Quisque at massa ipsum </li>
                        <li><i class="icon-arrow-right"></i>Maecenas a lorem augue, egestas</li>
                      </ul>
                      <ul>
                        <li><i class="icon-arrow-right"></i>Quisque at massa ipsum </li>
                        <li><i class="icon-arrow-right"></i>Maecenas a lorem augue, egestas</li>
                      </ul>
                    </div>
                    <div class="performance-box">
                      <div class="container-pie">
                        <div class="chart">
                          <div class="percentage" data-percent="50"><span>50</span>%</div>
                          <strong class="title">Baking Expert</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="70"><span>70</span>%</div>
                          <strong class="title">Chinese FOOD</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="80"><span>80</span>%</div>
                          <strong class="title">Baking Expert</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="100"><span>100</span>%</div>
                          <strong class="title">Chinese FOOD</strong> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="chef-box">
                  <div class="frame"> <img src="images/chef-img-1.jpg" alt="img">
                    <div class="social-box">
                      <ul>
                        <li class="facebook"><a href="#">Facebook</a></li>
                        <li class="twitter"><a href="#">Twitter</a></li>
                        <li class="gtalk"><a href="#">Gtalk</a></li>
                        <li class="in"><a href="#">in</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="text">
                    <div class="head">
                      <h5>Jonathan Andrews - Director</h5>
                      <strong class="designation">Senior Chef</strong> </div>
                    <div class="expertise-list">
                      <h5>Major expertise of our chef</h5>
                      <ul>
                        <li><i class="icon-arrow-right"></i>Quisque at massa ipsum </li>
                        <li><i class="icon-arrow-right"></i>Maecenas a lorem augue, egestas</li>
                      </ul>
                      <ul>
                        <li><i class="icon-arrow-right"></i>Quisque at massa ipsum </li>
                        <li><i class="icon-arrow-right"></i>Maecenas a lorem augue, egestas</li>
                      </ul>
                    </div>
                    <div class="performance-box">
                      <div class="container-pie">
                        <div class="chart">
                          <div class="percentage" data-percent="50"><span>50</span>%</div>
                          <strong class="title">Baking Expert</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="70"><span>70</span>%</div>
                          <strong class="title">Chinese FOOD</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="80"><span>80</span>%</div>
                          <strong class="title">Baking Expert</strong> </div>
                        <div class="chart">
                          <div class="percentage" data-percent="100"><span>100</span>%</div>
                          <strong class="title">Chinese FOOD</strong> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Carousel nav --> 
            <a class="carousel-control left" href="#myCarousel-02" data-slide="prev">&lsaquo;</a> <a class="carousel-control right" href="#myCarousel-02" data-slide="next">&rsaquo;</a> </div>
        </div>
        <div class="span3">
          <div class="video-box">
          <img src="images/full-video-img.jpg" alt="img">
            <div class="head">
              <h6>Video of the Month</h6>
              <strong class="title">RECEPIE OF Chicken Fajita Pizza</strong> </div>
          </div>
        </div>
      </div>
    </section>
    <!--Events Section Start--> 
  </section>
  <!--Content Area End--> 
  
  <?php include('includes/footer.php'); ?>
  
</div>
<!--Wrapper End--> 

<?php echo $add_scripts; ?>

</body>
</html>
