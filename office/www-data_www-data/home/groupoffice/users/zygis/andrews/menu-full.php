<?php

    // meta tags
    $meta_title       = 'menu-full';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    $page             = 'menu-full';


    // styles
    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Oswald|Lato|Roboto:400,500,700|Open+Sans:600,700,300" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <link href="css/datepicker.css" rel="stylesheet">
    ';
    

    // scripts
    $add_scripts = '
    <script type="text/javascript" src="js/jquery.js"></script> 
    <script src="js/bootstrap.min.js" type="text/javascript"></script> 
    <script defer src="js/jquery.flexslider.js"></script> 
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script> 
    <script type="text/javascript" src="js/fullcalendar.js"></script>
    <script type="text/javascript" src="js/google.map.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>

    ';

  ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>

<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">
<meta name="author" content="<?php echo $meta_author; ?>">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php echo $add_styles; ?>
<!-- Html5 Js -->
<script src="js/html5.js" type="text/javascript"></script>
</head>

<body>
<!--Wrapper Start-->
<div id="wrapper"> 
  <!--Header Start-->
  <header id="header">
    <div class="container">
      <div class="row-fluid">
        <div class="span3">
          <div class="logo"><a href="index.php"><img src="images/logo-img.png" alt="img"></a></div>
        </div>
        <div class="span9 margin-non">
          <div class="top-container"></div>
          <!-- Nav Start -->
          <?php include('includes/navigation.php'); ?>
          <!--Nav End--> 
        </div>
      </div>
    </div>
  </header>
  <!--Header End--> 
  
  <!--Banner Start-->
  <section id="banner" class="height">
    <div class="contact-banner"><img src="images/inner-banner-4.jpg" alt="img"></div>
  </section>
  <!--Banner End--> 
  
  <!--Content Area Start-->
  <section id="content"> 
    <!--Blog Page Section Start-->
    <section class="blog-page-section">
      <div class="container">
        <div class="row-fluid">
          <div class="span12">
            <div class="heading">
              <h1>Our Menu</h1>
            </div>
          </div>
        </div>
      </div>
      <!--Blog Post End--> 
      
      <!--Menu Page Start-->

      <section class="menu-page">
        <div class="container">
          <div class="row-fluid">
            <div id="accordion2" class="accordion accordion-area">
              <div class="accordion-group">
                <div class="accordion-heading"> <a href="#collapseOne" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle collapsed">Beef Products</a> </div>
                <div class="accordion-body collapse" id="collapseOne">
                  <div class="accordion-inner">
                    <h4>Joints</h4><hr />
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-1.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Rib Roast </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Ideal for a tasty Sunday roast, can be on the bone or boned & rolled. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-2.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Topside </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Economical and lean. Great option for buffets as it is easy to carve. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-3.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Fillet </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>The most exclusive cut, as it the most leanest and tender. Ideal for those special occasions. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-4.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Rolled Sirloin </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Perfect for roasting and full of flavour. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-5.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Rolled Brisket </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>A lean cut, which is ideal for pot roasting. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-6.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Salt Beef </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>An old favourite, which we cure on our premises. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-group">
                <div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle">Pork Products</a> </div>
                <div class="accordion-body collapse" id="collapseTwo">
                  <div class="accordion-inner">
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-1.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Leg </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-2.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Spare Rib </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-3.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Rack of Pork </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-4.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Loind of Pork </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-5.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Belly </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-6.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Shoulder </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-7.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Rump Steaks </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-8.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Tenderloin </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Nullam id dolor id niltricies vehicula ut id elit. osuer e consectetu. Aliquauam sollicitudin hendrerit. </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-group">
                <div class="accordion-heading"> <a href="#collapseThree" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle">Lamb Products</a> </div>
                <div class="accordion-body collapse" id="collapseThree">
                  <div class="accordion-inner">
                    <h4>Joints</h4><hr />
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-1.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Leg </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Ideal family roast lean & tender, can be boned for easy carving </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-2.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Shoulder </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Tender for slow roasting</p>

                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-3.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Rack Of Lamb </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Ideal family roast lean & tender, can be boned for easy carving </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      
                    
                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-group">
                <div class="accordion-heading"> <a href="#collapsefour" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle">Chicken Products</a> </div>
                <div class="accordion-body collapse" id="collapsefour">
                  <div class="accordion-inner">
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-1.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">FREE RANGE ROASTING CHICKEN </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Approx. 1 to 2 kg </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-2.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Supremes </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Breast part boned with skin </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-3.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Legs </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Part Boned </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-4.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Drumsticks </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Can be coated in one of our marinades </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-5.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Wings </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p>Also can be marinated </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="menu-box">
                          <div class="thumb"> <img src="images/menu-img-6.jpg" alt="img"><strong class="price">&pound;35.00</strong></div>
                          <div class="text"> <strong class="title">Breast Fillets </strong>
                            <ul class="testimonial-star pull-right">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                            <p> </p>
                          </div>
                          <div class="menu-box-bottom">
                            <ul>
                              <li>
                                <div class="face-review">
                                  <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                  </ul>
                                  <strong class="title">100 Reviews</strong> </div>
                              </li>
                              <li> <strong class="name"><br>
                                </strong> </li>
                              <li> <a href="#" class="view">View<br>
                                More</a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                     
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--Menu Page End--> 
    </section>
    <!--Blog Page Section End--> 
  </section>
  <!--Content Area End--> 
  

<?php include('includes/footer.php'); ?>
</div>

<!--Wrapper End-->
<?php echo $add_scripts; ?>
</body>
</html>
