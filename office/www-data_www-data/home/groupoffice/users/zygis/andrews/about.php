<?php

{/* meta tags */

    $meta_title       = 'About';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'About';

    $add_styles = '
    <link href="http://fonts.googleapis.com/css?family=Oswald|Lato|Roboto:400,500,700|Open+Sans:600,700,300" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/colors.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
    <link href="css/datepicker.css" rel="stylesheet">
    ';
    
    $add_scripts = '
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script defer src="js/jquery.flexslider.js"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="js/fullcalendar.js"></script>
    <script type="text/javascript" src="js/google.map.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    ';

  } 

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $meta_title; ?></title>

<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="keywords" content="<?php echo $meta_keywords; ?>">
<meta name="author" content="<?php echo $meta_author; ?>">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php echo $add_styles; ?>

<!-- Html5 Js -->
<script src="js/html5.js" type="text/javascript"></script>
</head>

<body>
<!--Wrapper Start-->
<div id="wrapper"> 
  <!--Header Start-->
  <header id="header">
    <div class="container">
      <div class="row-fluid">
        <div class="span3">
          <div class="logo"><a href="index"><img src="images/logo-img.png" alt="img"></a></div>
        </div>
        <div class="span9 margin-non">
          <div class="top-container"></div>
           <!-- Nav Start -->
          <?php include('includes/navigation.php'); ?>
          <!--Nav End--> 
        </div>
      </div>
    </div>
  </header>
  <!--Header End--> 
  
  <!--Banner Start-->
  <section id="banner" class="height">
    <div class="contact-banner"><img src="images/inner-banner-1.jpg" alt="img"></div>
  </section>
  <!--Banner End--> 
  
  <!--Content Area Start-->
  <section id="content"> 
    <!--Blog Page Section Start-->
    <section class="blog-page-section">
      <div class="container">
        <div class="row-fluid">
          <div class="span12">
            <div class="heading">
              <h1>About Us</h1>
            </div>
          </div>
        </div>
      </div>
      <!--Blog Post End--> 
      
      <!--About Page Start-->
      <div class="about-page">
        <div class="container">
          <div class="row-fluid about-section-1">
            <div class="span5">
              <div id="myCarousel" class="carousel slide">
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                  <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">
                  <div class="active item">
                    <div class="frame"><img src="images/about-img.jpg" alt="img"></div>
                  </div>
                  <div class="item">
                    <div class="frame"><img src="images/about-img.jpg" alt="img"></div>
                  </div>
                  <div class="item">
                    <div class="frame"><img src="images/about-img.jpg" alt="img"></div>
                  </div>
                  <div class="item">
                    <div class="frame"><img src="images/about-img.jpg" alt="img"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="span7">
              <p>Jonathan who has run the family business for the last 20 years which has been growing and growing. This is mainly due to great locally produced meats and a friendly service. We offer an old fashioned service that can never be equalled by the supermarkets, a great range of different cuts of meat, quality and price. </p>
              <p>All our meat is of premium quality and where possible local sourced from Kent and Sussex.</p>
              <p>All our staff are here to offer you advice, and cut your meat to your exact requirements.</p>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row-fluid about-section-2">
            <div class="span3">
              <div class="box-1">
                <div class="detail"> <strong class="round"><img src="images/about-detail-img-1.png" alt="img"></strong> <strong class="title">Fusce vitae luctus dui</strong> </div>
                <div class="text">
                  <p>Cras quis leo eu loresus porta. Ut rutrum consects nibh, pelletesque sed sagittis vta et dolor. Cum s natoque penats et magnis dis parturient. <a href="#" class="plus">[+]</a></p>
                </div>
              </div>
            </div>
            <div class="span3">
              <div class="box-1">
                <div class="detail"> <strong class="round"><img src="images/about-detail-img-2.png" alt="img"></strong> <strong class="title">Fusce vitae luctus dui</strong> </div>
                <div class="text">
                  <p>Cras quis leo eu loresus porta. Ut rutrum consects nibh, pelletesque sed sagittis vta et dolor. Cum s natoque penats et magnis dis parturient. <a href="#" class="plus">[+]</a></p>
                </div>
              </div>
            </div>
            <div class="span3">
              <div class="box-1">
                <div class="detail"> <strong class="round"><img src="images/about-detail-img-3.png" alt="img"></strong> <strong class="title">Fusce vitae luctus dui</strong> </div>
                <div class="text">
                  <p>Cras quis leo eu loresus porta. Ut rutrum consects nibh, pelletesque sed sagittis vta et dolor. Cum s natoque penats et magnis dis parturient. <a href="#" class="plus">[+]</a></p>
                </div>
              </div>
            </div>
            <div class="span3">
              <div class="box-1">
                <div class="detail"> <strong class="round"><img src="images/about-detail-img-4.png" alt="img"></strong> <strong class="title">Fusce vitae luctus dui</strong> </div>
                <div class="text">
                  <p>Cras quis leo eu loresus porta. Ut rutrum consects nibh, pelletesque sed sagittis vta et dolor. Cum s natoque penats et magnis dis parturient. <a href="#" class="plus">[+]</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <section class="testimonial-section">
          <div class="container">
            <div class="testimonial-box">
              <h2>Testimonial Customer</h2>
              <div id="myCarousel-05" class="carousel slide"> 
                <!-- Carousel items -->
                <div class="carousel-inner">
                  <div class="active item">
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-1.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-2.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-3.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-4.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-1.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-2.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-3.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-4.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="row-fluid">
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-1.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-2.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-3.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                      <div class="span3">
                        <div class="testimonial-inner-box">
                          <div class="frame"><img src="images/testimonial-img-4.jpg" alt="img"></div>
                          <div class="text">
                            <h3>Mandy joe</h3>
                            <strong class="title">Senior Food Inspector</strong>
                            <ul class="testimonial-star">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                          <blockquote class="testimonial-blockquote">
                            <p> Sed odio neqmpor sit amet vestibulum sed, pulvinar in dio. M eget lacus convallis mi bibendum eestt in massa. Etiam neque h, porta eu dignissim mattis, tristique non lorenterdum et malesuada fames ac ante ipsrimis in faucibusm mattis.</p>
                            <a href="#" class="comment">3 Comments</a> </blockquote>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Carousel nav --> 
                <a class="carousel-control left" href="#myCarousel-05" data-slide="prev">&lsaquo;</a> <a class="carousel-control right" href="#myCarousel-05" data-slide="next">&rsaquo;</a> </div>
            </div>
          </div>
        </section>
        <section>
          <div class="container">
            <div class="meet-team">
              <h3>Meet our Team</h3>
              <div class="row-fluid">
                <div class="span3">
                  <div class="frame"><img src="images/team-img-1.jpg" alt="img"></div>
                  <div class="text">
                    <p>Lorem ipsum dolor sit a consctetur adipiscing elit. Curabitur fringillrmentumam, sed blandit leo convallis ac. Vestibulum ae ipsum primis in faucibus orci luctus et ultes posuere cubilia Curae.</p>
                  </div>
                  <ul class="about-social">
                    <li class="icon-1"><a href="#"></a></li>
                    <li class="icon-2"><a href="#"></a></li>
                    <li class="icon-3"><a href="#"></a></li>
                    <li class="icon-4"><a href="#"></a></li>
                  </ul>
                </div>
                <div class="span3">
                  <div class="frame"><img src="images/team-img-2.jpg" alt="img"></div>
                  <div class="text">
                    <p>Lorem ipsum dolor sit a consctetur adipiscing elit. Curabitur fringillrmentumam, sed blandit leo convallis ac. Vestibulum ae ipsum primis in faucibus orci luctus et ultes posuere cubilia Curae.</p>
                  </div>
                  <ul class="about-social">
                    <li class="icon-1"><a href="#"></a></li>
                    <li class="icon-2"><a href="#"></a></li>
                    <li class="icon-3"><a href="#"></a></li>
                    <li class="icon-4"><a href="#"></a></li>
                  </ul>
                </div>
                <div class="span3">
                  <div class="frame"><img src="images/team-img-3.jpg" alt="img"></div>
                  <div class="text">
                    <p>Lorem ipsum dolor sit a consctetur adipiscing elit. Curabitur fringillrmentumam, sed blandit leo convallis ac. Vestibulum ae ipsum primis in faucibus orci luctus et ultes posuere cubilia Curae.</p>
                  </div>
                  <ul class="about-social">
                    <li class="icon-1"><a href="#"></a></li>
                    <li class="icon-2"><a href="#"></a></li>
                    <li class="icon-3"><a href="#"></a></li>
                    <li class="icon-4"><a href="#"></a></li>
                  </ul>
                </div>
                <div class="span3">
                  <div class="frame"><img src="images/team-img-4.jpg" alt="img"></div>
                  <div class="text">
                    <p>Lorem ipsum dolor sit a consctetur adipiscing elit. Curabitur fringillrmentumam, sed blandit leo convallis ac. Vestibulum ae ipsum primis in faucibus orci luctus et ultes posuere cubilia Curae.</p>
                  </div>
                  <ul class="about-social">
                    <li class="icon-1"><a href="#"></a></li>
                    <li class="icon-2"><a href="#"></a></li>
                    <li class="icon-3"><a href="#"></a></li>
                    <li class="icon-4"><a href="#"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <!--About Page End--> 
    </section>
    <!--Blog Page Section End--> 
  </section>
  <!--Content Area End--> 
  
  <?php include('includes/footer.php'); ?>

  </div>
  </footer>
  <!--Footer Area End--> 
  
</div>

<?php echo $add_scripts; ?>
<!--Wrapper End-->
</body>
</html>
