/**
 * Don't copy the next lines into a translation
 */

Ext.namespace('GO.calllog');

GO.calllog.lang={};
/**
 * Copy everything below for translations
 */
GO.calllog.lang.calllog='Список звонков';
GO.calllog.lang.showOnSummary='Отображать на странице Резюме';
GO.calllog.lang.call='Звонок';
GO.calllog.lang.saveAsContact='Сохранить в Контактах';