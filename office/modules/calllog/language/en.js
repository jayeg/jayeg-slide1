/**
 * Don't copy the next lines into a translation
 */

Ext.namespace('GO.calllog');

GO.calllog.lang={};
/**
 * Copy everything below for translations
 */
GO.calllog.lang.calllog='Call Log';
GO.calllog.lang.showOnSummary='Show on Summary page';
GO.calllog.lang.call='Call';
GO.calllog.lang.saveAsContact='Save as Contact';