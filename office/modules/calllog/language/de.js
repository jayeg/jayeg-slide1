/**
 * Don't copy the next lines into a translation
 */

Ext.namespace('GO.calllog');

GO.calllog.lang={};
/**
 * Copy everything below for translations
 */
GO.calllog.lang.calllog='Anruferliste';
GO.calllog.lang.showOnSummary='Zeigen auf Übersichtsseite';
GO.calllog.lang.call='Telefonate';
GO.calllog.lang.saveAsContact='Speichern als ein Kontact';