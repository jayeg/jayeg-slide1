/**
 * Copy everything below for translations
 */
GO.calllog.lang.calllog='Journal d\'appel';
GO.calllog.lang.showOnSummary='Afficher sur la page sommaire';
GO.calllog.lang.call='Appel';
GO.calllog.lang.saveAsContact='Sauvegarder comme contact';