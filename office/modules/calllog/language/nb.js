/**
 * Don't copy the next lines into a translation
 */

/**
 * Copy everything below for translations
 */
GO.calllog.lang.calllog='Ringelogg';
GO.calllog.lang.showOnSummary='Vis på oversiktssiden';
GO.calllog.lang.call='Samtale';
GO.calllog.lang.saveAsContact='Lagre som kontaktperson';