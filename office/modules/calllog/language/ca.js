/**
 * Copy everything below for translations
 */
GO.calllog.lang.calllog='Log de trucades';
GO.calllog.lang.showOnSummary='Mostrar en la pàgina de sumari';
GO.calllog.lang.call='Trucada';
GO.calllog.lang.saveAsContact='Desar com a contacte';