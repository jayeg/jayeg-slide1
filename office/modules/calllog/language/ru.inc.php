<?php
/**
 * Russian translation
 * By Valery Yanchenko (utf-8 encoding)
 * vajanchenko@hotmail.com
 * 10 December 2008
*/
//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('calllog'));

$lang['calllog']['name']='Список звонков';
$lang['calllog']['description']='Простой модуль учета звонков.';
