<?php


$l["links"]='Links';
$l["linkDescription"]="Link description";
$l["linkDescriptions"]="Link descriptions";
$l["defaultLinkFolderText"]='Folders listed here will be created automatically when an item is created. Enter folder paths each on a separate line like: <br /><br />Folder/Sub<br />Folder 2/Sub 2<br /><br />';
$l["linkFolders"]='Default link folders';
$l["link"]='Link';
$l['name']='Link descriptions';
$l['description']='Manage default link descriptions';
$l['link_description']='Link description';
$l['link_descriptions']='Link descriptions';
