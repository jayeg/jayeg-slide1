GO.links.lang.links='Links';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Descrição de link";
GO.links.lang.linkDescriptions="Descrições de links";

GO.links.lang.defaultLinkFolderText='As pastas listadas aqui serão criadas automaticamente quanto um item for criado. Digite cada pasta em uma linha separada, por exemplo: <br /><br />Pasta/Subpasta<br />Pasta 2/Subpasta 2<br /><br />'
GO.links.lang.linkFolders='Pastas padrão para links';