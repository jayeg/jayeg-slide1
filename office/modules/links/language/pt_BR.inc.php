<?php
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Descrições de links';
$lang['links']['description']='Gerenciar descrições padrão de links';
$lang['links']['link_description']='Descrição de link';
$lang['links']['link_descriptions']='Descrições de links';
