<?php

$l["links"]='Beschreibungen zu Verknüpfungen';
$l["linkDescription"]='Beschreibung';
$l["linkDescriptions"]='Beschreibungen';
$l["defaultLinkFolderText"]='Hier eingetragene Ordner werden automatisch angelegt, wenn ein neuer Eintrag verknüpft wird. Geben Sie jeden Ordnerpfad in einer eigenen Zeile wie folgt an: <br /><br />Ordner/Unterordner<br />Ordner 2/Unterordner 2<br /><br />';
$l["linkFolders"]='Standard Ordnerverknüpfungen';
$l["link"]='Verknüpfung';

$l['name']='Beschreibung von Verknuepfungen';
$l['description']='Modul zum Festlegen von Standardbeschreibungen fuer Verknuepfungen';
$l['link_description']='Beschreibung';
$l['link_descriptions']='Beschreibungen';