/** 
 * @copyright Copyright Boso d.o.o.
 * @author Mihovil Stanić <mihovil.stanic@boso.hr>
 */
 
GO.links.lang.links='Linkovi';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Opis linka";
GO.links.lang.linkDescriptions="Opisi linkova";

GO.links.lang.defaultLinkFolderText='Direktoriji navedeni ovdje će biti kreirani automatski kada stavka bude kreirana. Unesite putanje direktorija svaku u poseban red, ovako: <br /><br />Direktorij/Poddirektorij<br />Direktorij 2/Poddirektorij 2<br /><br />'
GO.links.lang.linkFolders='Zadani link za direktorije';

GO.links.lang.link='Link';