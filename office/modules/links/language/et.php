<?php


$l['name']='Lingi kirjeldus';
$l['description']='Halda vaikimis lingi kirjeldusi';
$l['link_description']='Lingi kirjeldus';
$l['link_descriptions']='Linkide kirjeldused';

$l["links"]='Links';
$l["linkDescription"]="Lingi kirjeldus";
$l["linkDescriptions"]="Linkide kirjledused";
$l["defaultLinkFolderText"]='Siin nimetatud kaustad tehakse automaatselt kohe peale kirje lisamist. Sisesta kaustade teekond erinevatele ridadele. Nt:: <br /><br />Kaust/Alamkaust<br />Kaust 2/Alamkaust 2<br /><br />';
$l["linkFolders"]='Vaikimisi lingi kaustad';
$l["link"]='Link';