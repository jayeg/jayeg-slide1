<?php

$l["links"]='Links';
$l["linkDescription"]="Descrição de link";
$l["linkDescriptions"]="Descrições de links";
$l["defaultLinkFolderText"]='As pastas listadas aqui serão criadas automaticamente quanto um item for criado. Digite cada pasta em uma linha separada, por exemplo: <br /><br />Pasta/Subpasta<br />Pasta 2/Subpasta 2<br /><br />';
$l["linkFolders"]='Pastas padrão para links';
$l['name']='Descrições de links';
$l['description']='Gerenciar descrições padrão de links';
$l['link_description']='Descrição de link';
$l['link_descriptions']='Descrições de links';
