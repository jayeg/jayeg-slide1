<?php


$l["links"]='Liens';
$l["linkDescription"]="Description des liens";
$l["linkDescriptions"]="Descriptions des liens";
$l["defaultLinkFolderText"]='Les dossiers listés ici seront automatiquement créés lorsqu\'un un élément est créé. Saisissez chaque chemin sur une ligne : <br /><br />Dossier/Sous-dossier<br />Dossier 2/Sous-dossier 2<br /><br />';
$l["linkFolders"]='Dossiers de lien par défaut';
$l["link"]='Lien';
$l['name']='Descriptions des liens';
$l['description']='Gérer les descriptions des liens';
$l['link_description']='Description des liens';
$l['link_descriptions']='Descriptions des liens';
