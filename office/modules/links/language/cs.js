

GO.links.lang.links='Odkazy';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Popisek odkazu";
GO.links.lang.linkDescriptions="Popisky odkazů";

GO.links.lang.defaultLinkFolderText='Složky, které jsou zde uvedeny, budou automaticky vytvořeny při vytvoření položky. Cestu ke složce zadejte na každý samostatný řádek: <br /><br />Složka/Podsložka<br />Složka 2/Podsložka 2<br /><br />'
GO.links.lang.linkFolders='Výchozí složky odkazů';

GO.links.lang.link='Odkaz';
