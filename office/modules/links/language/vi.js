/** 
 * Copyright Intermesh
 * 
 * This file is part of Group-Office. You should have received a copy of the
 * Group-Office license along with Group-Office. See the file /LICENSE.TXT
 * 
 * If you have questions write an e-mail to info@intermesh.nl
 * 
 * @copyright Copyright Intermesh
 * @version $Id: en.js 7708 2011-07-06 14:13:04Z wilmar1980 $
 * @author Dat Pham <datpx@fab.vn> +84907382345
 */
 

GO.links.lang.links='Liên kết';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Mô tả liên kết";
GO.links.lang.linkDescriptions="Mô tả liên kết";

GO.links.lang.defaultLinkFolderText='Thư mục dưới đây được tạo tự động khi mỗi thành phần được tạo. Mỗi đường dẫn dưới đây: <br /><br />Folder/Sub<br />Folder 2/Sub 2<br /><br />'
GO.links.lang.linkFolders='Thư mục liên kết ngầm định';

GO.links.lang.link='Liên kết';