<?php
/* Translator for the Greek Language: Konstantinos Georgakopoulos (kgeorga@uom.gr)*/
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Περιγραφές συνδέσμων';
$lang['links']['description']='Διαχείριση προεπιλεγμένων περιγραφών συνδέσμων';
$lang['links']['link_description']='Περιγραφή συνδέσμου';
$lang['links']['link_descriptions']='Περιγραφές συνδέσμου';
