Ext.namespace('GO.links');
GO.links.lang={};
GO.links.lang.links='Links';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Link description";
GO.links.lang.linkDescriptions="Link descriptions";

GO.links.lang.defaultLinkFolderText='Folders listed here will be created automatically when an item is created. Enter folder paths each on a separate line like: <br /><br />Folder/Sub<br />Folder 2/Sub 2<br /><br />'
GO.links.lang.linkFolders='Default link folders';

GO.links.lang.link='Link';