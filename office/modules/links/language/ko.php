<?php


$l["linkDescription"]="링크 설명";
$l["linkDescriptions"]="링크 설명";
$l["defaultLinkFolderText"]='Folders listed here will be created automatically when an item is created. Enter folder paths each on a separate line like: <br /><br />Folder/Sub<br />Folder 2/Sub 2<br /><br />';
$l["linkFolders"]='기본 링크 폴더';
$l["link"]='링크';
$l['name']='링크 설명';
$l['description']='Manage default link descriptions';
$l['link_description']='링크 설명';
$l['link_descriptions']='링크 설명';
