<?php

﻿;
$l['name']='Opisi linkova';
$l['description']='Upravljaj zadanim opisima linkova';
$l['link_description']='Opis linka';
$l['link_descriptions']='Opisi linkova';
﻿;
$l["links"]='Linkovi';
$l["linkDescription"]="Opis linka";
$l["linkDescriptions"]="Opisi linkova";
$l["defaultLinkFolderText"]='Direktoriji navedeni ovdje će biti kreirani automatski kada stavka bude kreirana. Unesite putanje direktorija svaku u poseban red, ovako: <br /><br />Direktorij/Poddirektorij<br />Direktorij 2/Poddirektorij 2<br /><br />';
$l["linkFolders"]='Zadani link za direktorije';
$l["link"]='Link';