

GO.links.lang.links='Links';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Lingi kirjeldus";
GO.links.lang.linkDescriptions="Linkide kirjledused";

GO.links.lang.defaultLinkFolderText='Siin nimetatud kaustad tehakse automaatselt kohe peale kirje lisamist. Sisesta kaustade teekond erinevatele ridadele. Nt:: <br /><br />Kaust/Alamkaust<br />Kaust 2/Alamkaust 2<br /><br />'
GO.links.lang.linkFolders='Vaikimisi lingi kaustad';
GO.links.lang.link='Link';