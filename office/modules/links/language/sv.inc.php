<?php
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Länkbeskrivningar';
$lang['links']['description']='Hantera standardlänkbeskrivningar.';
$lang['links']['link_description']='Länkbeskrivning';
$lang['links']['link_descriptions']='Länkbeskrivningar';
