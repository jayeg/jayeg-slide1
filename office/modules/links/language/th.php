<?php


$l['name']='เชื่อมโยงคำอธิบาย';//Link descriptions;
$l['description']='การจัดการค่าเริ่มต้นการเชื่อมโยงคำอธิบาย';//Manage default link descriptions;
$l['link_description']='เชื่อมโยงคำอธิบาย';//Link description;
$l['link_descriptions']='เชื่อมโยงคำอธิบาย';//Link descriptions;
$l["links"]='เชื่อมโยง';//Links;
$l["linkDescription"]="เชื่อมโยงคำอธิบาย";//Link description;
$l["linkDescriptions"]="เชื่อมโยงคำอธิบาย";//Link descriptions;
$l["defaultLinkFolderText"]='รายการโฟลเดอร์นี้จะถูกสร้างขึ้นโดยอัตโนมัติเมื่อสร้างรายการ ตัวอย่างการระบุตำแหน่งของโฟลเดอร์ : <br /><br />โฟลเดอร์/โฟลเดอร์ย่อย<br />โฟลเดอร์ 2/โฟลเดอร์ย่อย 2<br /><br />';
$l["linkFolders"]='ค่าเริ่มต้นการเชื่อมโยงโฟลเดอร์';
