<?php


$l['name']='Ссылки';
$l['description']='Ссылки';
$l['link_description']='Описание ссылки';
$l['link_descriptions']='Описания ссылок';

$l["links"]='Ссылки';
$l["linkDescription"]="Описание ссылки";
$l["linkDescriptions"]="Описания ссылок";
$l["defaultLinkFolderText"]='Папки, перечисленные здесь, будут созданы автоматически. Вводите каждую папку в отдельную строчку, например: <br/> <br/> Folder/Sub <br/> Folder 2/Sub 2 <br/> <br/>';
$l["linkFolders"]='Папка по умолчанию для ссылок';
$l["link"]='Ссылка';