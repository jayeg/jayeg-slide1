<?php


$l["links"]='Bağlantılar';
$l["linkDescription"]="Bağlantı açıklaması";
$l["linkDescriptions"]="Bağlantı açıklamaları";

$l['name']='Bağlantı açıklamaları';
$l['description']='Varsayılan bağlantı açıklamalarını yönet';
$l['link_description']='Bağlantı açıklaması';
$l['link_descriptions']='Bağlantı açıklamaları';
