GO.links.lang.links='Verknüpfungen';
GO.links.lang.linkDescription='Beschreibung';
GO.links.lang.linkDescriptions='Bschreibungen';
GO.links.lang.defaultLinkFolderText='Hier eingetragene Ordner werden automatisch angelegt, wenn ein Eintrag erstellt wird. Geben Sie jeden Ordnerpfad in einer eigenen Zeile wie folgt an: <br /><br />Ordner/Unterordner<br />Ordner 2/Unterordner 2<br /><br />';
GO.links.lang.linkFolders='Standard Verknüpfungsordner';
