GO.links.lang.links='Beschreibungen zu Verknüpfungen';
GO.links.lang.linkDescription='Beschreibung';
GO.links.lang.linkDescriptions='Beschreibungen';
GO.links.lang.defaultLinkFolderText='Hier eingetragene Ordner werden automatisch angelegt, wenn ein neuer Eintrag verknüpft wird. Geben Sie jeden Ordnerpfad in einer eigenen Zeile wie folgt an: <br /><br />Ordner/Unterordner<br />Ordner 2/Unterordner 2<br /><br />'
GO.links.lang.linkFolders='Standard Ordnerverknüpfungen';
GO.links.lang.link='Verknüpfung';
