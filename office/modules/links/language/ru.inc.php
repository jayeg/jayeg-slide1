<?php
//Uncomment this line in new translations!
/**
 * Russian translation
 * By Valery Yanchenko (utf-8 encoding)
 * vajanchenko@hotmail.com
 * 10 December 2008
*/
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));

$lang['links']['name']='Ссылки';
$lang['links']['description']='Ссылки';
$lang['links']['link_description']='Описание ссылки';
$lang['links']['link_descriptions']='Описания ссылок';
