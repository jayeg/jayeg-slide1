/**
 * Don't copy the next lines into a translation
 *
 * Polish Translation v1.1
 * Author : Paweł Dmitruk pawel.dmitruk@gmail.com
 * Date : September, 05 2010
 * Polish Translation v1.2
 * Author : rajmund
 * Date : January, 26 2011
 */
GO.links.lang.link='Odnośnik';
GO.links.lang.links='Odnośniki';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Opis odnośnika";
GO.links.lang.linkDescriptions="Opisy odnośników";

GO.links.lang.defaultLinkFolderText='Foldery wymienione tutaj będą automatycznie tworzone podczas tworzenia elementu. Wprowadź ścieżki folderu, każdy w oddzielnej lini, np.: <br /><br />Folder/Sub<br />Folder 2/Sub 2<br /><br />'
GO.links.lang.linkFolders='Domyślne folery odnośników';
