<?php
//Polish Translation v1.1
//Author : Paweł Dmitruk pawel.dmitruk@gmail.com
//Date : September, 05 2010
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Opisy odnośników';
$lang['links']['description']='Zarządzaj domyślnymi opisami odnośników';
$lang['links']['link_description']='Opis odnośnika';
$lang['links']['link_descriptions']='Opisy odnośników';
