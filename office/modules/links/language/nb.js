GO.links.lang.links='Lenker';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Lenkebeskrivelse";
GO.links.lang.linkDescriptions="Lenkebeskrivelser";

GO.links.lang.defaultLinkFolderText='Mappene angitt her vil bli opprettet automatisk når et nytt element opprettes. Legg inn sti/navn på separate linjer, for eksempel: <br /><br />Mappe/Under<br />Mappe 2/Under 2<br /><br />'
GO.links.lang.linkFolders='Standard lenkemapper';
GO.links.lang.link='Lenke';
