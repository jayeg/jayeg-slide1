<?php


$l['name']='Lenkebeskrivelser';
$l['description']='Behandle standard lenkebeskrivelser';
$l['link_description']='Lenkebeskrivelse';
$l['link_descriptions']='Lenkebeskrivelser';
$l["links"]='Lenker';
$l["linkDescription"]="Lenkebeskrivelse";
$l["linkDescriptions"]="Lenkebeskrivelser";
$l["defaultLinkFolderText"]='Mappene angitt her vil bli opprettet automatisk når et nytt element opprettes. Legg inn sti/navn på separate linjer, for eksempel: <br /><br />Mappe/Under<br />Mappe 2/Under 2<br /><br />';
$l["linkFolders"]='Standard lenkemapper';
$l["link"]='Lenke';
