<?php
	/** 
		* @copyright Copyright Boso d.o.o.
		* @author Mihovil Stanić <mihovil.stanic@boso.hr>
	*/
 
//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('links'));

$lang['links']['name']='Opisi linkova';
$lang['links']['description']='Upravljaj zadanim opisima linkova';
$lang['links']['link_description']='Opis linka';
$lang['links']['link_descriptions']='Opisi linkova';
