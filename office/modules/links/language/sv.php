<?php


$l['name']='Länkbeskrivningar';
$l['description']='Hantera standardlänkbeskrivningar.';
$l['link_description']='Länkbeskrivning';
$l['link_descriptions']='Länkbeskrivningar';
$l["links"]='Länkar';
$l["linkDescription"]="Länkbeskrivning";
$l["linkDescriptions"]="Länkbeskrivningar";
$l["defaultLinkFolderText"]='Mappar som listas här kommer skapas automatiskt när ett object skapas. Ange sökvägar, en på varje rad. Ex: <br /><br />Mapp/Undermapp<br />Mapp 2/Undermapp 2<br /><br />';
$l["linkFolders"]='Standard länkmappar';