GO.links.lang.links='เชื่อมโยง';//Links

/* table: go_link_descriptions */
GO.links.lang.linkDescription="เชื่อมโยงคำอธิบาย";//Link description
GO.links.lang.linkDescriptions="เชื่อมโยงคำอธิบาย";//Link descriptions
GO.links.lang.defaultLinkFolderText='รายการโฟลเดอร์นี้จะถูกสร้างขึ้นโดยอัตโนมัติเมื่อสร้างรายการ ตัวอย่างการระบุตำแหน่งของโฟลเดอร์ : <br /><br />โฟลเดอร์/โฟลเดอร์ย่อย<br />โฟลเดอร์ 2/โฟลเดอร์ย่อย 2<br /><br />'
GO.links.lang.linkFolders='ค่าเริ่มต้นการเชื่อมโยงโฟลเดอร์';
