Ext.namespace('GO.links');
GO.links.lang={};
GO.links.lang.links='Ссылки';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Описание ссылки";
GO.links.lang.linkDescriptions="Описания ссылок";

GO.links.lang.defaultLinkFolderText='Папки, перечисленные здесь, будут созданы автоматически. Вводите каждую папку в отдельную строчку, например: <br/> <br/> Folder/Sub <br/> Folder 2/Sub 2 <br/> <br/>'
GO.links.lang.linkFolders='Папка по умолчанию для ссылок';

GO.links.lang.link='Ссылка';