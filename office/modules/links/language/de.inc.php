<?php
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Beschreibung von Verknuepfungen';
$lang['links']['description']='Modul zum Festlegen von Standardbeschreibungen fuer Verknuepfungen';
$lang['links']['link_description']='Beschreibung';
$lang['links']['link_descriptions']='Beschreibungen';