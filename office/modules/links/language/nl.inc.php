<?php
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Beschrijvingen van koppelingen';
$lang['links']['description']='Beheer standaardbeschrijvingen van koppelingen';
$lang['links']['link_description']='Beschrijvingen van koppelingen';
$lang['links']['link_descriptions']='Beschrijvingen van koppelingen';
