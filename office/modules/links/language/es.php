<?php

$l["links"]='Verknüpfungen';
$l["linkDescription"]='Beschreibung';
$l["linkDescriptions"]='Bschreibungen';
$l["defaultLinkFolderText"]='Hier eingetragene Ordner werden automatisch angelegt, wenn ein Eintrag erstellt wird. Geben Sie jeden Ordnerpfad in einer eigenen Zeile wie folgt an: <br /><br />Ordner/Unterordner<br />Ordner 2/Unterordner 2<br /><br />';
$l["linkFolders"]='Standard Verknüpfungsordner';

$l['name']='Descripciones de enlace';
$l['description']='Administrar descripción de enlaces por defecto';
$l['link_description']='Descripción de enlace';
$l['link_descriptions']='Descripciones de enlace';
