<?php
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Descripciones de enlace';
$lang['links']['description']='Administrar descripción de enlaces por defecto';
$lang['links']['link_description']='Descripción de enlace';
$lang['links']['link_descriptions']='Descripciones de enlace';
