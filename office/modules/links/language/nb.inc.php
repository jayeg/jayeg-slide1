<?php
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Lenkebeskrivelser';
$lang['links']['description']='Behandle standard lenkebeskrivelser';
$lang['links']['link_description']='Lenkebeskrivelse';
$lang['links']['link_descriptions']='Lenkebeskrivelser';
