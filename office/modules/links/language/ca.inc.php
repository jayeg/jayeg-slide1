<?php
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('links'));
$lang['links']['name']='Descripcions d\'enllaç';
$lang['links']['description']='Administrar descripció d\'enllaços per defecte';
$lang['links']['link_description']='Descripció d\'enllaç';
$lang['links']['link_descriptions']='Descripcions d\'enllaç';
