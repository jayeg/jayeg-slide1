<?php


$l['name']='Descripcions d\'enllaç';
$l['description']='Administrar descripció d\'enllaços per defecte';
$l['link_description']='Descripció d\'enllaç';
$l['link_descriptions']='Descripcions d\'enllaç';
$l["link"]='Enllaç';
$l["links"]='Enllaços';
$l["linkDescription"]='Descripció d\'enllaç';
$l["linkDescriptions"]='Descripcions d\'enllaç';
$l["defaultLinkFolderText"]='Les carpetes llistades aquí es crearan automàticament quan es creï un ítem. Introduïu la ruta de cada carpeta en una nova línia com: <br /><br />Carpeta/Subcarpeta<br />Carpeta 2/Subcarpeta 2<br /><br />';
$l["linkFolders"]='Carpetes d\'enllaços per defecte';
