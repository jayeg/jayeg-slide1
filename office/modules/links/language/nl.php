<?php


$l['name']='Beschrijvingen van koppelingen';
$l['description']='Beheer standaardbeschrijvingen van koppelingen';
$l['link_description']='Beschrijvingen van koppelingen';
$l['link_descriptions']='Beschrijvingen van koppelingen';
$l["links"]='Beschrijvingen van koppelingen';
$l["linkDescription"]="Beschrijvingen van koppelingen";
$l["linkDescriptions"]="Beschrijvingen van koppelingen";
$l["defaultLinkFolderText"]='Mappen die hier ingevoerd worden, worden automatisch aangemaakt voor koppelingen van een nieuw item. Voer padden van mappen op een aparte regel in zoals: <br /><br />Map/Sub<br />Map 2/Sub 2<br /><br />';
$l["linkFolders"]='Standaard koppelmappen';
$l["link"]='Koppeling';