<?php


$l["link"]='Odnośnik';
$l["links"]='Odnośniki';
$l["linkDescription"]="Opis odnośnika";
$l["linkDescriptions"]="Opisy odnośników";
$l["defaultLinkFolderText"]='Foldery wymienione tutaj będą automatycznie tworzone podczas tworzenia elementu. Wprowadź ścieżki folderu, każdy w oddzielnej lini, np.: <br /><br />Folder/Sub<br />Folder 2/Sub 2<br /><br />';
$l["linkFolders"]='Domyślne folery odnośników';

$l['name']='Opisy odnośników';
$l['description']='Zarządzaj domyślnymi opisami odnośników';
$l['link_description']='Opis odnośnika';
$l['link_descriptions']='Opisy odnośników';
