GO.links.lang.links='Länkar';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Länkbeskrivning";
GO.links.lang.linkDescriptions="Länkbeskrivningar";

GO.links.lang.defaultLinkFolderText='Mappar som listas här kommer skapas automatiskt när ett object skapas. Ange sökvägar, en på varje rad. Ex: <br /><br />Mapp/Undermapp<br />Mapp 2/Undermapp 2<br /><br />';
GO.links.lang.linkFolders='Standard länkmappar';