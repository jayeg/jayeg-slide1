/* table: go_link_descriptions */
GO.links.lang.linkDescription="링크 설명";
GO.links.lang.linkDescriptions="링크 설명";

GO.links.lang.defaultLinkFolderText='Folders listed here will be created automatically when an item is created. Enter folder paths each on a separate line like: <br /><br />Folder/Sub<br />Folder 2/Sub 2<br /><br />'
GO.links.lang.linkFolders='기본 링크 폴더';

GO.links.lang.link='링크';