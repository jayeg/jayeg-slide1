GO.links.lang.link='Enllaç';
GO.links.lang.links='Enllaços';
GO.links.lang.linkDescription='Descripció d\'enllaç';
GO.links.lang.linkDescriptions='Descripcions d\'enllaç';
GO.links.lang.defaultLinkFolderText='Les carpetes llistades aquí es crearan automàticament quan es creï un ítem. Introduïu la ruta de cada carpeta en una nova línia com: <br /><br />Carpeta/Subcarpeta<br />Carpeta 2/Subcarpeta 2<br /><br />';
GO.links.lang.linkFolders='Carpetes d\'enllaços per defecte';
