<?php


$l['name']='Popisky odkazů';
$l['description']='Upráva výchozích popisků odkazů';
$l['link_description']='Popisek odkazu';
$l['link_descriptions']='Popisky odkazů';

$l["links"]='Odkazy';
$l["linkDescription"]="Popisek odkazu";
$l["linkDescriptions"]="Popisky odkazů";
$l["defaultLinkFolderText"]='Složky, které jsou zde uvedeny, budou automaticky vytvořeny při vytvoření položky. Cestu ke složce zadejte na každý samostatný řádek: <br /><br />Složka/Podsložka<br />Složka 2/Podsložka 2<br /><br />';
$l["linkFolders"]='Výchozí složky odkazů';
$l["link"]='Odkaz';
