/////////////////////////////////////////////////////////////////////////////////
//
// Copyright Intermesh
// 
// This file is part of Group-Office. You should have received a copy of the
// Group-Office license along with Group-Office. See the file /LICENSE.TXT
// 
// If you have questions write an e-mail to info@intermesh.nl
//
// @copyright Copyright Intermesh
// @version $Id: fr.js 8287 2011-10-12 12:03:09Z mschering $
// @author Merijn Schering <mschering@intermesh.nl>
//
// French Translation
// Version : 3.7.29 
// Author : Lionel JULLIEN
// Date : September, 27 2011
//
/////////////////////////////////////////////////////////////////////////////////

GO.links.lang.links='Liens';
/* table: go_link_descriptions */
GO.links.lang.linkDescription="Description des liens";
GO.links.lang.linkDescriptions="Descriptions des liens";
GO.links.lang.defaultLinkFolderText='Les dossiers listés ici seront automatiquement créés lorsqu\'un un élément est créé. Saisissez chaque chemin sur une ligne : <br /><br />Dossier/Sous-dossier<br />Dossier 2/Sous-dossier 2<br /><br />'
GO.links.lang.linkFolders='Dossiers de lien par défaut';
GO.links.lang.link='Lien';
