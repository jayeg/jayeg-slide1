GO.links.lang.links='Beschrijvingen van koppelingen';

/* table: go_link_descriptions */
GO.links.lang.linkDescription="Beschrijvingen van koppelingen";
GO.links.lang.linkDescriptions="Beschrijvingen van koppelingen";

GO.links.lang.defaultLinkFolderText='Mappen die hier ingevoerd worden, worden automatisch aangemaakt voor koppelingen van een nieuw item. Voer padden van mappen op een aparte regel in zoals: <br /><br />Map/Sub<br />Map 2/Sub 2<br /><br />'
GO.links.lang.linkFolders='Standaard koppelmappen';

GO.links.lang.link='Koppeling';