<?php


$l['name']='Email portlet';
$l['description']='Modul som viser e-postmapper på oversiktssiden.';
$l["showOnSummary"]='Vis på oversiktssiden';
$l["email"]='E-post';
$l["noEmailFolders"]='Ingen mapper er lagt til.';
$l["folderAdded"]='Mappen er lagt til på oversiktssiden.';
