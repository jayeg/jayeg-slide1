<?php
/**
 * Russian translation
 * By Valery Yanchenko (utf-8 encoding)
 * vajanchenko@hotmail.com
 * 10 December 2008
*/
////Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('emailportlet'));

$lang['emailportlet']['name']='Email Portlet';
$lang['emailportlet']['description']='Плугин, который отображает папку email сообщений на странице Резюме.';