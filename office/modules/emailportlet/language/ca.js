/**
 * Copy everything below for translations
 */
GO.emailportlet.lang.showOnSummary='Mostrar dins la pàgina de sumari';
GO.emailportlet.lang.email='Email';
GO.emailportlet.lang.noEmailFolders='No s\'ha afegit cap carpeta.';
GO.emailportlet.lang.folderAdded='La carpeta s\'ha afegit a la pàgina de sumari.';
