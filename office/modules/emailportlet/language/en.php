<?php


$l["showOnSummary"]='Show on Summary page';
$l["email"]='Email';
$l["noEmailFolders"]='No folders have been added.';
$l["folderAdded"]='Folder is added to the Summary page.';

$l['name']='Email Portlet';
$l['description']='Plugin to show email folders on the summary page.';