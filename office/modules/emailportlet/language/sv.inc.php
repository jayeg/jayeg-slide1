<?php
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('emailportlet'));

$lang['emailportlet']['name']='E-post Portlet';
$lang['emailportlet']['description']='Tillägg för att visa e-postmappar på sammanfattningssidan.';