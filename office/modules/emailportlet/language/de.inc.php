<?php
//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('emailportlet'));

$lang['emailportlet']['name']='E-Mail Portlet';
$lang['emailportlet']['description']='Plugin für E-Mail-Ordner auf der Übersichtsseite.';
