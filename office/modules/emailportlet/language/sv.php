<?php


$l['name']='E-post Portlet';
$l['description']='Tillägg för att visa e-postmappar på sammanfattningssidan.';$l["showOnSummary"]='Visa på sammanfattningssidan';
$l["email"]='E-post';
$l["noEmailFolders"]='Inga mappar har lagts till.';
$l["folderAdded"]='Mapp är tilllagt på sammanfattningssidan.';
