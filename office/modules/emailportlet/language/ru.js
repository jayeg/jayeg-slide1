/**
 * Russian translation
 * By Valery Yanchenko (utf-8 encoding)
 * vajanchenko@hotmail.com
 * 10 December 2008
*/
/**
 * Don't copy the next lines into a translation
 */
GO.emailportlet.lang.showOnSummary='Отображать на старнице Резюме';
GO.emailportlet.lang.email='Почта';
GO.emailportlet.lang.noEmailFolders='Папки не добавлены.';
GO.emailportlet.lang.folderAdded='Папка добавлена на страницу Резюме.';
