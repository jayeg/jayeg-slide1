<?php


$l["showOnSummary"]='요약 페이지에 표시';
$l["email"]='메일';
$l["noEmailFolders"]='No folders have been added.';
$l["folderAdded"]='Folder is added to the Summary page.';

$l['name']='Email Portlet';
$l['description']='Plugin to show email folders on the summary page.';