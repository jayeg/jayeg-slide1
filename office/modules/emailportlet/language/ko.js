/**
 * Don't copy the next lines into a translation
 */
/**
 * Copy everything below for translations
 */
GO.emailportlet.lang.showOnSummary='요약 페이지에 표시';
GO.emailportlet.lang.email='메일';
GO.emailportlet.lang.noEmailFolders='No folders have been added.';
GO.emailportlet.lang.folderAdded='Folder is added to the Summary page.';
