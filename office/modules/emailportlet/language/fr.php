<?php


$l["showOnSummary"]='Afficher sur la page de sommaire';
$l["email"]='Email';
$l["noEmailFolders"]='Aucun répertoire n\'a encore été ajouté.';
$l["folderAdded"]='Le répertoire a été ajouté a la page sommaire.';

$l['name']='Greffon email';
$l['description']='Greffon pour afficher des répertoires de votre boite email sur la page de sommaire.';
