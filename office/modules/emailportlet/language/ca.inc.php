<?php
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('emailportlet'));

$lang['emailportlet']['name']='Portlet d\'Email';
$lang['emailportlet']['description']='Plugin per mostrar les carpetes d\'e-mail dins la pàgina de sumari.';