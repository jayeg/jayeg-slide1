<?php
//Uncomment this line in new translations!
require($GLOBALS['GO_LANGUAGE']->get_fallback_language_file('emailportlet'));

$lang['emailportlet']['name']='Email portlet';
$lang['emailportlet']['description']='Modul som viser e-postmapper på oversiktssiden.';