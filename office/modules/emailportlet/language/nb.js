/**
 * Don't copy the next lines into a translation
 */
 
/**
 * Copy everything below for translations
 */
GO.emailportlet.lang.showOnSummary='Vis på oversiktssiden';
GO.emailportlet.lang.email='E-post';
GO.emailportlet.lang.noEmailFolders='Ingen mapper er lagt til.';
GO.emailportlet.lang.folderAdded='Mappen er lagt til på oversiktssiden.';
