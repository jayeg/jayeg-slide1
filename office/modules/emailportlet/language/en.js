/**
 * Don't copy the next lines into a translation
 */
 
Ext.namespace('GO.emailportlet');

GO.emailportlet.lang={};
/**
 * Copy everything below for translations
 */
GO.emailportlet.lang.showOnSummary='Show on Summary page';
GO.emailportlet.lang.email='Email';
GO.emailportlet.lang.noEmailFolders='No folders have been added.';
GO.emailportlet.lang.folderAdded='Folder is added to the Summary page.';
