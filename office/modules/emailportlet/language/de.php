<?php


$l["showOnSummary"]='Auf Übersichtsseite anzeigen';
$l["email"]='E-Mail';
$l["noEmailFolders"]='Kein Ordner hinzugefügt.';
$l["folderAdded"]='Folder ist auf der Übersichtsseite hinzugefügt.';

$l['name']='E-Mail Portlet';
$l['description']='Plugin für E-Mail-Ordner auf der Übersichtsseite.';