/////////////////////////////////////////////////////////////////////////////////
//
// Copyright Intermesh
// 
// This file is part of Group-Office. You should have received a copy of the
// Group-Office license along with Group-Office. See the file /LICENSE.TXT
// 
// If you have questions write an e-mail to info@intermesh.nl
//
// @copyright Copyright Intermesh
// @version $Id: fr.js 5934 2010-10-20 13:59:49Z mschering $
// @author Merijn Schering <mschering@intermesh.nl>
//
// French Translation
// Version : 3.7.29 
// Author : Lionel JULLIEN
// Date : September, 27 2011
//
/////////////////////////////////////////////////////////////////////////////////

GO.emailportlet.lang.showOnSummary='Afficher sur la page de sommaire';
GO.emailportlet.lang.email='Email';
GO.emailportlet.lang.noEmailFolders='Aucun répertoire n\'a encore été ajouté.';
GO.emailportlet.lang.folderAdded='Le répertoire a été ajouté a la page sommaire.';
