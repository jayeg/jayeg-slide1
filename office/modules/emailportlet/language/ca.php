<?php


$l['name']='Portlet d\'Email';
$l['description']='Plugin per mostrar les carpetes d\'e-mail dins la pàgina de sumari.';
$l["showOnSummary"]='Mostrar dins la pàgina de sumari';
$l["email"]='Email';
$l["noEmailFolders"]='No s\'ha afegit cap carpeta.';
$l["folderAdded"]='La carpeta s\'ha afegit a la pàgina de sumari.';
