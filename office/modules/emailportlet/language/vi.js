/** 
 * Copyright Intermesh
 * 
 * This file is part of Group-Office. You should have received a copy of the
 * Group-Office license along with Group-Office. See the file /LICENSE.TXT
 * 
 * If you have questions write an e-mail to info@intermesh.nl
 * 
 * @copyright Copyright Intermesh
 * @version $Id: en.js 7708 2011-07-06 14:13:04Z wilmar1980 $
 * @author Dat Pham <datpx@fab.vn> +84907382345
 */
 
GO.emailportlet.lang.showOnSummary='Hiển thị ở trang tổng hợp';
GO.emailportlet.lang.email='Email';
GO.emailportlet.lang.noEmailFolders='Không có thư mục được thêm.';
GO.emailportlet.lang.folderAdded='Thư mục được thêm vào trang tổng hợp.';
