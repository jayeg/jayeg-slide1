<?php
/**
 * Copyright Intermesh
 *
 * This file is part of Group-Office. You should have received a copy of the
 * Group-Office license along with Group-Office. See the file /LICENSE.TXT
 *
 * If you have questions write an e-mail to info@intermesh.nl
 *
 * @version $Id: Phpbb3Module.php 15624 2013-09-06 11:43:57Z mschering $
 * @copyright Copyright Intermesh
 * @author Wesley Smits <wsmits@intermesh.nl>
 */

/**
 * 
 * The Dokuwiki module maintenance class
 * 
 */
class GO_Phpbb3_Phpbb3Module extends GO_Base_Module{
	
	public function autoInstall() {
		return false;
	}
}