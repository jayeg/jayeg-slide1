<?php
require($GO_LANGUAGE->get_fallback_language_file('mediawiki'));
$lang['mediawiki']['name']='Mediawiki';
$lang['mediawiki']['description']='Ce module permet d\'intégrer Mediawiki avec Group-Office et de connecter automatiquement les utilisateurs Group-Office. Référez vous au fichier modules/mediawiki/INSTALL.TXT pour plus d\'informations.';
?>