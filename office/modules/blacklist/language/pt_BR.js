/**
 * Don't copy the next line into a translation
 */
GO.blacklist.lang.blacklist="Bloqueio de IP\'s";
/**
 * Copy everything below for translations
 */


/* table: bl_ips */
GO.blacklist.lang.ip="Endereço IP";
GO.blacklist.lang.ips="Endereços IP";
GO.blacklist.lang.count="Tentativas";
