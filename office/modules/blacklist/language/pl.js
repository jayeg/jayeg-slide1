/**
 * Don't copy the next line into a translation
 * Polish Translation v1.1
 * Author : Paweł Dmitruk pawel.dmitruk@gmail.com
 * Date : September, 05 2010
 */
GO.blacklist.lang.blacklist='Czarna lista IP';
/**
 * Copy everything below for translations
 */


/* table: bl_ips */
GO.blacklist.lang.ip="Adres IP";
GO.blacklist.lang.ips="Adresy IP";
GO.blacklist.lang.count="Ilość";
