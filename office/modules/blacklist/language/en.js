/**
 * Don't copy the next line into a translation
 */
Ext.namespace('GO.blacklist');
GO.blacklist.lang={};
GO.blacklist.lang.blacklist='IP blacklist';
/**
 * Copy everything below for translations
 */


/* table: bl_ips */
GO.blacklist.lang.ip="IP address";
GO.blacklist.lang.ips="IP addresses";
GO.blacklist.lang.count="Count";
