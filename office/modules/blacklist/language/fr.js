/** 
 * Copyright Intermesh
 * 
 * This file is part of Group-Office. You should have received a copy of the
 * Group-Office license along with Group-Office. See the file /LICENSE.TXT
 * 
 * If you have questions write an e-mail to info@intermesh.nl
 * 
 * @copyright Copyright Intermesh
 * @version $Id: fr.js 5934 2010-10-20 13:59:49Z mschering $
 * @author Merijn Schering <mschering@intermesh.nl>
 *
 * French Translation
 * Author : Lionel JULLIEN
 * Date : September, 27 2010
 */

/* table: bl_ips */
GO.blacklist.lang.ip="Addresse IP";
GO.blacklist.lang.ips="Addresses IP";
GO.blacklist.lang.count="Décompte";
