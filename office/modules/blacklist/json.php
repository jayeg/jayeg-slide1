<?php
/** 
 * Copyright Intermesh
 * 
 * This file is part of Group-Office. You should have received a copy of the
 * Group-Office license along with Group-Office. See the file /LICENSE.TXT
 * 
 * If you have questions write an e-mail to info@intermesh.nl
 * 
 * @version $Id: json.tpl 2030 2008-06-04 10:12:13Z mschering $
 * @copyright Copyright Intermesh
 * @author Merijn Schering <mschering@intermesh.nl>
 */
require_once('../../Group-Office.php');
$GO_SECURITY->json_authenticate('blacklist');
require_once ($GO_MODULES->modules['blacklist']['class_path'].'blacklist.class.inc.php');
$blacklist = new blacklist();
$task=isset($_REQUEST['task']) ? $_REQUEST['task'] : '';
try{
	switch($task)
	{

		case 'ips':
			if(isset($_POST['delete_keys']))
			{
				try{
					$response['deleteSuccess']=true;
					$delete_ips = json_decode($_POST['delete_keys']);
					foreach($delete_ips as $ip)
					{
						$blacklist->delete_ip($ip);
					}
				}catch(Exception $e)
				{
					$response['deleteSuccess']=false;
					$response['deleteFeedback']=$e->getMessage();
				}
			}
			$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : 'mtime';
			$dir = isset($_REQUEST['dir']) ? $_REQUEST['dir'] : 'DESC';
			$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : '0';
			$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : '0';
			$query = isset($_REQUEST['query']) ? '%'.$_REQUEST['query'].'%' : '';
			$response['total'] = $blacklist->get_ips( $query, $sort, $dir, $start, $limit);
			$response['results']=array();
			while($ip = $blacklist->next_record())
			{
				$ip['mtime']=Date::get_timestamp($ip['mtime']);
				$response['results'][] = $ip;
			}
			break;
/* {TASKSWITCH} */
	}
}catch(Exception $e)
{
	$response['feedback']=$e->getMessage();
	$response['success']=false;
}
echo json_encode($response);
