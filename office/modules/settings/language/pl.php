<?php


$l['name']='Ustawienia';
$l['description']='Moduł pozwale na zarządzanie różnymi ustawieniami Group-Office i jego modułów';
$l['mainTitle'] = 'Ustawienia';
$l['loginScreenText']='Tekst na stronie logowania';
$l['text']='Tekst';
$l['title']='Tytuł';
$l['loginTextEnabled']='Włącz tekst na stronie logowania';
$l['notRenamedNoUser']='Nie można zmienić, brak przsypisanych użytkowników po zmianie';
$l['allRenamingSuccess']='Domyślne wzory dla wszystkich użytkowników zostały zmienione';
$l['GO_Addressbook_Model_Addressbook']='Książka adresowa';
$l['GO_Tasks_Model_Tasklist']='Zadania';
$l['GO_Calendar_Model_Calendar']='Kalendarz';