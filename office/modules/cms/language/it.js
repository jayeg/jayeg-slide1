GO.cms.lang.cms='Sito web';

/* table: cms_sites */
GO.cms.lang.site="Sito";
GO.cms.lang.sites="Siti";
GO.cms.lang.allowProperties="Consenti proprietà";
GO.cms.lang.domain="Dominio";
GO.cms.lang.webmaster="Webmaster";
GO.cms.lang.language="Lingua";

/* table: cms_folders */
GO.cms.lang.folder="Cartella";
GO.cms.lang.folders="Cartelle";
GO.cms.lang.disabled="Nascondi questa cartella";

/* table: cms_files */
GO.cms.lang.file="File";
GO.cms.lang.files="File";


GO.cms.lang.autoMeta="Genera automaticamente i meta data";
GO.cms.lang.title="Titolo";
GO.cms.lang.keywords="Parole chiave";

/* table: cms_templates */
GO.cms.lang.template="Modello";

GO.cms.lang.selectFolder="Prima seleziona una cartella nella vista ad albero";
GO.cms.lang.selectFolderAdd="Seleziona una cartella nel menù della vista ad albero prima di aggiungere un file";
GO.cms.lang.authentication='Usa l\'autenticazione di Group-Office';
GO.cms.lang.applyRecursive='Applicare ricorsivamente le impostazioni sopra?';
GO.cms.lang.defaultTemplateOptions='Impostazioni modello predefinite';
GO.cms.lang.newPage='Nuova pagina';
GO.cms.lang.newFolder='Nuova cartella';
GO.cms.lang.view='Vedi';
GO.cms.lang.folderProperties='Proprietà cartella';

GO.cms.lang.siteId='ID sito';

GO.cms.lang.saveChanges='Il documento corrente ha subito delle modifiche. Vuoi salvare le modifiche?';