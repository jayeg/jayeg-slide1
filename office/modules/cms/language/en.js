/**
 * Don't copy the next line into a translation
 */
Ext.namespace('GO.cms');
GO.cms.lang={};
GO.cms.lang.cms='Websites';
/**
 * Copy everything below for translations
 */


/* table: cms_sites */
GO.cms.lang.site="Site";
GO.cms.lang.sites="Sites";
GO.cms.lang.allowProperties="Allow properties";
GO.cms.lang.domain="Domain";
GO.cms.lang.webmaster="Webmaster";
GO.cms.lang.language="Language";

/* table: cms_folders */
GO.cms.lang.folder="Folder";
GO.cms.lang.folders="Folders";
GO.cms.lang.disabled="Hide this folder";

/* table: cms_files */
GO.cms.lang.file="File";
GO.cms.lang.files="Files";


GO.cms.lang.autoMeta="Generate meta data automatically";
GO.cms.lang.title="Title";
GO.cms.lang.keywords="Keywords";

/* table: cms_templates */
GO.cms.lang.template="Template";

GO.cms.lang.selectFolder="Please select a folder in the treeview first";
GO.cms.lang.selectFolderAdd="Please select a folder in the treemenu first before you add a file";
GO.cms.lang.authentication='Use Group-Office authentication';
GO.cms.lang.applyRecursive='Apply above setting recursively?';
GO.cms.lang.defaultTemplateOptions='Default template options';
GO.cms.lang.newPage='New page';
GO.cms.lang.newFolder='New folder';
GO.cms.lang.view='View';
GO.cms.lang.folderProperties='Folder properties';

GO.cms.lang.siteId='Site ID';

GO.cms.lang.saveChanges='The current document has been changed. Do you wish to save the changes?';
GO.cms.lang.insertTemplate='Insert template';
GO.cms.lang.selectTemplate='Select template';

GO.cms.lang.selectPage='Select page';

GO.cms.lang.showUntil='Show page until';
GO.cms.lang.alwaysShow='Leave empty to show forever';
