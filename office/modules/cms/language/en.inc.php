<?php
//Uncomment this line in new translations!
//require($GO_LANGUAGE->get_fallback_language_file('cms'));
$lang['cms']['name']='Websites';
$lang['cms']['description']='Put a description here';
$lang['cms']['site']='Site';
$lang['cms']['sites']='Sites';
$lang['cms']['folder']='Folder';
$lang['cms']['folders']='Folders';
$lang['cms']['file']='File';
$lang['cms']['files']='Files';
$lang['cms']['template']='Template';
$lang['cms']['templates']='Templates';
$lang['cms']['template_item']='Template item';
$lang['cms']['template_items']='Template items';

$lang['cms']['back']='Back';
$lang['cms']['message']='Message';
$lang['cms']['reset']='Reset';
$lang['cms']['subject']='Subject';
$lang['cms']['continue']='Continue';
$lang['cms']['cancel']='Cancel';

$lang['cms']['error_email']='You entered an invalid e-mail address';
$lang['cms']['sendmail_error']='Could not send the e-mail message';
$lang['cms']['sendmail_success']='<h1>Thank you</h1>The message has been sent successfully';
$lang['cms']['please_select']='Please select';
$lang['cms']['cms_permissions']='Checking CMS folder permissions';
$lang['cms']['adding_share']='Adding share for site: ';
$lang['cms']['done_with_cms']='Done with CMS';

$lang['cms']['path_error']='Error: could not resolve path: ';
$lang['cms']['include_file_error']='Error: include_file requires path or file_id parameter';
$lang['cms']['cant_delete_site_treeview']='You can\'t delete the entire site from the treeview. Delete it from the sites administration dialog if you have permissions.';
$lang['cms']['cant_move_into_itself']='Can not move into itself';
$lang['cms']['done_with_cms']='Done with CMS';

$lang['cms']['antispam_fail']='The antispam answer was incorrect. Please try again';