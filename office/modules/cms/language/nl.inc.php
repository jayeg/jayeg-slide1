<?php
//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('cms'));
$lang['cms']['name']='Websites';
$lang['cms']['description']='Zet hier een beschrijving neer';
$lang['cms']['site']='Webpagina';
$lang['cms']['sites']='Webpagina\'s';
$lang['cms']['folder']='Map';
$lang['cms']['folders']='Mappen';
$lang['cms']['file']='Bestand';
$lang['cms']['files']='Bestanden';
$lang['cms']['template']='Sjabloon';
$lang['cms']['templates']='Sjablonen';
$lang['cms']['template_item']='Sjabloon item';
$lang['cms']['template_items']='Sjabloon items';

$lang['cms']['back']='Terug';
$lang['cms']['message']='Bericht';
$lang['cms']['reset']='Reset';
$lang['cms']['subject']='Onderwerp';
$lang['cms']['continue']='Doorgaan';
$lang['cms']['cancel']='Annuleren';

$lang['cms']['error_email']='U heeft een ongeldig e-mail adres ingevoerd';
$lang['cms']['sendmail_error']='Kan het e-mail bericht niet verzenden';
$lang['cms']['sendmail_success']='<h1>Dank u</h1>Het bericht is succesvol verzonden';
$lang['cms']['please_select']='Selecteer a.u.b.';

$lang['cms']['cms_permissions']='CMS maptoegang controleren';
$lang['cms']['adding_share']='Gedeelde map maken voor site: ';
$lang['cms']['done_with_cms']='Klaar met CMS';
$lang['cms']['path_error']='Error: kan het pad niet vinden: ';
$lang['cms']['include_file_error']='Error: include_file heeft een pad of file_id parameter nodig';
$lang['cms']['cant_delete_site_treeview']='U kunt de hele site niet verwijderen vanaf de treeview. Verwijder hem vanuit het administrator venster van de site als u daar toegang tot heeft.';
$lang['cms']['cant_move_into_itself']='Kan niet verplaatsen naar zichzelf';

$lang['cms']['antispam_fail']='Het antispam antwoord was onjuist. Probeer het opnieuw a.u.b.';