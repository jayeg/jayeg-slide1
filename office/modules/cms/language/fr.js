/** 
 * Copyright Intermesh
 * 
 * This file is part of Group-Office. You should have received a copy of the
 * Group-Office license along with Group-Office. See the file /LICENSE.TXT
 * 
 * If you have questions write an e-mail to info@intermesh.nl
 * 
 * @copyright Copyright Intermesh
 * @version $Id: en.js 2895 2008-08-29 15:21:02Z mschering $
 * @author Merijn Schering <mschering@intermesh.nl>
 *
 * French Translation v1.0
 * Author : Lionel JULLIEN
 * Date : September, 08 2008
 */



GO.cms.lang.cms='Sites Web';

/* table: cms_sites */
GO.cms.lang.site="Site";
GO.cms.lang.sites="Sites";
GO.cms.lang.allowProperties="Autoriser les propriétés";
GO.cms.lang.domain="Domaine";
GO.cms.lang.webmaster="Webmaster";
GO.cms.lang.language="Langage";

/* table: cms_folders */
GO.cms.lang.folder="Dossier";
GO.cms.lang.folders="Dossiers";
GO.cms.lang.disabled="Cacher ce dossier";

/* table: cms_files */
GO.cms.lang.file="Fichier";
GO.cms.lang.files="fichiers";

GO.cms.lang.autoMeta="Générer les Meta Data automatiquement";
GO.cms.lang.title="Titre";
GO.cms.lang.keywords="Mots-clés";

/* table: cms_templates */
GO.cms.lang.template="Modèle";

GO.cms.lang.selectFolder="Veuillez d'abord sélectionner un dossier dans le treeview";
GO.cms.lang.selectFolderAdd="Veuillez d'abord sélectionner un dossier dans le treeview avant d\'ajouter un fichier";
GO.cms.lang.authentication='Utiliser l\'authentification Group-Office';
GO.cms.lang.applyRecursive='Appliquer les paramètres récursivement ?';
GO.cms.lang.defaultTemplateOptions='Options de modèle par défaut';
GO.cms.lang.newPage='Nouvelle page';
GO.cms.lang.newFolder='Nouveau dossier';
GO.cms.lang.view='Voir';
GO.cms.lang.folderProperties='Propriétés du dossier';

// 3.0-14
GO.cms.lang.siteId='ID du site';
GO.cms.lang.saveChanges='Le document courant a été modifié. Voulez-vous sauvegarder les changements ?';
