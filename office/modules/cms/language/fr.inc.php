<?php
//French Translation v1.0
//Author : Lionel JULLIEN
//Date : September, 08 2008

//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('cms'));

$lang['cms']['name']='Sites Web';
$lang['cms']['description']='Module de création de sites web intégrés';
$lang['cms']['site']='Site';
$lang['cms']['sites']='Sites';
$lang['cms']['folder']='Dossier';
$lang['cms']['folders']='Dossiers';
$lang['cms']['file']='Fichier';
$lang['cms']['files']='Fichiers';
$lang['cms']['template']='Modèle';
$lang['cms']['templates']='Modèles';
$lang['cms']['template_item']='Elément de modèle';
$lang['cms']['template_items']='Eléments de modèle';
$lang['cms']['comment']='Commentaire';
$lang['cms']['comments']='Commentaires';

$lang['cms']['back']='Retour';
$lang['cms']['message']='Message';
$lang['cms']['reset']='Réinitialiser';
$lang['cms']['subject']='Sujet';
$lang['cms']['continue']='Continuer';
$lang['cms']['cancel']='Annuler';

$lang['cms']['error_email']='Vous avez saisi une adresse e-mail invalide';
$lang['cms']['sendmail_error']='Impossible d\'envoyer le message e-mail';
$lang['cms']['sendmail_success']='<h1>Merci</h1>Le message a bien été envoyé';
$lang['cms']['please_select']='Veuillez sélectionner';
$lang['cms']['cms_permissions']='Vérification des permission du dossier CMS';
$lang['cms']['adding_share']='Ajout d\'un partage pour ce site : ';
$lang['cms']['done_with_cms']='Terminé avec le CMS';

$lang['cms']['path_error']='ERREUR : impossible de résoudre le chemin : ';
$lang['cms']['include_file_error']='ERREUR : include_file nécessite un chemin ou le paramètre file_id';
$lang['cms']['cant_delete_site_treeview']='Vous ne pouvez pas supprimer la totalité du site depuis le treeview. Supprimez le depuis la partie Administration si vous en avez les droits.';
$lang['cms']['cant_move_into_itself']='Impossible de déplacer';
$lang['cms']['done_with_cms']='Terminé avec le CMS';

// 3.0-14
$lang['cms']['antispam_fail']='La répose de l\'anti-spam est incorrecte. Veuillez réessayer';