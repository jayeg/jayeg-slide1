GO.cms.lang.cms='Websites';

/* table: cms_sites */
GO.cms.lang.site="Site";
GO.cms.lang.sites="Sites";
GO.cms.lang.allowProperties="Sta wijzigen van eigenschappen toe";
GO.cms.lang.domain="Domein";
GO.cms.lang.webmaster="Webmaster";
GO.cms.lang.language="Taal";

/* table: cms_folders */
GO.cms.lang.folder="Map";
GO.cms.lang.folders="Mappen";
GO.cms.lang.disabled="Verberg deze map";

/* table: cms_files */
GO.cms.lang.file="Bestand";
GO.cms.lang.files="Bestanden";


GO.cms.lang.autoMeta="Genereer meta data automatisch";
GO.cms.lang.title="Titel";
GO.cms.lang.keywords="Sleutelwoorden";

/* table: cms_templates */
GO.cms.lang.template="Sjabloon";

GO.cms.lang.selectFolder="Selecteer eerst een map links in het scherm a.u.b.";
GO.cms.lang.selectFolderAdd="Selecteer eerst een map links in het scherm voordat u een bestand toevoegd.";
GO.cms.lang.authentication='Gebruik Group-Office authenticatie';
GO.cms.lang.applyRecursive='Bovenstaande wijzigingen recursief toepassen?';
GO.cms.lang.defaultTemplateOptions='Standaard sjabloon opties';
GO.cms.lang.newPage='Nieuw pagina';
GO.cms.lang.newFolder='Nieuwe map';
GO.cms.lang.view='Weergeven';
GO.cms.lang.folderProperties='Map eigenschappen';
GO.cms.lang.siteId='Site ID';

GO.cms.lang.saveChanges='Het huidige document is gewijzigd. Wilt u de wijzigingen opslaan?';
GO.cms.lang.insertTemplate='Sjabloon invoegen';
GO.cms.lang.selectTemplate='Selecteer sjabloon';

GO.cms.lang.selectPage='Selecteer pagina';

GO.cms.lang.showUntil='Toon pagina tot';
GO.cms.lang.alwaysShow='Laat leeg om altijd te tonen';