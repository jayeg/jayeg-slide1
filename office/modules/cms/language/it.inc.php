<?php
//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('cms'));
$lang['cms']['name']='Sito web';
$lang['cms']['description']='Inserisci qui una descrizione';
$lang['cms']['site']='Sito';
$lang['cms']['sites']='Siti';
$lang['cms']['folder']='Cartella';
$lang['cms']['folders']='Cartelle';
$lang['cms']['file']='File';
$lang['cms']['files']='File';
$lang['cms']['template']='Modello';
$lang['cms']['templates']='Modelli';
$lang['cms']['template_item']='Modello articolo';
$lang['cms']['template_items']='Modello articoli';
$lang['cms']['comment']='Nota';
$lang['cms']['comments']='Note';

$lang['cms']['back']='Indietro';
$lang['cms']['message']='Messaggio';
$lang['cms']['reset']='Reset';
$lang['cms']['subject']='Oggetto';
$lang['cms']['continue']='Continua';
$lang['cms']['cancel']='Cancella';

$lang['cms']['error_email']='Hai inserito un indirizzo e-mail non valido';
$lang['cms']['sendmail_error']='Impossibile inviare l\'email';
$lang['cms']['sendmail_success']='<h1>Grazie</h1>Il messaggio è stato inviato correttamente';
$lang['cms']['please_select']='Seleziona';
$lang['cms']['cms_permissions']='Verifica la cartella delle autorizzazioni del CMS';
$lang['cms']['adding_share']='Aggiungi una condivisione: ';
$lang['cms']['done_with_cms']='Realizzato con un CMS';

$lang['cms']['path_error']='Errore: potrebbe non risolvere il percorso: ';
$lang['cms']['include_file_error']='Errore: include_file richiede un percorso o un parametro file_id';
$lang['cms']['cant_delete_site_treeview']='Non è possibile eliminare l\'intero sito dalla vista ad albero. Cancellalo dalla finestra di amministrazione del sito se hai i permessi.';
$lang['cms']['cant_move_into_itself']='Non puoi spostarlo dentro a se stesso ';
$lang['cms']['done_with_cms']='Realizzato con un CMS';

$lang['cms']['antispam_fail']='La risposta antispam non è corretta. Prova ancora';