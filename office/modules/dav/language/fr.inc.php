<?php
require($GO_LANGUAGE->get_fallback_language_file('dav'));
$lang['dav']['name']='Serveur WebDAV';
$lang['dav']['description']='Accédez à vos fichiers avec le serveur WebDAV. Ce service DAV fonctionne grace à la librairie SabreDAV.';
?>