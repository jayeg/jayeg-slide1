<?php
//Uncomment this line in new translations!
//require($GO_LANGUAGE->get_fallback_language_file('wordpress'));

$lang['wordpress']['name']='Wordpress CMS';
$lang['wordpress']['description']='A bridge to the Wordpress CMS to manage a website. The GroupOffice plugin needs to be installed at the Wordpress system.';
?>