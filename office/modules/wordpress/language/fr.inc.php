<?php
//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('wordpress'));

$lang['wordpress']['name']='Wordpress CMS';
$lang['wordpress']['description']='Ce module permet de faire un pont avec Wordpress pour gérer un site web. Le greffon GroupOffice doit préalablement être installé dans le système Wordpress.';
?>