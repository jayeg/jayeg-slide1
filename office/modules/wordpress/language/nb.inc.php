<?php
//Uncomment this line in new translations!
require($GO_LANGUAGE->get_fallback_language_file('wordpress'));

$lang['wordpress']['name']='Wordpress CMS';
$lang['wordpress']['description']='En bro til Wordpress CMS for å vedlikeholde et nettsted. Du må også installere GroupOffice modulen i Wordpress systemet.';
?>