/**
 * Don't copy the next lines into a translation
 */
Ext.namespace('GO.wordpress');

GO.wordpress.lang={};

/**
 * Copy everything below for translations
 */

GO.wordpress.lang.wordpress='Website';
GO.wordpress.lang.wordpressAdmin='Admin panel';
GO.wordpress.lang.publishToWebsite='Publish to website';
