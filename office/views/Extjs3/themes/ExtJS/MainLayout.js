
/*
 * Remove module icons
 */
GO.mainLayout.onReady(function(){
	for(var module in GO.moduleManager.panelConfigs){
		delete GO.moduleManager.panelConfigs[module].iconCls;
	}
});