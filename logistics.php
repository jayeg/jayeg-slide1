<?php    
$meta_title       = 'Logistics';    
$meta_description = 'meta_description';    
$meta_keywords    = 'meta_keywords';    
$meta_author      = '';        
$page             = 'Logistics';	
	
$og_meta_image = 'http://www.jayegroup.com/images/slide-show/logistics-home-page.jpg';	
$og_url = 'http://www.jayegroup.com/logistics.php';	
$og_meta_title = 'We have a large and varied fleet of vehicles suitable for almost any type or size of delivery.';	
$og_meta_description = 'A modern, cost effective solutions, dedicated to combining logistics and technology in order to streamline your operation and fulfil all your logistical requirements.';

$site_name = 'Jayegroup';	
		    
$add_scripts = '    <script src="js/jquery-1.10.2.min.js"></script>    <script src="js/bootstrap.min.js"></script>    <script src="js/bootstrap-hover-dropdown.min.js"></script>    <script src="js/jquery.bxslider.min.js"></script>    <script src="js/jquery.nivo.slider.pack.js"></script>    <script src="owl-carousel/owl.carousel.js"></script>    <script src="js/jquery.fancybox.js"></script>    <script src="js/jquery.easing.1.3.js"></script>    <script src="js/progressbar-plugin.js"></script>    <script src="js/jquery.fittext.js"></script>    <script src="js/customTwo.js"></script>    <script src="js/color-picker.js"></script>    <script src="js/modernizr.js"></script>    <script src="js/jquery.retina.js"></script>    ';	
require_once 'includes/site_settings/main_head.php';	  
?>

<body><?php include 'includes/sections/core/header.php';?>    <div class="BreadCrumbWrapper WhiteSkin">        <div class="container">            <div class="row">                       <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>           </div>        </div>    </div>    <header class="elements logistics">        <div class="container">            <div class="row">                <div class="col-sm-12 slogan">                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>                </div>            </div>        </div>    </header>		    <div class="container ">        <div class='row '>            <div class='col-md-6 service-text'>				<p>        Jayegroup provides a wide variety of transport and storage solutions to meet your needs and take cost out of your suply chain. We are proud to offer transport services on a one-way basis where your items complement our existing volume, which helps to reduce yout transport costs. As our national distribution grows, so will our price advantage, service capability and your gain as our client.You can protect your business against the price rises for the duration of the contract by committing to pre-agreed cost base.				<a href="booking-form.php?action=logistics_quote">Get a quote </a>		</p>			</div>            <div class='col-sm-5 col-md-offset-1'>                <div class="controls clearfix">                    <div class="controls pull-right clearfix">                        <a href="">                            <span id="arrowNext">                            </span>                        </a>                        <a href="">                            <span id="arrowPrev">                            </span>                        </a>                    </div>                </div>                <article class="blog-post clearfix">                    <figure id="folioGallery">                        <img class="img-responsive" src="images/logistics/logistics-2.jpg" title="Image title" alt="alternative information">                        <img class="img-responsive" src="images/logistics/logistics-small-2.jpg"  alt="alternative information">                    </figure>                </article>			</div>					</div>    </div><br/><br/><br/><br/><br/>	<?php include 'includes/core/services.php';?>	<?php include 'includes/core/footer_in_full.php';?></body></html>