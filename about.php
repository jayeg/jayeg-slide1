<?php
{/* meta tags */

    $meta_title       = 'jayegroup';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'About Us';

		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/gallery/delivery/5.jpg';
	$og_url = 'http://www.jayegroup.com/about.php';
	$og_meta_title = 'Jayegroup About';
	$og_meta_description = 'Jayegroup has a vision of brighter future for people who need a complete removal- delivery-disposal solution offered all in one place. We are determined to earn the lifetime loyalty of our customers by frequently delivering consistent transport services which offer great value. ';
	$site_name = 'Jayegroup';		
	
    $add_styles = '
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css" id="ColorStyle">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    ';
    
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>
            </div>
        </div>
    </div>
    <header class="elements about-us ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		
    <div class="container">
	
        <div class="row ">
            <div id="OurCrow ">
                <div class="col-sm-6 ">

 <br>					
<p><strong>Our vision:</strong>
<p>
 Jayegroup has a vision of brighter future for people who need a complete removal- delivery-disposal solution offered all in one place. We are determined to earn the lifetime loyalty of our customers by frequently delivering consistent transport services which offer great value.
</p>
<br/>
<p><strong>Our aims:</strong>
<ul>
	<li>to provide a high quality, professional service to our customers,</li>
	<li>to advice our clients on the most efficient proccess of our services,</li>
	<li>to grow on strength of our excellent reputation through referrals from our satisfied clients.</li>

</ul>

</p>
				
                </div>
            <div class='col-sm-5 col-md-offset-1'>
                <div class="controls clearfix">
                    <div class="controls pull-right clearfix">
                        <a href="">
                            <span id="arrowNext">
                            </span>
                        </a>
                        <a href="">
                            <span id="arrowPrev">
                            </span>
                        </a>
                    </div>
                </div>
                <article class="blog-post WhiteSkin clearfix">
                    <figure id="folioGallery">
                        <img class="img-responsive" src="images/about-us/about-us-1.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/about-us/about-us-2.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/about-us/about-us--3.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/about-us/about-us-4.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/about-us/about-us-5.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/about-us/about-us-6.jpg" title="Image title" alt="alternative information">
                    </figure>
                </article>
			</div>	
            </div>
        </div>
    </div>
   <!-- <div class="container">
        <div class="SpecialDivider">
            <span>4</span>
        </div>
    </div>-->
		<?php include 'includes/core/our_clients.php';?>
	<?php include 'includes/core/footer_in_full.php';?>
</body>

</html>
