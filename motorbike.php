<?php
{/* meta tags */

    $meta_title       = 'Motorbike Recovery';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Motorbike';

		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/motorbike/motorbike2.jpg';
	$og_url = 'http://www.jayegroup.com/motorbike.php';
	$og_meta_title = 'Win free motorbike recovery service!';
	$og_meta_description = 'simply like this post to enter the competition!';
	$site_name = 'Jayegroup';	
	
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>
            </div>
        </div>
    </div>
    <!-- The class motorbike is located in theme-responsive.css file -->
    <header class="elements motorbike"> 
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		
    <div class="container">
        <div class='row '>
            <div class='col-md-6 service-text'>
				<p>   A company you can rely on. We have been working in the motorbike industry for over 6 years with hands-on experience with a vast amount of bikes ranging from mopeds to ninjas to superbikes. All collected and delivered safely.<br>
                We cater for private customers and businesses bringing a great ethic support of confidence due to a high demand of customers.<br>
                We can tailor our services to meet your requirements. Give our friendly staff a call and we will be more than happy to assist you.<br>
					
				<a href="booking-form.php?action=motorbike_quote">Get a quote </a>	
				</p>
				<h4>We Cover</h4>
				
				<ul>
					<li><i class="fa fa-check "></i>Motorbike Recovery</li>
					<li><i class="fa fa-check "></i>Any location within and outside London</li>
					
				</ul>
			
			</div>
            <div class='col-sm-5 col-md-offset-1'>
                <div class="controls clearfix">
                    <div class="controls pull-right clearfix">
                        <a href="">
                            <span id="arrowNext">
                            </span>
                        </a>
                        <a href="">
                            <span id="arrowPrev">
                            </span>
                        </a>
                    </div>
                </div>
                <article class="blog-post  clearfix">
                    <figure id="folioGallery">
                        <img class="img-responsive" src="images/motorbike/bike-2.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/motorbike/bike-1.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/motorbike/bike-3.jpg" title="Image title" alt="alternative information">
                        
                    </figure>
                </article>
			</div>		
		</div>
    </div>
<br/>
<br/>
<br/>
<br/>
<br/>
	<?php include 'includes/core/services.php';?>
	<?php include 'includes/core/footer_in_full.php';?>
</body>
</html>
