<?php
{/* meta tags */

    $meta_title       = 'Home page';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Get Quote';

		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/social-media/about-us.jpg';
	$og_url = 'http://www.jayegroup.com/about.php';
	$og_meta_title = 'Jayegroup About';
	$og_meta_description = 'Jayegroup has a vision of brighter future for people who need a complete removal- delivery-disposal solution offered all in one place. We are determined to earn the lifetime loyalty of our customers by frequently delivering consistent transport services which offer great value. ';
	$site_name = 'Jayegroup';		
	
    $add_styles = '
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css" id="ColorStyle">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    ';
    
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
  
    <script src="js/bootstrap-hover-dropdown.min.js"></script>


    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa-home"></i>Home</a>
                        </li>
                        <li class="active"><?php echo $page ;?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <header class="elements about-us ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		
	<?php
	if(isset($_POST['get_quote'])) {
	
		$email_to = "donatas@creative-webuk.com";
		$email_subject = "Your email subject line";

		function died($error) {
			// your error code can go here
			echo "We are very sorry, but there were error(s) found with the form you submitted. ";
			echo "These errors appear below.<br /><br />";
			echo $error."<br /><br />";
			echo "Please go back and fix these errors.<br /><br />";
			die();
	 
		}
		// validation expected data exists
		if(!isset($_POST['firstname']) ||
			!isset($_POST['surname']) ||
			!isset($_POST['phone']) ||
			!isset($_POST['email']) ||
			!isset($_POST['movingfrom']) ||
			!isset($_POST['movingto']) ||
			!isset($_POST['select_property']) ||
			!isset($_POST['date_moving']) ||
			!isset($_POST['comments'])) {
			died('We are sorry, but there appears to be a problem with the form you submitted.');      
		} 
	
		$firstname = $_POST['firstname']; // required
		$surname = $_POST['surname']; // required
		$phone = $_POST['phone']; // required
		$email = $_POST['email']; // not required
		$movingfrom = $_POST['movingfrom']; // required	
		$movingto = $_POST['movingto']; // required	
		$select_property = $_POST['select_property']; // required	
		$date_moving = $_POST['date_moving']; // required	
		$comments = $_POST['comments']; // required	

    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
	  if(!preg_match($email_exp,$email)) {
	 
		$error_message .= 'The Email Address you entered does not appear to be valid.<br />';
	 
	  }		
   $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$firstname)) {
 
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
 
  }	
  if(!preg_match($string_exp,$surname)) {
 
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
 
  }	
 if(strlen($comments) < 2) {
 
    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
 
  }	
if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
  $email_message = "Form details below.\n\n";	
	function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }	
 $email_message .= "First Name: ".clean_string($firstname)."\n";
 
    $email_message .= "Last Name: ".clean_string($surname)."\n";
 
    $email_message .= "Email: ".clean_string($email)."\n";
 
    $email_message .= "Telephone: ".clean_string($phone)."\n";
    $email_message .= "movingfrom: ".clean_string($movingfrom)."\n";
    $email_message .= "movingto: ".clean_string($movingto)."\n";
    $email_message .= "select_property: ".clean_string($select_property)."\n";
    $email_message .= "parking: ".clean_string($parking)."\n";
    $email_message .= "date_moving: ".clean_string($date_moving)."\n";
 
    $email_message .= "Comments: ".clean_string($comments)."\n";	
	
	

		
// create email headers
 
$headers = 'From: '.$email."\r\n".
 
'Reply-To: '.$email."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  



	
	
	?>

	<h1>Thank you the form has been sent </h1>
<?php
	}
?>
	
    <div class="container" style="border:solid red 1px;">
        <div class="row ">
				<form method="post" action="" name="contactform" id="contactform">
                    <div id="ContactWrapper">
                        <div class="LeaveMassage">	
							<div class="container">
								<input name="firstname" type="text" id="name" class="form-control MassageInput" placeholder="First Name">       
								<input name="surname" type="text" id="name" class="form-control MassageInput" placeholder="Surname">      
								<input name="phone" type="text" id="phone" class="form-control MassageInput" placeholder="Contact Number">
								<input name="email" type="text" id="email" class="form-control MassageInput" placeholder="Email">   						
							</div>
							<div class="container col-sm-6">
								<input name="movingfrom" type="text" id="movingfrom" class="form-control MassageInput" placeholder="Moving From"> 
							</div>
							<div class="container col-sm-6">
								<input name="movingto" type="text" id="phone" class="form-control MassageInput" placeholder="Moving to"> 	
							</div>							
							<div class="container col-sm-6">
									 <select name="select_property" class="select-booking-option form-control ">
											<option value="none-selected">-------Select one property----------</option>
											<option value="detachedhouse">Detached House</option>
											<option value="semihouse">semihouse</option>
											<option value="terraced">Terraced</option>
											<option value="bungalow">Bungalow</option>
											<option value="basement">Basement Flat</option>
											<option value="1stfloor">1st Floor</option>
											<option value="2ndfloor">2nd Floor</option>
											<option value="3rdfloor">3rd Floor</option>
											<option value="4thfloor">4th Floor</option>
											<option value="5thfloor">5th Floor</option>
											<option value="renthouse">Renthouse</option>
											<option value="mobilehouse">Mobile House</option>
											<option value="other">Other</option>
									</select>
							</div>	
							<div class="container col-sm-6">
									<select name="parking" class="select-booking-option form-control ">
											<option value="none-selected">-------Parking Accessibility----------</option>
											<option value="advancedparking">Advanced parking permit</option>
											<option value="citycenter">City centre streets</option>
											<option value="busytraffic">Very busy traffic</option>
											<option value="Other">Other</option>
									</select>
							</div>	
							<div class="container col-sm-6">
<textarea name="comments" rows="7"  style="margin-top:20px;" placeholder="Please specify items to be moved in details: (boxes, bags, furnitures ...)" class="form-control " id="comments"></textarea>	
							</div>								 
							<div class="container ">
									<div class="container col-sm-6">
										<label for="meeting" class="lable-booking-option"><strong>When do you want to move? :</strong> </label>
									</div>									
									<div class=" container col-sm-6">
										<input id="meeting" name="get_quote" type="date" value="" class="select-booking-option form-control "/>	
									</div>	
									
                            </div>
            
                        </div>
						<div class="row">
                                    <div class="SubCleButtons pull-right clearfix">
                                        <input type="submit" name="submit" id="submit" value="SEND" class="Button ActivateButton"></input>
                                     
                                    </div>
                         
                            </div>						
                    </div>
                </form>
			
        </div>
    </div>
   <!-- <div class="container">
        <div class="SpecialDivider">
            <span>4</span>
        </div>
    </div>-->
	<?php include 'includes/core/footer_in_full.php';?>
</body>

</html>
