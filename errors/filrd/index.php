<?php
{/* meta tags */

    $meta_title       = 'Home page';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'index';
		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/social-media/home.jpg';
	$og_url = 'http://www.jayegroup.com/index.php';
	$og_meta_title = 'Jayegroup Home';
	$og_meta_description = 'Jayegroup Ltd was established in 2011 and since then we have grown as a company and have gained extensive skills with dealing with our customers. ';
	$site_name = 'Jayegroup';	
	
	
    $add_styles = '
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css" id="ColorStyle">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    ';
    
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>

<body>
<?php include 'includes/sections/core/header.php';?>
    <div id="MainSlider">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 RemovePaddingRight">
                    <div class="MainSliderContent">
                        <div class="MainSliderEntryWrapper">
                        <a data-slide-index="0" href="removals.php">
                                <div class="MainSliderEntry">
                                    <h2>Removals</h2>
                                    <p>
Moving home can be one of the most exciting events in your life and one of the most stressful. Experience has taught us that some people want to take an active part in their removal and pack themselves and others prefer to sit back and let the professional removal company do the work.
                                    </p>		
                                </div>
                            </a>						
   
                            <a data-slide-index="1" href="disposals.php">
                                <div class="MainSliderEntry">
                                    <h2>Disposals</h2>
                                    <p>
            Jayegroup offers house, garden and office clearance, as well as trade waste collection including high street retailers, markets, hotels and shopping centres. In addition to great, professional service at competitive prices, our aim is to help you dispose of your unwanted items, whether you are looking for a simple paper collection or a range of materials collected from your premises.



                                    </p>

                                </div>
                            </a>
                            <a data-slide-index="2" href="logistics.php">
                                <div class="MainSliderEntry">
                                    <h2>Logistics</h2>
                                    <p>
Jayegroup offers a local, national and international transport services delivered on time and with reliability. Our expertise within logistics industry has enabled us to become the first choice for many companies involved in the delivery of fragile goods, including a full logistics service from container unloading to door-to-door delivery within London, the UK and Europe.
                                    </p>

                                </div>
                            </a>
                           <a data-slide-index="3" href="deliveries.php">
                                <div class="MainSliderEntry">
                                    <h2>Deliveries</h2>
                                    <p>
If you need a same day delivery of your goods, you can depend on Jayegroup. Working with experienced drivers across London and the UK, we provide a professional door-to-door delivery service and whether you need a signed contract to reach a customer today or a van- load of goods moving every day, you will get the same commitment from our drivers. 
                                    </p>
                                </div>
                            </a>  
                            <a data-slide-index="4" href="storage.php">
                                <div class="MainSliderEntry">
                                    <h2>Storage</h2>
                                    <p>
Our facilities are ideal if you are moving houses, if you need an extra space to store your business goods or even if you just renovate your office. We provide dry, clean units with free access of troleys and if you need transport solution, we will organize the collection of your items and deliver them to our storage facilities in fully insured vehicles.
                                    </p>
                                </div>
                            </a>							
                        </div>
                        <div class="MainSliderButtons">
                            <ul>
                                <li><a class="active" href="booking-form.php">GET QUOTE</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 RemovePaddingLeft">
                    <div class="MainSliderImages">
<a href="#"><img src="./images/slide-show/removals-in-london.jpg" class="img-responsive" title="24/7 Anywhere and anything in the UK" alt="alternative information"> </a>
<a href="#"><img src="./images/slide-show/Disposals-home-page.jpg" class="img-responsive"  alt="Disposals"> </a>
<a href="#"><img src="./images/slide-show/logistics-home-page.jpg" class="img-responsive"  alt="logistics-home-page"></a>
<a href="#"><img src="./images/slide-show/deliveries.jpg" class="img-responsive"  alt="alternative information"></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="AnnounceTable WhiteSkin clearfix">
            <div class="col-md-9">
                <div class="AnnounceSlider">
                    <li>
                        <p>
							24/7 On time deliveries anywhere in UK
                        </p>
                    </li>
                    <li>
                        <p>
                           Find us on <a href="https://www.facebook.com/pages/Jayegroup/1543848465839707?ref=hl" target="_blank">facebook!</a>
                        </p>
                    </li>

                </div>
            </div>

            <div class="col-md-3 hidden-sm hidden-xs">
                <ul class="AnnounceDropdown pull-right">
                    <li>-</li>
                    <li class="dropdown" style="border-right: 1px solid #544b4a;"><a href="#" class="ddown dropdown-toggle" data-toggle="dropdown"><i class="fa-angle-down"></i></a>
                       <!-- <ul class="dropdown-menu">
                            <li><a href="#">Offer 1</a>
                            </li>
                            <li><a href="#">Offer 2</a>
                            </li>
                            <li><a href="#">Offer 3</a>
                            </li>
                        </ul> -->
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="removals.php">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Removals</h3>
<p>Experience the difference of a professional, customer-driven company that offers residential, commercial and business removals in fully insured vehicles.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="disposals.php">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Disposals </h3>
<p>  A comprehensive range of services, tailored to your needs for private and commercial customers.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="logistics.php">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Logistics</h3>
                                <p>A modern, cost effective solutions, dedicated to combining logistics and technology in order to streamline your operation and fulfil all your logistical requirements.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="deliveries.php">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Deliveries</h3>
                                <p>With a swift and reliable service, Jayegroup always ensures all your deliveries arrive on time and in immaculate condition.</p>
                            </div>
                        </a>
                    </div>
					<div style="clear:both;"></div>
                    <div class="col-sm-12">
                        <a href="storage.php">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Storage </h3>
                                <p>Affordable, safe, short or long-term storage facilities tailored for personal, domestic, student and business needs for our clients.</p>
                            </div>
                        </a>
                    </div>	
					
                </div>
            </div>
            <div class="col-sm-4">
                <div class="NewProject WhiteSkin">
                    <div class="NewProjectHeader">
                        <div class="NewProjectTitle">
                            <h4>Latest Projects</h4>
                        </div>
                        <a class="NewProjectSliderCtrls">
                            <span id="slider-next">
                            </span>
                        </a>
                        <a class="NewProjectSliderCtrls">
                            <span id="slider-prev">
                            </span>
                        </a>
                    </div>
                    <div id="NewProjectImageSlides">
                        <a href="#">
                            <img class="img-responsive" src="images/new-project/airport-project.jpg" title="Image title" alt="alternative information">
                        </a>
                        <a href="#">
                            <img class="img-responsive" src="./images/new-project/Military.jpg" title="Image title" alt="alternative information">
                        </a>
                        <a href="#">
                            <img class="img-responsive" src="./images/new-project/removals-project.jpg" title="Image title" alt="alternative information">
                        </a>
                    </div>
                    <div id="NewProjectContentSlides">
                        <a data-slide-index="0" href="">
                            <div class="NewProjectContent">
                                <h4>
                                  Airport Jet Fuel Delivery
                                </h4>

                                <i>
                                    5th March 2014
                                </i>

                                <p>
                                  Jayegroup had an excellent opportunity to deliver jet fuel to The Luton Airport . On load we had three pallets full of fuel that had to be delivered directly to storage units in the landing zone. It was job great done and new experience for us gained.
                                </p>
                            </div>
                        </a>
                        <a data-slide-index="1" href="">
                            <div class="NewProjectContent">
                                <h4>
                                   Military Equipment Delivery
                                </h4>

                                <i>
                                    15th March 2014
                                </i>

                                <p>
                                   Today we had a very interesting assignment collection of military equipment from Bristol to London Exel Exhibition. We arrived there on time which put us in even better working mode and as always we could be proud of our mission completed.
                                </p>
                            </div>
                        </a>
                        <a data-slide-index="2" href="">
                            <div class="NewProjectContent">
                                <h4>
                                   
                                </h4>

                                <i>
                                     18th March 2014
                                </i>

                                <p>
              We had a 5 bedroom mansion  property to remove. Our customer was very helpful and polite to our staff. We were removing a lot of fragile items so it was quite an intense task. The day went extremely well thanks to our dedicated workers who always work as a team.
                                </p>
                            </div>
                        </a>
                    </div>
                   <div class="NewProjectFooter">
                        <a href="gallery.php" class="NewProjectVeiwBtn">View Gallery</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php include 'includes/core/testimonials.php';?>
	<?php include 'includes/core/footer_in_full.php';?>
</body>
</html>
