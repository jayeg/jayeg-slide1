<?php
{/* meta tags */

    $meta_title       = 'Removals';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Removals';

		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/removals/removals-2.jpg';
	$og_url = 'http://www.jayegroup.com/removals.php';
	$og_meta_title = 'Win free removal service!';
	$og_meta_description = 'simply like this post to enter the competition!';
	$site_name = 'Jayegroup';	
	
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>
            </div>
        </div>
    </div>
    <header class="elements removals">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		
    <div class="container">
        <div class='row '>
            <div class='col-md-6 service-text'>
				<p>   We have extensive experience of removing clients from 1 bedroom properties to 12 bedroom mansions. At Jayegroup, we deliver an excellent, polite and friendly removal service making it easy for our customers to allow us to move you hassle free. All the members of staff in Jayegroup have been trained in removing heavy objects from pianos to three seater sofas. Our aim is to tailor our services to our clients' individual needs.
					
				<a href="booking-form.php">Get a quote </a>	
				</p>
				<h4>We Cover</h4>
				
				<ul>
					<li><i class="fa fa-check "></i>construction deliveries</li>
					<li><i class="fa fa-check "></i>hospital and medical</li>
					<li><i class="fa fa-check "></i>schools</li>
					<li><i class="fa fa-check "></i>councils </li>
					<li><i class="fa fa-check "></i>exhibitions</li>
				</ul>
			
			</div>
            <div class='col-sm-5 col-md-offset-1'>
                <div class="controls clearfix">
                    <div class="controls pull-right clearfix">
                        <a href="">
                            <span id="arrowNext">
                            </span>
                        </a>
                        <a href="">
                            <span id="arrowPrev">
                            </span>
                        </a>
                    </div>
                </div>
                <article class="blog-post  clearfix">
                    <figure id="folioGallery">
                        <img class="img-responsive" src="images/removals/removals-1.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/removals/removals-2.jpg" title="Image title" alt="alternative information">
                    </figure>
                </article>
			</div>		
		</div>
    </div>
<br/>
<br/>
<br/>
<br/>
<br/>
	<?php include 'includes/core/services.php';?>
	<?php include 'includes/core/footer_in_full.php';?>
</body>
</html>
