<?php
{/* meta tags */

    $meta_title       = 'Disposals';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Disposals';
	
		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/slide-show/Disposals-home-page.jpg';
	$og_url = 'http://www.jayegroup.com/disposals.php';
	$og_meta_title = 'Get a quote now!';
	$og_meta_description = ' A comprehensive range of services, tailored to your needs for private and commercial customers. ';
	$site_name = 'Jayegroup';		
	
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';
	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>
            </div>
        </div>
    </div>
    <header class="elements disposals">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		
    <div class="container">
        <div class='row'>
            <div class='col-md-6 service-text'>
				<p  >  Our waste disposal service covers the removal and environmental responsible disposal or reuse of all of the  materials and confidential waste.
All our work is carried out to the highest best practice standards, so you can be sure that not only is your  clearance carried out ethically - complying with all current UK and EU legislation - but also that every effort will be made to ensure the highest reuse and recycling rates are achieved.
<br/>
  We offer our services to private clients as well as trade waste collections from high street retailers, markets, hotels and shopping centres.	<a href="booking-form.php?action=disposals_quote">Get a quote </a>	
				</p>	
	
				<h4>We Cover</h4>
				<ul>
					<li> <i class="fa fa-check "></i>House Clearance </li>
					<li><i class="fa fa-check "></i>Rubbish Clarence</li>
				</ul>				
			</div>
            <div class='col-sm-5 col-md-offset-1'>
                <div class="controls clearfix">
                    <div class="controls pull-right clearfix">
                        <a href="">
                            <span id="arrowNext">
                            </span>
                        </a>
                        <a href="">
                            <span id="arrowPrev">
                            </span>
                        </a>
                    </div>
                </div>
                <article class="blog-post clearfix">
                    <figure id="folioGallery">
                        <img class="img-responsive" src="images/disposals/disposal-1-before.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/disposals/disposal-2-after.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="images/disposals/Disposals-home-page.jpg" title="Image title" alt="alternative information">
                    </figure>
                </article>
			</div>			
		</div>
    </div>
<br/>
<br/>
<br/>
<br/>
<br/>
	<?php include 'includes/core/services.php';?>
	<?php include 'includes/core/footer_in_full.php';?>
</body>
</html>
