<?php
{/* meta tags */

    $meta_title       = 'jayegroup';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'index';
		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/slide-show/removals-in-london.jpg';
	$og_url = 'http://www.jayegroup.com/index.php';
	$og_meta_title = 'Happy Customers!';
	$og_meta_description = 'Having dealt with JAYEGROUP on a number of occasions I have found them to be reliable, friendly and above all professional. Germaine has always shown a conscientious attitude and his attention to detail is clear from the smallest job up to the largest event. With competitive pricing and exemplary service working with JAYEGROUP makes perfect sense. - Andy';
	$site_name = 'Jayegroup';	
	
	
    $add_styles = '
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="css/site.css" rel="stylesheet" type="text/css" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    ';
    
    $add_scripts = '
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
 
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.0.2/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-nivoslider/3.2/jquery.nivo.slider.pack.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.2/owl.carousel.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-retina/0.3.1/angular-retina.min.js"></script>
    <script src="js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
    <script src="https://github.com/pipwerks/PDFObject/blob/master/pdfobject.min.js"></script>
    <!-- Latest compiled and minified CSS -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>


<body>
<div id="pamole">
<?php include 'includes/sections/core/header.php';?>
    <div id="MainSlider">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 hidden-sm hidden-xs RemovePaddingRight">
                    <div class="MainSliderContent">
                        <div class="MainSliderEntryWrapper">

                                  <a data-slide-index="0" href="<?php echo $root_url;?>deliveries<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Deliveries</h2>
                                                <p>
            If you need a same day delivery of your goods, you can depend on Jayegroup. Working with experienced drivers across London and the UK, we provide a professional door-to-door delivery service and whether you need a signed contract to reach a customer today or a van- load of goods moving every day, you will get the same commitment from our drivers. 
                                                </p>
                                            </div>
                                        </a>

                                    <!-- Motorbike delivery services added -->
                                    <!-- <a data-slide-index="4" href="<?php echo $root_url;?>motorbike<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Motorbike Recovery Service</h2>
                                                <p>
                                                    24 Hour rapid response motorbike recovery services throughout London. Fully insured; Feel at peace and let us do the work
                                                </p>        
                                            </div>
                                    </a> -->

                                    <a data-slide-index="1" href="<?php echo $root_url;?>removals<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Removals</h2>
                                                <p>
            Moving home can be one of the most exciting events in your life and one of the most stressful. Experience has taught us that some people want to take an active part in their removal and pack themselves and others prefer to sit back and let the professional removal company do the work.
                                                </p>		
                                            </div>
                                    </a>						
               
                                        <a data-slide-index="2" href="<?php echo $root_url;?>disposals<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Disposals</h2>
                                                <p>
                        Jayegroup offers house, garden and office clearance, as well as trade waste collection including high street retailers, markets, hotels and shopping centres. In addition to great, professional service at competitive prices, our aim is to help you dispose of your unwanted items, whether you are looking for a simple paper collection or a range of materials collected from your premises.

                                                </p>

                                            </div>
                                        </a>
                                        <a data-slide-index="3" href="<?php echo $root_url;?>logistics<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Logistics</h2>
                                                <p>
            Jayegroup offers a local, national and international transport services delivered on time and with reliability. Our expertise within logistics industry has enabled us to become the first choice for many companies involved in the delivery of fragile goods, including a full logistics service from container unloading to door-to-door delivery within London, the UK and Europe.
                                                </p>

                                            </div>
                                        </a>
                                         <!-- Motorbike delivery services added -->
                                    <a data-slide-index="4" href="<?php echo $root_url;?>motorbike<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Motorbike Recovery Service</h2>
                                                <p>
                                                    24 Hour rapid response motorbike recovery services throughout London. Fully insured; Feel at peace and let us do the work
                                                </p>        
                                            </div>
                                    </a>
                                     <!--   <a data-slide-index="0" href="<?php echo $root_url;?>deliveries<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Deliveries</h2>
                                                <p>
            If you need a same day delivery of your goods, you can depend on Jayegroup. Working with experienced drivers across London and the UK, we provide a professional door-to-door delivery service and whether you need a signed contract to reach a customer today or a van- load of goods moving every day, you will get the same commitment from our drivers. 
                                                </p>
                                            </div>
                                        </a> -->  
                                        <a data-slide-index="5" href="<?php echo $root_url;?>storage<?php echo $extension;?>">
                                            <div class="MainSliderEntry">
                                                <h2>Storage</h2>
                                                <p>
            Our facilities are ideal if you are moving houses, if you need an extra space to store your business goods or even if you just renovate your office. We provide dry, clean units with free access of troleys and if you need transport solution, we will organize the collection of your items and deliver them to our storage facilities in fully insured vehicles.
                                                </p>
                                            </div>
                                        </a>							
                        </div>
                        <div class="MainSliderButtons">
                            <ul>
                                <li><a class="active" href="<?php echo $root_url;?>booking-form<?php echo $extension;?>">GET QUOTE</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 RemovePaddingLeft">
                    <div class="MainSliderImages">
                        <a href="#"><img src="./images/slide-show/deliveries.jpg" class="img-responsive"  alt="alternative information"></a>
                        <!-- <a href="#"><img src="./images/slide-show/yamaha.jpg" class="img-responsive" title="24/7 Anywhere and anything in the UK" alt="alternative information"> </a> -->
                        <a href="#"><img src="./images/slide-show/removals-in-london.jpg" class="img-responsive" title="24/7 Anywhere and anything in the UK" alt="alternative information"> </a>
                        <a href="#"><img src="./images/slide-show/Disposals-home-page.jpg" class="img-responsive"  alt="Disposals"> </a>
                        <a href="#"><img src="./images/slide-show/logistics-home-page.jpg" class="img-responsive"  alt="logistics-home-page"></a>
                        <!-- <a href="#"><img src="./images/slide-show/deliveries.jpg" class="img-responsive"  alt="alternative information"></a> -->
                        <a href="#"><img src="./images/slide-show/yamaha.jpg" class="img-responsive" title="24/7 Anywhere and anything in the UK" alt="alternative information"> </a>
                        <a href="#"><img src="./images/slide-show/slide2.jpg" class="img-responsive"  alt="alternative information"></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="AnnounceTable WhiteSkin clearfix">
            <div class="col-md-9">
                <div class="AnnounceSlider">
                    <li>
                        <p>
							24/7 On time deliveries anywhere in UK
                        </p>
                    </li>
                    <li>
                        <p>
                           Find us on <a href="https://www.facebook.com/jayegroup/" target="_blank">facebook!</a>
                        </p>
                    </li>

                </div>
            </div>

            <div class="col-md-3 hidden-sm hidden-xs">
                <ul class="AnnounceDropdown pull-right">
                    <li>-</li>
                    <li class="dropdown" style="border-right: 1px solid #544b4a;"><a href="#" class="ddown dropdown-toggle" data-toggle="dropdown"><i class="fa-angle-down"></i></a>
                       <!-- <ul class="dropdown-menu">
                            <li><a href="#">Offer 1</a>
                            </li>
                            <li><a href="#">Offer 2</a>
                            </li>
                            <li><a href="#">Offer 3</a>
                            </li>
                        </ul> -->
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="<?php echo $root_url;?>deliveries<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Deliveries</h3>
                                <p>With a swift and reliable service, Jayegroup always ensures all your deliveries arrive on time and in immaculate condition.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 ">
                        <a href="<?php echo $root_url;?>motorbike<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin boxflow">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Motorbike Recovery</h3>
                                <p>Our emphasis is to get your motorbike moved with peace of mind. We will come to your exact location to collect your bike and take it to your desired destination. We cover all areas within and outside London.</p>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-sm-6">
                        <a href="<?php echo $root_url;?>removals<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Removals</h3>
<p>Experience the difference of a professional, customer-driven company that offers residential, commercial and business removals in fully insured vehicles.</p>
                            </div>
                        </a>
                    </div> -->
                    <!-- <div class="col-sm-6">
                        <a href="<?php echo $root_url;?>disposals<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Disposals </h3>
<p>  A comprehensive range of services, tailored to your needs for private and commercial customers.</p>
                            </div>
                        </a>
                    </div> -->
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="<?php echo $root_url;?>removals<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Removals</h3>
<p>Experience the difference of a professional, customer-driven company that offers residential, commercial and business removals in fully insured vehicles.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="<?php echo $root_url;?>disposals<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Disposals </h3>
<p>  A comprehensive range of services, tailored to your needs for private and commercial customers.</p>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-sm-6 boxflow">
                        <a href="<?php echo $root_url;?>logistics<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Logistics</h3>
                                <p>A modern, cost effective solutions, dedicated to combining logistics and technology in order to streamline your operation and fulfil all your logistical requirements.</p>
                            </div>
                        </a>
                    </div> -->
                    <!-- <div class="col-sm-6">
                        <a href="<?php echo $root_url;?>deliveries<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Deliveries</h3>
                                <p>With a swift and reliable service, Jayegroup always ensures all your deliveries arrive on time and in immaculate condition.</p>
                            </div>
                        </a>
                    </div> -->
                </div>
				<div class="row">
                    <div class="col-sm-6">
                        <a href="<?php echo $root_url;?>storage<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Storage </h3>
                                <p>Affordable, safe, short or long-term storage facilities tailored for personal, domestic, student and business needs for our clients.</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 boxflow">
                        <a href="<?php echo $root_url;?>logistics<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Logistics</h3>
                                <p>A modern, cost effective solutions, dedicated to combining logistics and technology in order to streamline your operation and fulfil all your logistical requirements.</p>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-sm-6 ">
                        <a href="<?php echo $root_url;?>motorbike<?php echo $extension;?>">
                            <div class="Mosaic WhiteSkin boxflow">
                                <i class="fa fa-check VeryLargeSize"></i>
                                <h3>Motorbike Recovery</h3>
                                <p>Our emphasis is to get your motorbike moved with peace of mind. We will come to your exact location to collect your bike and take it to your desired destination. We cover all areas within and outside London.</p>
                            </div>
                        </a>
                    </div> -->
</div>	
					
                
            </div>
            <div class="col-sm-4">
                <div class="NewProject WhiteSkin">
                    <div class="NewProjectHeader">
                        <div class="NewProjectTitle">
                            <h4>Latest Projects</h4>
                        </div>
                        <a class="NewProjectSliderCtrls">
                            <span id="slider-next">
                            </span>
                        </a>
                        <a class="NewProjectSliderCtrls">
                            <span id="slider-prev">
                            </span>
                        </a>
                    </div>
                    <div id="NewProjectImageSlides">
                        <a href="#">
                            <img class="img-responsive" src="images/new-project/airport-project.jpg" title="Image title" alt="alternative information">
                        </a>
                        <a href="#">
                            <img class="img-responsive" src="./images/new-project/Military.jpg" title="Image title" alt="alternative information">
                        </a>
                        <a href="#">
                            <img class="img-responsive" src="./images/new-project/removals-project.jpg" title="Image title" alt="alternative information">
                        </a>
                    </div>
                    <div id="NewProjectContentSlides">
                        <a data-slide-index="0" href="#">
                            <div class="NewProjectContent">
                                <h4>
                                  Airport Jet Oil Delivery
                                </h4>

                                <i>
                                    5th March 2014
                                </i>

                                <p>
                                  Jayegroup had an excellent opportunity to deliver jet Oil to The Luton Airport . On load we had three pallets full of Oil that had to be delivered directly to storage units in the landing zone. It was job great done and new experience for us gained.
                                </p>
                            </div>
                        </a>
                        <a data-slide-index="1" href="#">
                            <div class="NewProjectContent">
                                <h4>
                                   Military Equipment Delivery
                                </h4>

                                <i>
                                    15th March 2014
                                </i>

                                <p>
                                   Today we had a very interesting assignment collection of military equipment from Bristol to London Exel Exhibition. We arrived there on time which put us in even better working mode and as always we could be proud of our mission completed.
                                </p>
                            </div>
                        </a>
                        <a data-slide-index="2" href="#">
                            <div class="NewProjectContent">
                                <h4>
                                   
                                </h4>

                                <i>
                                     18th March 2014
                                </i>

                                <p>
              We had a 5 bedroom mansion  property to remove. Our customer was very helpful and polite to our staff. We were removing a lot of fragile items so it was quite an intense task. The day went extremely well thanks to our dedicated workers who always work as a team.
                                </p>
                            </div>
                        </a>
                    </div>
                   <div class="NewProjectFooter">
                        <a href="<?php echo $root_url;?>gallery<?php echo $extension;?>" class="NewProjectVeiwBtn">View Gallery</a>
                    </div>
                </div>
            </div>
            <!-- end of latest project -->
            <!-- beginning of the testimonial -->
            <!-- <div class="container"> -->
        <!-- <div class="row"> -->
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"> <span class="glyphicon glyphicon-list-alt"></span><b>What Our Customers Say</b>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="demo1">
                                    <li class="news-item">
                                        <table cellpadding="4">
                                            <tr>
                                                <td><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Tony Francis</td>
                                                <td>
                                                <h3>Fantastic service provided by this company</h3><p>We had very stressful time moving houses and thanks to friendly and professional approach of the staff- we managed to do it within one day.</p></td>
                                            </tr>
                                        </table>
                                    </li>
                                    <li class="news-item">
                                        <table cellpadding="4">
                                            <tr>
                                                <td><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Mark Walker</td>
                                                <td><h3>Definitely worth recommending</h3><p>I would like to say big thank you  to Jayegroup for the delivery of  our flight cases to London Excel Exhibition. Everything has been done before time and the staff were on board all the time.</p></td>
                                            </tr>
                                        </table>
                                    </li>
                                    <li class="news-item">
                                        <table cellpadding="4">
                                            <tr>
                                                <td><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Andy</td>
                                                <td><h3>Competitive pricing and Exemplary service</h3><p>Having dealt with JAYEGROUP on a number of occasions I have found them to be reliable, friendly and above all professional.</p></td>
                                            </tr>
                                        </table>
                                    </li>
                                    <li class="news-item">
                                        <table cellpadding="4">
                                            <tr>
                                                <td><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Martin Diep</td>
                                                <td><h3>Very friendly and no job is too big or small for Jayegroup</h3> <p>HHB/Source Distribution: Germaine is very reliable and hardworking individual who thrives on giving good service with a smile. I highly recommend using Jayegroup</p></td>
                                            </tr>
                                        </table>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer"> </div>
                </div>
            </div>

        <!-- </div> -->
    <!-- </div> -->
            <!-- end of the testimonial -->

        </div>
    </div>
	<!-- <?php 
    // include 'includes/core/testimonials.php';?> -->
	<?php include 'includes/core/footer_in_full.php';?>
</div>


<script type="text/javascript">
    $(function () {
        $(".demo1").bootstrapNews({
            newsPerPage: 3,
            autoplay: true,
            pauseOnHover:true,
            direction: 'up',
            newsTickerInterval: 4000,
            onToDo: function () {
                //console.log(this);
            }
        });
      
    });
</script>
</body>
</html>
