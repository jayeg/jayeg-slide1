<?php   
 
$meta_title       = 'Removals';    
$meta_description = 'meta_description';    
$meta_keywords    = 'meta_keywords';    
$meta_author      = '';        
$page             = 'Storage';	

$og_meta_image = 'http://www.containermodifications.com/images/shipping%20container%20modification%20and%20repair%20019_01.jpg';	
$og_url = 'http://www.jayegroup.com/storage.php';	
$og_meta_title = 'Storage from Jayegroup ';	
$og_meta_description = 'Affordable, safe, short or long-term storage facilities tailored for personal, domestic, student and business needs for our clients.';	
$site_name = 'Jayegroup';	    

$add_scripts = '    <script src="js/jquery-1.10.2.min.js"></script>    <script src="js/bootstrap.min.js"></script>    <script src="js/bootstrap-hover-dropdown.min.js"></script>    <script src="js/jquery.bxslider.min.js"></script>    <script src="js/jquery.nivo.slider.pack.js"></script>    <script src="owl-carousel/owl.carousel.js"></script>    <script src="js/jquery.fancybox.js"></script>    <script src="js/jquery.easing.1.3.js"></script>    <script src="js/progressbar-plugin.js"></script>    <script src="js/jquery.fittext.js"></script>    <script src="js/customTwo.js"></script>    <script src="js/color-picker.js"></script>    <script src="js/modernizr.js"></script>    <script src="js/jquery.retina.js"></script>    ';	

require_once 'includes/site_settings/main_head.php';	  ?>

<body><?php include 'includes/sections/core/header.php';?>    <div class="BreadCrumbWrapper WhiteSkin">        <div class="container">            <div class="row">                

                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>        


 </div>        </div>    </div>    <header class="elements storage">        <div class="container">            <div class="row">                <div class="col-sm-12 slogan">                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>                </div>            </div>        </div>    </header>		    <div class="container">        <div class='row '>            <div class='col-md-6 service-text'>				<p  >                    Jayegroup offers overnight storage services in a secure, monitored unit surveilanced 24 hrs a day, 365 days, a year. We have good rates available for short or extended period of time. We have different size units, whether you need to store your household items or you need some extra storage space for your business.We provide dry, clean units with free access of troleys and if you need transport solution, we will organize the collection of your items and deliver them to our storage facilities in fully insured vehicles.<br/>Jayegroup team are always happy to assist you with all your individual storage requirements.		<a href="booking-form.php?action=storage_quote">Get a quote </a>			</p>				<h4 >We Cover</h4>				<ul>					<li><i class="fa fa-check "></i>  24/7 CCTV </li>				</ul>				</div>            <div class='col-sm-5 col-md-offset-1'>                <div class="controls clearfix">                    <div class="controls pull-right clearfix">                        <a href="">                            <span id="arrowNext">                            </span>                        </a>                        <a href="">                            <span id="arrowPrev">                            </span>                        </a>                    </div>                </div>                <article class="blog-post  clearfix">                    <figure id="folioGallery">                        <img class="img-responsive" src="images/storage/storage-small-1.jpg" title="Image title" alt="alternative information">                        <img class="img-responsive" src="images/storage/storage-small-2.jpg" title="Image title" alt="alternative information">                        <img class="img-responsive" src="images/storage/storage-small-3.jpg" title="Image title" alt="alternative information">                    </figure>                </article>			</div>							</div>    </div><br/><br/><br/><br/><br/>	<?php include 'includes/core/services.php';?>	<?php include 'includes/core/footer_in_full.php';?></body></html>