<?php
{/* meta tags */

    $meta_title       = 'jayegroup';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Get Quote';

		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/social-media/about-us.jpg';
	$og_url = 'http://www.jayegroup.com/about.php';
	$og_meta_title = 'Jayegroup About';
	$og_meta_description = 'Jayegroup has a vision of brighter future for people who need a complete removal- delivery-disposal solution offered all in one place. We are determined to earn the lifetime loyalty of our customers by frequently delivering consistent transport services which offer great value. ';
	$site_name = 'Jayegroup';		
	
    $add_styles = '
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css" id="ColorStyle">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    ';
    
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa-home"></i>Home</a>
                        </li>
                        <li class="active"><?php echo $page ;?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <header class="elements about-us ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		


	
    <div class="container" >
        <div class="row ">
				<form method="post" action="" >
	<?php
	if(isset($_POST['get_quote'])) {
	
		//$email_to = "donatas@creative-webuk.com";
		$email_to = "info@jayegroup.com";
		$email_subject = "Website Quote Form";

		function died($error) {
			// your error code can go here
			echo "We are very sorry, but there were error(s) found with the form you submitted. ";
			echo "These errors appear below.<br /><br />";
			echo $error."<br /><br />";
			echo "Please go back and fix these errors.<br /><br />";
			die();
	 
		}
		// validation expected data exists
		if(!isset($_POST['firstname']) ||
			!isset($_POST['surname']) ||
			!isset($_POST['phone']) ||
			!isset($_POST['email']) ||
			!isset($_POST['subject']) ||
			!isset($_POST['comments'])) {
			died('We are sorry, but there appears to be a problem with the form you submitted.');      
		} 
	
		$subject = $_POST['subject']; // required
		$firstname = $_POST['firstname']; // required
		$surname = $_POST['surname']; // required
		$phone = $_POST['phone']; // required

		$email = $_POST['email']; // not required
		$movingfrom = $_POST['movingfrom']; // required	
		$movingto = $_POST['movingto']; // required	
		
		$select_property = $_POST['select_property']; // required	
		$parking = $_POST['parking']; // required	

		$date_moving = $_POST['date_moving']; // required	
		$comments = $_POST['comments']; // required	

    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
	  if(!preg_match($email_exp,$email)) {
	 
		$error_message .= 'The Email Address you entered does not appear to be valid.<br />';
	 
	  }		
   $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$firstname)) {
 
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
 
  }	
  if(!preg_match($string_exp,$surname)) {
 
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
 
  }	
 if(strlen($comments) < 2) {
 
    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
 
  }	
if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
  $email_message = "Quote Form Details Below.
 
  
  \n\n";	
	function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }	

	
	$email_message .= "Subject: ".clean_string($subject)."\n";
	$email_message .= "First Name: ".clean_string($firstname)."\n";
    $email_message .= "Last Name: ".clean_string($surname)."\n";
    $email_message .= "Email: ".clean_string($email)."\n";
    $email_message .= "Telephone: ".clean_string($phone)."\n";
	
    $email_message .= "Moving from : ".clean_string($movingfrom)."\n";
    $email_message .= "Moving To: ".clean_string($movingto)."\n";
	
    $email_message .= "Selected Property: ".clean_string($select_property)."\n";
    $email_message .= "Parking: ".clean_string($parking)."\n";
    $email_message .= "Date Moving: ".clean_string($date_moving)."\n";
 
    $email_message .= "Comments: ".clean_string($comments)."\n";	

		
// create email headers
 
$headers = 'From: '.$email."\r\n".
 
'Reply-To: '.$email."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  



	
	
	?>
<div class="container">
	<div class="row">
		<h2>Thank you for submitting your form .We will get back to you within one working hour. Jayegroup Team </h2>
		<br/>
	
	</div>
</div>

<?php
	}
?>		
                    <div id="ContactWrapper">
                        <div class="LeaveMassage">	
							<div class="container">
								<div style="width:45%; float:left;">
									<input name="subject" type="text" id="subject" class="form-control MassageInput" placeholder="Subject *"/>        
									<input name="firstname" type="text" id="name" class="form-control MassageInput" placeholder="First Name *"/>        
									<input name="surname" type="text" id="surname" class="form-control MassageInput" placeholder="Surname *"/>      
									<input name="phone" type="text" id="phone" class="form-control MassageInput" placeholder="Contact Number *"/>
									<input name="email" type="text" id="email" class="form-control MassageInput" placeholder="Email *"/>   	
								</div>
								<div style="width:45%; float:right;">
									<input name="movingfrom" type="text" id="movingfrom" class="form-control MassageInput" placeholder="From"> 
									<input name="movingto" type="text" id="phone" class="form-control MassageInput" placeholder="To"> 	
			<?php
	
				if($_REQUEST['action']=="storage_quote")
				{
					echo '
										<select name="select_property" class="select-booking-option form-control ">
												<option value="none-selected">-------Storage sizes 10sq sqft----------</option>
												<option value="detachedhouse">Detached House</option>
												<option value="semihouse">  15    sqft</option>
												<option value="terraced"> 25    Sqft</option>
												<option value="bungalow"> 40    sqft</option>
												<option value="basement"> 50     sqft </option>
												<option value="1stfloor"> 75     sqft</option>
												<option value="2ndfloor">100   sqft</option>
												<option value="3rdfloor">150   sqft</option>
												<option value="4thfloor">200   Sqft</option>
										</select> 					

					';
				} else if ($_REQUEST['action']=="motorbike_quote") {
					echo '
										<select name="select_bike" class="select-booking-option form-control ">
												<option value="none-selected">-------Select Bike Brand----------</option>
												<option value="yamaha">Yamaha</option>
												<option value="honda">Honda</option>
												<option value="kawasaki">Kawasaki</option>
												<option value="suzuki">Suzuki</option>
												<option value="apilla">Apilla</option>
												<option value="piaggio">Piaggio</option>
												<option value="gilera">Gilera</option>
												<option value="bmw">Bmw</option>
												<option value="ducati">Ducati</option>
												<option value="triumph">Triumph</option>
												<option value="moto">Moto</option>
												<option value="guzzi">Guzzi</option>
												<option value="harley">Harley</option>
										</select> 		<br>
										<select name="select_cc" class="select-booking-option form-control style="position:relative; top:-8px; ">
												<option value="none-selected">-------Select Bike CC----------</option>
												<option value="50cc">50cc</option>
												<option value="125cc">125cc</option>
												<option value="400cc">400cc</option>
												<option value="600cc">600cc</option>
												<option value="1000cc">1000cc</option>
										</select> 		

					';
				}

				else {
					echo '
					
						
								
						';
				}
			
			?>
									
		
								</div>
			<?php
	
				if($_REQUEST['action']=="deliveries_quote" || $_REQUEST['action']=="disposals_quote" || $_REQUEST['action']=="logistics_quote" || $_REQUEST['action']=="storage_quote" || $_REQUEST['action']=="motorbike_quote")
				{
					echo '
					

					';
				} else {
					echo '
					
								<div style="width:45%; float:right;">
										 <select name="select_property" class="select-booking-option form-control ">
												<option value="none-selected">-------Select one property----------</option>
												<option value="detachedhouse">Detached House</option>
												<option value="semihouse">semihouse</option>
												<option value="terraced">Terraced</option>
												<option value="bungalow">Bungalow</option>
												<option value="basement">Basement Flat</option>
												<option value="1stfloor">1st Floor</option>
												<option value="2ndfloor">2nd Floor</option>
												<option value="3rdfloor">3rd Floor</option>
												<option value="4thfloor">4th Floor</option>
												<option value="5thfloor">5th Floor</option>
												<option value="renthouse">Renthouse</option>
												<option value="mobilehouse">Mobile House</option>
												<option value="other">Other</option>
										</select>
												<br/>
										<select name="parking" class="select-booking-option form-control " style="position:relative; top:-8px;">
												<option value="none-selected">-------Parking Accessibility----------</option>
												<option value="advancedparking">Advanced parking permit</option>
												<option value="citycenter">City centre streets</option>
												<option value="busytraffic">Very busy traffic</option>
												<option value="Other">Other</option>
										</select>			
								</div>					<br/>				
								
						';
				}
			
			?>
			<?php 
					echo '<div style="width:45%; float:right;" >
										 
										<select name="advertised" class="select-booking-option form-control ">
												<option value="none-selected">-------Where You Saw Us Advertised----------
												</option>
												<option value="newspaper">Newspaper</option>
												<option value="leaflet">Leaflet</option>
												<option value="internet">Internet</option>
												<option value="motorbikeshop">Motorbike Shop</option>
												<option value="showindow">Shop Window</option>
												<option value="recommendation">Recommendation</option>
										</select>			
								</div>	
							'
			?>
			<?php
	
				if($_REQUEST['action']=="deliveries_quote" || $_REQUEST['action']=="disposals_quote" || $_REQUEST['action']=="logistics_quote" || $_REQUEST['action']=="storage_quote")
				{
					echo '
					
<textarea name="comments" rows="7"  style="margin-top:20px;" placeholder="Please specify  details:  *" class="form-control " id="comments"></textarea>								
					
					';
				} else {
					echo '
					
<textarea name="comments" rows="7"  style="margin-top:20px;" placeholder="Please specify items to be moved in details: (boxes, bags, furnitures ...) *" class="form-control " id="comments"></textarea>										
								
						';
				}
			
			?>			
			
			

<input id="meeting" name="date_moving" type="date" placeholder="Please enter a date example: 20/11/2014" value="" class="select-booking-option form-control "  style="position:relative; top:10px; width:45%; "/>	
                       <div class="SubCleButtons  clearfix" style="position:relative;  top:10px; left:-20px;">
                                        <input type="submit" name="get_quote" id="submit" value="Send Form" class="Button ActivateButton"></input>
                                    </div>
							</div>	


                        </div>
							
                    </div>
                </form>
			
        </div>
    </div>
   <!-- <div class="container">
        <div class="SpecialDivider">
            <span>4</span>
        </div>
    </div>-->
	<?php include 'includes/core/footer_in_full.php';?>
</body>

</html>
