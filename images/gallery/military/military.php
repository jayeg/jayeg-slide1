                <div class="element military portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/military/1.jpg" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/military/1.jpg" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>
                <div class="element military portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/military/2.jpg" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/military/2.jpg" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>
                <div class="element military portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/military/3.jpg" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/military/3.jpg" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>				
                <div class="element military portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/military/4.jpg" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/military/4.jpg" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>				