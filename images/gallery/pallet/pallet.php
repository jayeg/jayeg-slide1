                <div class="element pallet portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/pallet/1.JPG" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/pallet/1.JPG" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>
                <div class="element pallet portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/pallet/2.JPG" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/pallet/2.JPG" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>
                <div class="element pallet portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/pallet/3.jpg" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/pallet/3.jpg" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>				
                <div class="element pallet portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/pallet/4.JPG" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/pallet/4.JPG" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>				