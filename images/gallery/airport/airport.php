                <div class="element airport portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/delivery/1.jpg"  rel='gallery' title="Image title" alt="alternative information">

                        <div class="icon-overlay">
                     
                            <a href="images/gallery/delivery/1.jpg" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>
                <div class="element airport portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/delivery/2.jpg"  rel='gallery' title="Image title" alt="alternative information">

                        <div class="icon-overlay">
                     
                            <a href="images/gallery/delivery/2.jpg" rel='gallery' class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>
