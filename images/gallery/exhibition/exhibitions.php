

                <div class="element exhibitions portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/exhibition/3.JPG" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/exhibition/3.JPG" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>
                <div class="element exhibitions portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/exhibition/5.JPG" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/exhibition/5.JPG" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>				
                <div class="element exhibitions portfolio-mosaic col-sm-3 ">
                    <figure>
                        <img class="img-responsive" src="images/gallery/exhibition/6.jpg" title="Image title" alt="alternative information">
                        <div class="icon-overlay">
                            <a href="images/gallery/exhibition/6.jpg" class="fancybox"><i class="fa-search"></i></a>
                        </div>
                    </figure>
                </div>				