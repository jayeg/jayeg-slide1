<?php
{/* meta tags */

    $meta_title       = 'jayegroup';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'index';

    $add_styles = '
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css" id="ColorStyle">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    ';
    
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>
<body>
              <!--
    ============================================
    HEADER
    ============================================= -->
    <div class="container">
        <div class="row">
            <!-- ----------- Begin:Header ----------- -->
				<?php include 'includes/site_header.php';?>
            <!-- ----------- Finish:Header ----------- -->
        </div>
    </div>


          <!--
    ============================================
    NAVIGATION
    ============================================= -->
    <div id="NavigationWrapper" class="clearfix">
        <div class="container">
            <div class="row">
                <!-- ----------- Begin:Slogan ----------- -->
                <div class="col-md-4 col-sm-12 RemovePaddingRight">
                    <div class="Slogan">
                        <h3>Slogan here </h3>
                    </div>
                </div>
                <!-- ----------- Finish:Slogan ----------- -->

                <!-- ----------- Begin:Navigation ----------- -->
                <div class="col-md-8 col-sm-12 RemovePaddingLeft">
                    <?php include('includes/navigation.php'); ?>
                </div>
                <!-- ----------- Finish:Navigation ----------- -->
            </div>
        </div>
    </div>

    <!--
    ============================================
    BREADCRUMB
    ============================================= -->
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa-home"></i>Home</a>
                        </li>
                        <li class="active">Exhibitions</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!--

    <!--
    ============================================
    Special Divider
    ============================================= -->
    <div class="container">
        <div class="SpecialDivider">
            
        </div>
        <div class='row'>
            <div class='col-md-12'>
        <h1 class='text-center'>Exhibitions</h1>
        <p>Jayegroup offer exhibitions, corporate events, trade shows and music festivals services. We are familiar with most UK’s and London’s major venues.
We collect from your business premises, store if required and deliver your equipment to the destination and after the event; we can assist in packing and palletising your equipment and return it to your premises.
All your items are fully insured right from the moment of collection till we deliver them back to you.
</p>
    </div>
</div>
    </div>

    <br /><br />

    <!--
    ============================================
    TESTIMONIALS
    ============================================= -->
	<?php include 'includes/core/testimonials.php';?>
	
	
		<?php include 'includes/core/our_clients.php';?>
	
	
	<?php include 'includes/core/footer_in_full.php';?>
</body>

</html>
