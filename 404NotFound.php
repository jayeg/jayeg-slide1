<?php
{/* meta tags */

    $meta_title       = 'jayegroup';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'index';
		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/social-media/home.jpg';
	$og_url = 'http://www.jayegroup.com/index.php';
	$og_meta_title = 'Jayegroup Home';
	$og_meta_description = 'Jayegroup Ltd was established in 2011 and since then we have grown as a company and have gained extensive skills with dealing with our customers. ';
	$site_name = 'Jayegroup';	
	
	
    $add_styles = '
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/style.css" id="ColorStyle">
    <link rel="stylesheet" href="css/theme-responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    ';
    
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';

	require_once 'includes/site_settings/main_head.php';
	
  } 
  ?>

<body>
<?php include 'includes/sections/core/header.php';?>
	 <div class="container">

		<!--
		============================================
		ERROR 404
		============================================= -->
		<div class="container">
			<div class="row">
				<div class="Error404 clearfix">
					<h1 id="fittext">4<i class="Orange">0</i>4</h1>
					<h2>THE PAGE CANNOT BE FOUND</h2>
				</div>
			</div>
		</div>
	</div>
	<?php include 'includes/core/footer_in_full.php';?>
</body>
</html>