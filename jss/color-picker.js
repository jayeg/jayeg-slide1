    (function($) {
        "use strict";
        // With the element initially hidden, we can show it slowly:
        $("#OpenCP").click(function() {
            $("#colorPicker").fadeIn();
        });

        $("#closeCP").click(function() {
            $(this).closest("#colorPicker").fadeOut();
        });

        $("#orange").click(function() {
            $("#colorStyle").attr("href", "css/style.css");
            $("#colorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style.css");
            return false;
        });

        $("#green").click(function() {
            $("#colorStyle").attr("href", "css/colors/green.css");
            $("#colorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style-green.css");
            return false;
        });

        $("#blue").click(function() {
            $("#colorStyle").attr("href", "css/colors/blue.css");
            $("#colorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style-blue.css");
            return false;
        });

        $("#purple").click(function() {
            $("#colorStyle").attr("href", "css/colors/purple.css");
            $("#colorSlider").attr("href", "masterslider/gallery/style/ms-gallery-style-purple.css");
            return false;
        });


    }(jQuery));
