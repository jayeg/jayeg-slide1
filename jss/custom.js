// ================================================
// Default Bootstrap Slider
// ================================================
$(document).ready(function() {
    $('.carousel').carousel({
        interval: 6000
    })
});
$(document).ready(function() {
    $('.manual').carousel({
        interval: false
    })
});
// ================================================
// Activate Pager For Testimonials
// ================================================
$(document).ready(function() {
    var current = $('.panel-container a > li');
    $('.panel-container a > li').click(function() {
        current.removeClass("active");
        current = $(this);
        current.addClass("active");
    });
});
// ================================================
// Activate slideshow for services
// ================================================
$(document).ready(function() {
    var current = $('.service-item');
    $(".service-item").click(function() {
        current.removeClass("active");
        current = $(this);
        current.addClass("active");
    });
});
// ================================================
// call knob for progress bars in about page
// ================================================
$(document).ready(function() {
    $(".dial").knob({
        bgColor: '#ff9311'
    });
});
// ================================================
// activate close button for widgets
// ================================================
$(document).ready(function() {
    $(".hideWidget").click(function() {
        
        $(this).parent().parent().fadeOut();
    return false;
    });
});
// ================================================
// plus and minus sign for accordion widget
// ================================================
$("ul#AccordionCaret > li > a").click(function() {
    $(this).toggleClass("Minus");
});
// ================================================
// call fittext plugin for responsive error 404 text
// ================================================
$(document).ready(function() {
    $("#fittext").fitText(0.2, {
        minFontSize: 30,
        maxFontSize: '240px'
    });
});
// ================================================
// jQuery scroll to up
// ================================================
$('#scrollTop').on("click", function() {
    $('html,body').animate({
        scrollTop: 0
    }, 'slow', function() {});
});
// ================================================
// Main Slider
// ================================================
$('#sliderImages').bxSlider({
    pagerCustom: "#pagerCustom",
    controls: false,
    minSlides: 1,
    maxSlides: 1,
    auto: true,
    mode: "horizontal",
    controls: true,
    autoHover: true,
    preloadImages: "all",
    prevText: '<i class="fa-angle-left"></i>',
    nextText: '<i class="fa-angle-right"></i>',
    captions: true,
    preloadImages: "visible"
});
// ================================================
// Latest News Slider
// ================================================
$('#newsSlider').bxSlider({
    minSlides: 1,
    maxSlides: 1,
    auto: true,
    autoControls: false,
    pager: false,
    controls: false,
    autoHover: true,
    mode: "vertical"
});
// ================================================
// New Project Slider
// ================================================
$('#projectImage').bxSlider({
    pagerCustom: "#projectPager",
    controls: false,
    minSlides: 1,
    maxSlides: 1,
    auto: true,
    mode: "fade",
    controls: true,
    autoHover: true,
    preloadImages: "visible",
    nextSelector: '#sliderNext',
    prevSelector: '#sliderPrev',
    prevText: '<i class="fa-angle-left"></i>',
    nextText: '<i class="fa-angle-right"></i>',
    captions: false
});
// ================================================
// Portfolio Image Slider
// ================================================
$('#folioGallery').bxSlider({
    controls: false,
    minSlides: 1,
    maxSlides: 1,
    auto: false,
    mode: "fade",
    pager: false,
    controls: true,
    autoHover: true,
    preloadImages: "all",
    nextSelector: '#arrowNext',
    prevSelector: '#arrowPrev',
    prevText: '<i class="fa-angle-left"></i>',
    nextText: '<i class="fa-angle-right"></i>',
    captions: false
});
// ================================================
// retina display
// ================================================
$(document).ready(function() {
    $('img').retina();
});
// ================================================
// JQUERY (fancy) Script for the awesome fancybox 
// ==================================================
$(document).ready(function() {
    $(".fancybox").fancybox({
        helpers: {
            overlay: {
                locked: false
            }
        },
        padding: ['5px', '5px', '5px', '5px']
    });
});
// ================================================
// JQUERY nivo slider
// ================================================
$(window).load(function() {
    $('#slider').nivoSlider({
        effect: 'random',
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 500,
        pauseTime: 5000,
        startSlide: 0,
        directionNav: true,
        controlNav: false,
        controlNavThumbs: false,
        pauseOnHover: true,
        manualAdvance: false,
        prevText: '<i class="fa-angle-left"></i>',
        nextText: '<i class="fa-angle-right"></i>',
        randomStart: false
    });
});
// ================================================
// Services Carousel, Owl Carousel
// ================================================
$(document).ready(function() {
    var owl = $("#svcCarousel");
    owl.owlCarousel({
        items: 6, //10 items above 1000px browser width
        navigation: false,
        pagination: false
    });
    // Custom Navigation Events
    $(".next").click(function() {
        owl.trigger('owl.next');
    })
    $(".prev").click(function() {
        owl.trigger('owl.prev');
    })
});

// ================================================
// MasterSlider Configuration
// ================================================
    var sliderStaffStyle1 = new MasterSlider();
    sliderStaffStyle1.setup('mastersliderStaffStyle1' , {
        loop:true,
        width:240,
        height:240,
        speed:20,
        view:'stf',
        preload:'all',
        space:0,
        wheel:true
    });
    sliderStaffStyle1.control('arrows');
    sliderStaffStyle1.control('slideinfo',{insertTo:'#staff-infoStaffStyle1'});

    var sliderStaffStyle2 = new MasterSlider();
    sliderStaffStyle2.setup('mastersliderStaffStyle2' , {
        loop:true,
        width:240,
        height:240,
        speed:20,
        view:'stffade',
        preload:0,
        space:0,
        wheel:true
    });
    sliderStaffStyle2.control('arrows');
    sliderStaffStyle2.control('slideinfo',{insertTo:'#staff-infoStaffStyle2'});

    var sliderStaffStyle3 = new MasterSlider();
    sliderStaffStyle3.setup('mastersliderStaffStyle3' , {
        loop:true,
        width:240,
        height:240,
        speed:20,
        view:'stf',
        preload:0,
        space:35,
        viewOptions:{centerSpace:1.6}
    });
    sliderStaffStyle3.control('arrows');
    sliderStaffStyle3.control('slideinfo',{insertTo:'#staff-infoStaffStyle3'});

    var sliderStaffStyle4 = new MasterSlider();
    sliderStaffStyle4.setup('mastersliderStaffStyle4' , {
        loop:true,
        width:240,
        height:240,
        speed:20,
        view:'stffade',
        preload:0,
        wheel:true,
        space:45
    });
    sliderStaffStyle4.control('arrows');
    sliderStaffStyle4.control('slideinfo',{insertTo:'#staff-infoStaffStyle4'});

    var sliderStaffStyle5 = new MasterSlider();
    sliderStaffStyle5.setup('mastersliderStaffStyle5' , {
        loop:true,
        width:240,
        height:240,
        speed:20,
        view:'wave',
        preload:0,
        space:0,
        wheel:true
    });
    sliderStaffStyle5.control('arrows');
    sliderStaffStyle5.control('slideinfo',{insertTo:'#staff-infoStaffStyle5'});

    var sliderStaffStyle6 = new MasterSlider();
    sliderStaffStyle6.setup('mastersliderStaffStyle6' , {
        loop:true,
        width:240,
        height:240,
        speed:20,
        view:'flow',
        preload:0,
        space:0,
        wheel:true
    });
    sliderStaffStyle6.control('arrows');
    sliderStaffStyle6.control('slideinfo',{insertTo:'#staff-infoStaffStyle6'});

    var sliderLayer = new MasterSlider();
    sliderLayer.setup('mastersliderLayer' , {
        width:1400,
        height:580,
        //space:100,
        fullwidth:true,
        centerControls:false,
        speed:18,
        autoplay:true,
        loop:true,
        view:'flow'
    });
    //slider.control('arrows'); 
    sliderLayer.control('bullets' ,{autohide:true});


    var sliderPartial1 = new MasterSlider();
    sliderPartial1.setup('mastersliderPartial1' , {
        width:760,
        height:400,
        space:10,
        loop:true,
        autoplay:true,
        view:'prtialwave'
    });
    
    sliderPartial1.control('arrows');   
    sliderPartial1.control('slideinfo',{insertTo:"#partial-view-1" , autohide:false});
    sliderPartial1.control('circletimer' , {color:"#FFFFFF" , stroke:9});


    var sliderPartial2 = new MasterSlider();
    sliderPartial2.setup('mastersliderPartial2' , {
        width:760,
        height:400,
        space:10,
        loop:true,
        autoplay:true,
        view:'prtialwave2'
    });
    sliderPartial2.control('arrows');   
    sliderPartial2.control('slideinfo',{insertTo:"#partial-view-1" , autohide:false});
    sliderPartial2.control('circletimer' , {color:"#FFFFFF" , stroke:9});


    var sliderPartial3 = new MasterSlider();
    sliderPartial3.setup('mastersliderPartial3' , {
        width:760,
        height:400,
        space:10,
        autoplay:true,
        loop:true,
        view:'prtialwave3'
    });
    sliderPartial3.control('arrows');   
    sliderPartial3.control('slideinfo',{insertTo:"#partial-view-1" , autohide:false});
    sliderPartial3.control('circletimer' , {color:"#FFFFFF" , stroke:9});

    var sliderTabs = new MasterSlider();
    sliderTabs.setup('mastersliderTabs' , {
        width:1336,
        height:580,
        fullwidth:true,
        space:0,
        autoplay:true,
        loop:true,
        preload:'all',
        view:'basic'
    });
    sliderTabs.control('arrows');   
    sliderTabs.control('circletimer' , {color:"#FFFFFF" , stroke:9});
    sliderTabs.control('thumblist' , {autohide:false ,dir:'h'});

    var sliderVideo = new MasterSlider();
    sliderVideo.setup('mastersliderVideo', {
        width : 1336,
        height : 478,
        space : 5,
        shuffle : true,
        fullwidth:true,
        autoplay:true,
        loop:true,
        view : 'basic'
    });
    sliderVideo.control('arrows');
    sliderVideo.control('thumblist', {autohide : false,  dir : 'h'});