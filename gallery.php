<?php    

$meta_title       = 'jayegroup';    
$meta_description = 'meta_description';    
$meta_keywords    = 'meta_keywords';    
$meta_author      = '';  
  
$page             = 'Our Gallery';	
$og_meta_image = 'http://www.jayegroup.com/images/gallery/exhibition/3.JPG';	
$og_url = 'http://www.jayegroup.com/gallery.php';	
$og_meta_title = 'See us hard working!';	
$og_meta_description = 'check out the images of our work.';	
$site_name = 'Jayegroup';    

$add_scripts = '   <!-- javascript    ================================================== -->    <!-- These are all the javascript files -->   <script src="js/bootstrap.min.js"></script>    <!-- Plugins -->    <script src="js/jquery.isotope.min.js"></script>    <script src="js/jquery.infinitescroll.min.js"></script>    <script src="js/jquery.manual-trigger.js"></script>    <script src="js/isotop-custom.js"></script>    <script src="js/bootstrap-hover-dropdown.min.js"></script>    <script src="js/jquery.bxslider.min.js"></script>    <script src="js/jquery.nivo.slider.pack.js"></script>    <script src="owl-carousel/owl.carousel.js"></script>    <script src="js/jquery.fancybox.js"></script>    <script src="js/jquery.easing.1.3.js"></script>    <script src="js/progressbar-plugin.js"></script>    <script src="js/jquery.fittext.js"></script>    <script src="js/custom.js"></script>    <script src="js/color-picker.js"></script>    <script src="js/modernizr.js"></script>    <script src="js/jquery.retina.js"></script>';			

require_once 'includes/site_settings/main_head.php';  ?>

<body><?php include 'includes/sections/core/header.php';?>    <div class="BreadCrumbWrapper WhiteSkin">        <div class="container">            <div class="row">              

                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>    


	</div>        </div>    </div>    <header class="elements gallery">        <div class="container">            <div class="row">                <div class="col-sm-12 slogan">                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>                </div>            </div>        </div>    </header>	    <div class="container">        <nav class="navbar navbar-default portfolio-navbar" role="navigation">            <div class="navbar-header">               

	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#portfolioNavCollapse">                    <span class="sr-only">Toggle navigation</span>                    <span class="icon-bar"></span>                    <span class="icon-bar"></span>                    <span class="icon-bar"></span>               

	</button>                <a class="navbar-brand navbar-title" href="#">Our Gallery</a>            </div>            <div class="collapse navbar-collapse" id="portfolioNavCollapse">                <ul id="filters" data-option-key="filter" class="nav navbar-nav navbar-right portfolio-filter">                    <li><a href="#show-all" data-option-value="*">ALL</a> </li>                    <li><a href="#airport" data-option-value=".airport">Airport</a> </li>                    <li><a href="#delivery" data-option-value=".delivery" class="selected">Deliveries</a> </li>                    <li><a href="#exhibitions" data-option-value=".exhibitions">Exhibitions</a>                    <li><a href="#military" data-option-value=".military">Military</a>                    <li><a href="#pallet" data-option-value=".pallet">Pallets</a>                    <li><a href="#removals" data-option-value=".removals" >Removals</a>                    </li>                </ul>            </div>        </nav>    </div>    <div class="container">        <div class="row">           

	<div id="container">			<!--- airport -->			

	<?php include 'images/gallery/airport/airport.php';?>								<!--- delivery -->				<?php include 'images/gallery/delivery/delivery.php';?>										<!--- exhibitions -->				<?php include 'images/gallery/exhibition/exhibitions.php';?>								<!--- military -->				<?php include 'images/gallery/military/military.php';?>								<!--- pallets -->				<?php include 'images/gallery/pallet/pallet.php';?>												<!--- removals -->				<?php include 'images/gallery/removals/removals.php';?>					            </div>        </div>    </div>   <!-- <div class="container">        <div class="SpecialDivider">            <span>4</span>        </div>    </div>-->	<br/>	
	
	
	<?php include 'includes/core/footer_in_full.php';?></body></html>