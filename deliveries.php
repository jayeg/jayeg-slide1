<?php

    $meta_title       = 'Removals';
    $meta_description = 'meta_description';
    $meta_keywords    = 'meta_keywords';
    $meta_author      = '';
    
    $page             = 'Deliveries';
	
		// social media
	$og_meta_image = 'http://www.jayegroup.com/images/slide-show/deliveries.jpg';
	$og_url = 'http://www.jayegroup.com/deliveries.php';
	$og_meta_title = 'Deliveries by Jayegroup';
	$og_meta_description = 'With a swift and reliable service, Jayegroup always ensures all your deliveries arrive on time and in immaculate condition.';
	$site_name = 'Jayegroup';		
	
    $add_scripts = '
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="owl-carousel/owl.carousel.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/progressbar-plugin.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/customTwo.js"></script>
    <script src="js/color-picker.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/jquery.retina.js"></script>
    ';
	require_once 'includes/site_settings/main_head.php';
	

  ?>
<body>
<?php include 'includes/sections/core/header.php';?>
    <div class="BreadCrumbWrapper WhiteSkin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
						<?php include "includes/breadcrumb.php";?>
                    </ol>                        
                </div>
            </div>
        </div>
    </div>
    <header class="elements delivery">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 slogan">
                    <h2 style="font-weight: bold;"><?php echo $page ;?></h2>
                </div>
            </div>
        </div>
    </header>		
    <div class="container">

        <div class='row '>
            <div class='col-md-6 service-text'>
				<p  >                    We pride ourselves on providing local customer service, backed by our national presence and intuitive online booking, tracking and reporting platform. We work with a range of businesses in different sectors including healthcare, public sector and manufacturing.
Our experience ensures that we can deliver a bespoke solution tailored to your needs, whether you require a scheduled, dedicated or adhoc service.
Our vehicles are equipped with pallet trucks allowing us to load your pallets onto our vehicles.
We have weight and load service with no surcharge and we are happy to deliver anything that fits onto the pallet.
We provide services to many clients within the fashion and retail industry providing transport support to retailer and designers for fashion events and daily business activity.	<br/><a href="booking-form.php?action=deliveries_quote">Get a quote </a>	

				</p>
				<h4>We Cover</h4>
				<ul style="float:left; margin-right:10px; ">
					<li><i class="fa fa-check "></i>Same day delivery </li>
					<li><i class="fa fa-check "></i>Construction deliveries</li>
					<li><i class="fa fa-check "></i>Hospital and medical</li>
					<li><i class="fa fa-check "></i>Schools</li>
					<li><i class="fa fa-check "></i>Local councils </li>
				</ul>	
				<ul>
							
					<li><i class="fa fa-check "></i>Exhibitions  </li>
					<li><i class="fa fa-check "></i>Retail  </li>
					<li><i class="fa fa-check "></i>General public sector </li>
					<li><i class="fa fa-check "></i>Pallets   </li>
					<li><i class="fa fa-check "></i>Parcels   </li>
				</ul>
			</div>
            <div class='col-sm-5 col-md-offset-1'>
                <div class="controls clearfix">
                    <div class="controls pull-right clearfix">
                        <a href="">
                            <span id="arrowNext">
                            </span>
                        </a>
                        <a href="">
                            <span id="arrowPrev">
                            </span>
                        </a>
                    </div>
                </div>
                <article class="blog-post  clearfix">
                    <figure id="folioGallery">
                        <img class="img-responsive" src="http://www.jayegroup.com/images/gallery/delivery/7.jpg" title="Image title" alt="alternative information">
                        <img class="img-responsive" src="http://www.jayegroup.com/images/gallery/delivery/9.jpg" title="Image title" alt="alternative information">
                    </figure>
                </article>
			</div>		
		</div>
    </div>
<br/>
<br/>
<br/>
<br/>
<br/>
	<?php include 'includes/core/services.php';?>
	<?php include 'includes/core/footer_in_full.php';?>
</body>
</html>
